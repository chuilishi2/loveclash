﻿using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;
#pragma warning disable CS0162 // Unreachable code detected

namespace VMFramework.Core
{
    public static partial class AssetUtility
    {
        #region Get Asset Path

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static string GetAssetPath(this Object obj)
        {
#if UNITY_EDITOR
            return AssetDatabase.GetAssetPath(obj);
#endif
            return null;
        }

        #endregion

        #region Rename

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Rename(this Object obj, string newName)
        {
#if UNITY_EDITOR
            if (newName.IsNullOrEmptyAfterTrim())
            {
                Debug.LogWarning($"{obj.name}'s New Name cannot be Null or Empty.");
                return;
            }
            
            string selectedAssetPath = AssetDatabase.GetAssetPath(obj);

            if (selectedAssetPath.IsNullOrEmpty())
            {
                obj.name = newName;
            }
            else
            {
                AssetDatabase.RenameAsset(selectedAssetPath, newName);
            }

            Undo.RecordObject(obj, "Rename");
            
            obj.EnforceSave();
#endif
        }

        #endregion
    }
}