﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;
#pragma warning disable CS0162 // Unreachable code detected

namespace VMFramework.Core
{
    public static partial class AssetUtility
    {
        #region Get All Assets Recursively

        public static IEnumerable<Object> GetAllAssetsRecursively(
            this IEnumerable<Object> objects)
        {
            foreach (var obj in objects)
            {
                if (obj.IsFolder())
                {
                    foreach (var objInFolder in obj.GetAllAssetsInFolder())
                    {
                        yield return objInFolder;
                    }
                }
                else
                {
                    yield return obj;
                }
            }
        }

        #endregion

        #region Is Folder

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsFolder(this Object obj)
        {
            return obj.IsFolder(out _);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool IsFolder(this Object obj, out string path)
        {
#if UNITY_EDITOR
            path = AssetDatabase.GetAssetPath(obj);

            return AssetDatabase.IsValidFolder(path);
#endif
            path = null;
            return false;
        }

        #endregion
        
        #region Get All Assets In Folder

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static IEnumerable<Object> GetAllAssetsInFolder(this Object obj)
        {
#if UNITY_EDITOR
            if (obj.IsFolder(out var path))
            {
                string[] assetPaths = AssetDatabase.FindAssets("", new[] { path });

                foreach (string assetPath in assetPaths)
                {
                    Object objInFolder =
                        AssetDatabase.LoadAssetAtPath<Object>(
                            AssetDatabase.GUIDToAssetPath(assetPath));
                    yield return objInFolder;
                }
            }
#endif
            yield break;
        }

        #endregion
    }
}