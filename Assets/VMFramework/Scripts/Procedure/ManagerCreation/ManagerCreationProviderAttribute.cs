﻿using System;

namespace VMFramework.Procedure
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ManagerCreationProviderAttribute : Attribute
    {
        public readonly ManagerType ManagerType = ManagerType.OtherCore;

        public ManagerCreationProviderAttribute()
        {
        }

        public ManagerCreationProviderAttribute(ManagerType managerType)
        {
            ManagerType = managerType;
        }
    }
}