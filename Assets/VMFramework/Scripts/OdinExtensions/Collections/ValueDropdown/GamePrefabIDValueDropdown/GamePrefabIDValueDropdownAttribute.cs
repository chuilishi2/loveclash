﻿using System;
using VMFramework.GameLogicArchitecture;

namespace VMFramework.OdinExtensions
{
    public class GamePrefabIDValueDropdownAttribute : GeneralValueDropdownAttribute
    {
        public readonly Type[] GamePrefabTypes;

        public GamePrefabIDValueDropdownAttribute(params Type[] gamePrefabTypes)
        {
            GamePrefabTypes = gamePrefabTypes;
        }

        public GamePrefabIDValueDropdownAttribute()
        {
            GamePrefabTypes = new[] { typeof(IGamePrefab) };
        }
    }
}