﻿#if UNITY_EDITOR
using System;
using VMFramework.Procedure.Editor;

namespace VMFramework.GameLogicArchitecture.Editor
{
    public class GameSettingEditorInitializer : IEditorInitializer
    {
        public void OnBeforeInit(Action onDone)
        {
            foreach (var generalSetting in GameCoreSettingBase.GetAllGeneralSettings())
            {
                if (generalSetting == null)
                {
                    continue;
                }
                
                generalSetting.InitializeOnLoad();
            }
        }
    }
}
#endif