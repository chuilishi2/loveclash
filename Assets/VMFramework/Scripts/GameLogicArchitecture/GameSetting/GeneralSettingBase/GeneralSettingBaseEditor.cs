﻿#if UNITY_EDITOR
namespace VMFramework.GameLogicArchitecture
{
    public partial class GeneralSettingBase
    {
        protected virtual void OnInspectorInit()
        {
            AutoConfigureLocalizedString(new()
            {
                defaultTableName = defaultLocalizationTableName,
                save = true
            });
        }

        public virtual void InitializeOnLoad()
        {
            
        }
    }
}
#endif