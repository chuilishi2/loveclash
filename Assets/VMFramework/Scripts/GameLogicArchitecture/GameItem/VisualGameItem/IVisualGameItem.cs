﻿using VMFramework.UI;

namespace VMFramework.GameLogicArchitecture
{
    public interface IVisualGameItem : IGameItem, ITracingTooltipProvider
    {
        
    }
}