﻿// #if FISHNET
// using FishNet.Serializing;
// using UnityEngine;
//
// namespace VMFramework.GameLogicArchitecture
// {
//     public partial interface IGameItem
//     {
//         /// <summary>
//         /// 在网络上如何传输，当在此实例被写进byte流时调用
//         /// </summary>
//         /// <param name="writer"></param>
//         protected void OnWrite(Writer writer)
//         {
//             Debug.LogError("Not implemented On Write");
//         }
//
//         /// <summary>
//         /// 在网络上如何传输，当在此实例被从byte流中读出时调用
//         /// </summary>
//         /// <param name="reader"></param>
//         protected void OnRead(Reader reader)
//         {
//             Debug.LogError("Not implemented On Read");
//         }
//
//         /// <summary>
//         /// Fishnet的网络byte流写入
//         /// </summary>
//         /// <param name="writer"></param>
//         /// <param name="gameItem"></param>
//         public static void WriteGameItem(Writer writer, IGameItem gameItem)
//         {
//             if (gameItem == null)
//             {
//                 writer.WriteString(IGamePrefab.NULL_ID);
//             }
//             else
//             {
//                 writer.WriteString(gameItem.id);
//                 
//                 Debug.LogError($"Is Writing GameItem : {gameItem.id}");
//                 gameItem.OnWrite(writer);
//                 Debug.LogError($"Is Writing GameItem : {gameItem.id} - Done");
//             }
//         }
//
//         /// <summary>
//         /// Fishnet的网络byte流读出
//         /// </summary>
//         /// <param name="reader"></param>
//         /// <returns></returns>
//         public static IGameItem ReadGameItem(Reader reader)
//         {
//             var id = reader.ReadString();
//
//             if (id == IGamePrefab.NULL_ID)
//             {
//                 return null;
//             }
//
//             var gameItem = Create(id);
//
//             gameItem.OnRead(reader);
//
//             return gameItem;
//         }
//     }
// }
// #endif