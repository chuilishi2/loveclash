﻿#if UNITY_EDITOR
using VMFramework.Core;
using VMFramework.Localization;

namespace VMFramework.GameLogicArchitecture
{
    public partial class GameTypeGeneralSetting
    {
        public override void AutoConfigureLocalizedString(LocalizedStringAutoConfigSettings settings)
        {
            base.AutoConfigureLocalizedString(settings);

            subrootGameTypeInfos ??= new();
            
            foreach (var gameTypeInfo in subrootGameTypeInfos.PreorderTraverse(true))
            {
                if (gameTypeInfo is ILocalizedStringOwnerConfig localizedStringOwnerConfig)
                {
                    localizedStringOwnerConfig.AutoConfigureLocalizedString(settings);
                }
            }
            
            this.EnforceSave();
        }

        public override void CreateLocalizedStringKeys()
        {
            base.CreateLocalizedStringKeys();
            
            subrootGameTypeInfos ??= new();

            foreach (var gameTypeInfo in subrootGameTypeInfos.PreorderTraverse(true))
            {
                if (gameTypeInfo is ILocalizedStringOwnerConfig localizedStringOwnerConfig)
                {
                    localizedStringOwnerConfig.CreateLocalizedStringKeys();
                }
            }
        }

        public override void SetKeyValueByDefault()
        {
            base.SetKeyValueByDefault();
            
            subrootGameTypeInfos ??= new();
            
            foreach (var gameTypeInfo in subrootGameTypeInfos.PreorderTraverse(true))
            {
                if (gameTypeInfo is ILocalizedStringOwnerConfig localizedStringOwnerConfig)
                {
                    localizedStringOwnerConfig.SetKeyValueByDefault();
                }
            }
        }
    }
}
#endif