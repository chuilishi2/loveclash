﻿#if UNITY_EDITOR
using Sirenix.OdinInspector;
using UnityEngine;
using VMFramework.Configuration;
using VMFramework.OdinExtensions;

namespace VMFramework.Editor
{
    public class HierarchyColorPreset : BaseConfigClass
    {
        [IsNotNullOrEmpty]
        [LabelText("关键字符")]
        public string keyChar;

        [LabelText("文字颜色")]
        public Color textColor = Color.white;

        [LabelText("背景颜色")]
        public Color backgroundColor = Color.black;

        [LabelText("文字对齐")]
        [EnumToggleButtons]
        public TextAnchor textAlignment = TextAnchor.MiddleCenter;

        [LabelText("字体格式")]
        [EnumToggleButtons]
        public FontStyle fontStyle = FontStyle.Bold;

        [HideLabel]
        [ToggleButtons("自动大写字母", "保持原样")]
        public bool autoUpperLetters = true;
    }
}
#endif