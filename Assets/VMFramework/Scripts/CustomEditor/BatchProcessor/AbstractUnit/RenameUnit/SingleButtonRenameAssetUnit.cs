﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VMFramework.Core;

namespace VMFramework.Editor
{
    public abstract class SingleButtonRenameAssetUnit : SingleButtonBatchProcessorUnit
    {
        protected override Color buttonColor => Color.yellow;

        public override bool IsValid(IList<object> selectedObjects)
        {
            return selectedObjects.Any(o => o is Object);
        }

        protected sealed override IEnumerable<object> OnProcess(
            IEnumerable<object> selectedObjects)
        {
            foreach (var o in selectedObjects)
            {
                if (o is Object unityObject)
                {
                    unityObject.Rename(ProcessAssetName(unityObject.name));
                }

                yield return o;
            }
        }

        protected abstract string ProcessAssetName(string oldName);
    }
}