﻿#if UNITY_EDITOR
using Sirenix.OdinInspector;
using VMFramework.GameLogicArchitecture;
using VMFramework.Localization;

namespace VMFramework.Editor
{
    public sealed partial class HierarchyComponentIconGeneralSetting : GeneralSettingBase
    {
        [LabelText("最大图标数量")]
        [PropertyRange(1, 10)]
        public int maxIconNum = 5;

        [LabelText("图标大小")]
        [PropertyRange(1, 24)]
        public int iconSize = 16;
    }
}

#endif