#if UNITY_EDITOR
using System;
using Cysharp.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using VMFramework.Editor;
using VMFramework.Procedure;
using VMFramework.Procedure.Editor;

public class GameEditorInitializer : IEditorInitializer
{
    async void IInitializer.OnInitComplete(Action onDone)
    {
        if (Application.isPlaying)
        {
            return;
        }
        
        var gameEditor = EditorWindow.GetWindow<GameEditor>();

        if (gameEditor != null)
        {
            await UniTask.Delay(1000);
                
            gameEditor.Repaint();
            gameEditor.ForceMenuTreeRebuild();
                
            await UniTask.Delay(1000);
                
            gameEditor.Repaint();
            gameEditor.ForceMenuTreeRebuild();
        }
    }
}

#endif