﻿using VMFramework.Containers;

namespace VMFramework.UI
{
    public interface IContainerUIPanel : IUIPanelController
    {
        public int containerUIPriority { get; }

        public Container GetBindContainer();

        public void SetBindContainer(Container newBindContainer);
    }

}