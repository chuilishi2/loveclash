﻿using Sirenix.OdinInspector;

namespace VMFramework.GlobalEvent
{
    public enum InputType
    {
        [LabelText("键盘、鼠标或操纵杆")]
        KeyBoardOrMouseOrJoyStick
    }
}