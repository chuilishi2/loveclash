﻿using FishNet.Object;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using FishNet.Connection;
using Sirenix.OdinInspector;
using UnityEngine;
using VMFramework.Network;
using VMFramework.Procedure;

namespace VMFramework.Containers
{
    [ManagerCreationProvider(ManagerType.NetworkCore)]
    public abstract class ContainerManagerBase : 
        UUIDManager<ContainerManagerBase, Container, ContainerManagerBase.ContainerInfo>
    { 
        public class ContainerInfo : OwnerInfo
        {
            public HashSet<int> dirtySlots;
        }

        [SerializeField]
        protected bool isDebugging;

        protected override void OnPreInit()
        {
            base.OnPreInit();

            OnRegisterEvent += OnRegister;
            OnUnregisterEvent += OnUnregister;
        }

        #region Register & Unregister

        private static void OnRegister(ContainerInfo info)
        {
            if (_instance.IsServerStarted)
            {
                info.dirtySlots = new();
                info.owner.OnItemCountChangedEvent += OnContainerItemCountChanged;
                info.owner.OnItemAddedEvent += OnItemAdded;
                info.owner.OnItemRemovedEvent += OnItemRemoved;
            }
        }

        private static void OnUnregister(ContainerInfo info)
        {
            if (_instance.IsServerStarted)
            {
                info.owner.OnItemCountChangedEvent -= OnContainerItemCountChanged;
                info.owner.OnItemAddedEvent -= OnItemAdded;
                info.owner.OnItemRemovedEvent -= OnItemRemoved;
            }
        }

        #endregion

        #region Observe & Unobserve

        protected override void OnObserved(Container container, bool isDirty,
            NetworkConnection connection)
        {
            base.OnObserved(container, isDirty, connection);

            if (TryGetInfo(container.uuid, out var containerInfo))
            {
                if (containerInfo.observers.Count == 0)
                {
                    container.OpenOnServer();
                }
            }
            else
            {
                Debug.LogWarning(
                    $"不存在此{container.uuid}对应的{nameof(ContainerInfo)}");
            }
            
            if (isDirty)
            {
                ReconcileAllOnTarget(connection, container.uuid);
            }
        }

        protected override void OnUnobserved(Container container,
            NetworkConnection connection)
        {
            base.OnUnobserved(container, connection);

            if (TryGetInfo(container.uuid, out var containerInfo))
            {
                if (containerInfo.observers.Count <= 0)
                {
                    container.CloseOnServer();
                }
            }
            else
            {
                Debug.LogWarning(
                    $"不存在此{container.uuid}对应的{nameof(ContainerInfo)}");
            }
        }

        #endregion

        #region Container Changed

        private void Update()
        {
            if (IsServerStarted == false)
            {
                return;
            }

            var isHost = IsHostStarted;
            var hostClientID = isHost ? ClientManager.Connection.ClientId : -1;
            foreach (var containerInfo in GetAllOwnerInfos())
            {
                if (containerInfo.dirtySlots.Count == 0)
                {
                    continue;
                }

                if (containerInfo.observers.Count == 0)
                {
                    //containerInfo.dirtySlots.Clear();
                    continue;
                }

                if (containerInfo.observers.Count == 1 &&
                    containerInfo.observers.Contains(hostClientID))
                {
                    containerInfo.dirtySlots.Clear();
                    continue;
                }

                var ratio = containerInfo.dirtySlots.Count /
                            (float)containerInfo.owner.size;

                if (ratio > 0.6f)
                {
                    ReconcileAllItemsOnObservers(containerInfo);
                }
                else
                {
                    if (containerInfo.dirtySlots.Count == 1)
                    {
                        var slotIndex = containerInfo.dirtySlots.First();
                        ReconcileItemOnObservers(containerInfo, slotIndex);
                    }
                    else
                    {
                        ReconcileSomeItemsOnObservers(containerInfo,
                            containerInfo.dirtySlots);
                    }
                }

                SetDirty(containerInfo.owner.uuid);

                containerInfo.dirtySlots.Clear();
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetSlotDirty(Container container, int slotIndex)
        {
            SetSlotDirty(container.uuid, slotIndex);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetSlotDirty(string containerUUID, int slotIndex)
        {
            if (TryGetInfo(containerUUID, out var info))
            {
                SetSlotDirty(info, slotIndex);
            }
            else
            {
                Debug.LogWarning($"试图设置一个不存在的{typeof(Container)}的slot为脏");
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static void SetSlotDirty(ContainerInfo containerInfo,
            int slotIndex)
        {
            containerInfo.dirtySlots.Add(slotIndex);
        }

        private static void OnContainerItemCountChanged(Container container, 
            int slotIndex, IContainerItem item, int previous, int current)
        {
            if (current != previous)
            {
                SetSlotDirty(container.uuid, slotIndex);
            }
        }

        private static void OnItemRemoved(Container container, int slotIndex, 
            IContainerItem item)
        {
            SetSlotDirty(container.uuid, slotIndex);
        }

        private static void OnItemAdded(Container container, int slotIndex, 
            IContainerItem item)
        {
            SetSlotDirty(container.uuid, slotIndex);
        }

        #endregion

        #region Reconcile On Observers

        protected abstract void _ReconcileItemOnObservers(ContainerInfo containerInfo,
            int slotIndex);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static void ReconcileItemOnObservers(ContainerInfo containerInfo,
            int slotIndex)
        {
            _instance._ReconcileItemOnObservers(containerInfo, slotIndex);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static void ReconcileItemOnObservers(string containerUUID, int slotIndex)
        {
            if (TryGetInfo(containerUUID, out var info) == false)
            {
                Debug.LogWarning($"不存在此{containerUUID}对应的{nameof(info)}");
                return;
            }

            ReconcileItemOnObservers(info, slotIndex);
        }

        protected abstract void _ReconcileSomeItemsOnObservers(
            ContainerInfo containerInfo, HashSet<int> slotIndexes);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static void ReconcileSomeItemsOnObservers(
            ContainerInfo containerInfo, HashSet<int> slotIndexes)
        {
            _instance._ReconcileSomeItemsOnObservers(containerInfo, slotIndexes);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static void ReconcileSomeItemsOnObservers(string containerUUID,
            HashSet<int> slotIndices)
        {
            if (TryGetInfo(containerUUID, out var info) == false)
            {
                Debug.LogWarning($"不存在此{containerUUID}对应的{nameof(info)}");
                return;
            }

            ReconcileSomeItemsOnObservers(info, slotIndices);
        }

        protected abstract void _ReconcileAllItemsOnObservers(
            ContainerInfo containerInfo);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected static void ReconcileAllItemsOnObservers(
            ContainerInfo containerInfo)
        {
            _instance._ReconcileAllItemsOnObservers(containerInfo);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [Button]
        protected static void ReconcileAllItemsOnObservers(string containerUUID)
        {
            if (TryGetInfo(containerUUID, out var info) == false)
            {
                Debug.LogWarning($"不存在此{containerUUID}对应的{nameof(info)}");
                return;
            }

            ReconcileAllItemsOnObservers(info);
        }

        #endregion

        #region Reconcile On Target

        protected abstract void _ReconcileAllOnTarget(NetworkConnection connection,
            string containerUUID);

        protected static void ReconcileAllOnTarget(NetworkConnection connection,
            string containerUUID)
        {
            _instance._ReconcileAllOnTarget(connection, containerUUID);
        }

        #endregion

        #region Set Dirty

        [ObserversRpc(ExcludeServer = true)]
        protected void SetDirty(string containerUUID)
        {
            if (TryGetInfo(containerUUID, out var containerInfo))
            {
                if (containerInfo.owner.isOpen == false)
                {
                    containerInfo.owner.isDirty = true;
                }
            }
        }

        #endregion
    }
}
