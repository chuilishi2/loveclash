﻿using System.Collections.Generic;

namespace VMFramework.Containers
{
    public interface IContainerOwner
    {
        public IEnumerable<Container> GetContainers();
    }
}
