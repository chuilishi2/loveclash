﻿using System;
using System.Collections.Generic;
using VMFramework.GameLogicArchitecture;

namespace VMFramework.Containers
{
    public interface IContainer : IGameItem
    {
        public bool isOpen { get; }

        public bool isDestroyed { get; }

        public abstract int size { get; }

        public int validItemsSize { get; }
        
        public bool isFull { get; }
        
        public IReadOnlyCollection<int> validSlotIndices { get; }
        
        public bool TryGetItem(int index, out IContainerItem item);
        
        public IContainerItem GetItem(int index);
        
        public IContainerItem GetItemWithoutCheck(int index);
        
        public IEnumerable<IContainerItem> GetAllItems();

        public IContainerItem[] GetItemArray();

        public bool TryMergeItem(int index, IContainerItem newItem);
        
        public void SetItem(int index, IContainerItem item);

        public bool TryAddItem(IContainerItem item);
        
        public bool TryAddItem(IContainerItem item, int startIndex, int endIndex);

        public bool TryPopItemByPreferredCount(int preferredCount, out IContainerItem item,
            out int slotIndex);
        
        public void Sort(int startIndex, int endIndex, Comparison<IContainerItem> comparison);

        public void Open();

        public void Close();
    }
}