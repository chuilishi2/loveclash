﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using VMFramework.Configuration;
using VMFramework.Core;
using VMFramework.Core.Linq;
using VMFramework.GameLogicArchitecture;
using VMFramework.OdinExtensions;

namespace VMFramework.Property
{
    public class TooltipPropertyConfig : BaseConfigClass, IIDOwner<Type>
    {
        #region Configs

        [LabelText("实例类型")]
        [OnValueChanged(nameof(OnInstanceTypeChangedGUI))]
        [IsNotNullOrEmpty]
        [TypeValueDropdown(typeof(IGameItem), IncludingInterfaces = false, IncludingAbstract = true,
            IncludingGeneric = false)]
        [SerializeField]
        private Type _instanceType;
        
        [LabelText("属性")]
        [OnCollectionChanged(nameof(OnInstanceTypeChangedGUI))]
        [SerializeField]
        private List<InstanceTooltipPropertyConfig> tooltipPropertyConfigs = new();

        #endregion

        #region Properties

        public Type instanceType
        {
            init => _instanceType = value;
            get => _instanceType;
        }

        public IEnumerable<InstanceTooltipPropertyConfigRuntime> tooltipPropertyConfigsRuntime
        {
            init
            {
                tooltipPropertyConfigs.Clear();
                
                foreach (var configRuntime in value)
                {
                    var config = new InstanceTooltipPropertyConfig()
                    {
                        propertyID = configRuntime.propertyConfig.id,
                        propertyConfig = configRuntime.propertyConfig,
                        isStatic = configRuntime.isStatic
                    };
                    
                    tooltipPropertyConfigs.Add(config);
                }
            }
            get
            {
                foreach (var config in tooltipPropertyConfigs)
                {
                    yield return config.ConvertToRuntime();
                }
            }
        }

        #endregion

        #region Interface Implementation

        Type IIDOwner<Type>.id => instanceType;

        #endregion

        #region GUI

        protected override void OnInspectorInit()
        {
            base.OnInspectorInit();
        
            OnInstanceTypeChangedGUI();
        }
        
        private void OnInstanceTypeChangedGUI()
        {
            tooltipPropertyConfigs ??= new();
        
            foreach (var config in tooltipPropertyConfigs)
            {
                config.filterType = _instanceType;
            }
        }

        #endregion

        public override void CheckSettings()
        {
            base.CheckSettings();
        
            foreach (var tooltipPropertyConfig in tooltipPropertyConfigs)
            {
                tooltipPropertyConfig.propertyID.AssertIsNotNullOrEmpty(
                    $"{nameof(tooltipPropertyConfig)}." +
                    $"{nameof(tooltipPropertyConfig.propertyID)}");
            }
        
            if (tooltipPropertyConfigs.ContainsSame(config => config.propertyID))
            {
                Debug.LogWarning("默认的提示框属性设置有重复的属性ID");
            }
        
            foreach (var config in tooltipPropertyConfigs)
            {
                config.CheckSettings();
            }
        }
        
        protected override void OnInit()
        {
            base.OnInit();
        
            foreach (var config in tooltipPropertyConfigs)
            {
                config.Init();
            }
        }
    }
}