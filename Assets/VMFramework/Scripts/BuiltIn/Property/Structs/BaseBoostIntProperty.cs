﻿using System;
using System.Runtime.CompilerServices;
using VMFramework.Core;
using Sirenix.OdinInspector;
using VMFramework.OdinExtensions;

#if FISHNET
using FishNet.Serializing;
#endif

[PreviewComposite]
public struct BaseBoostIntProperty : IFormattable
{
    [ShowInInspector]
    [ReadOnly]
    public int value { get; private set; }

    private int _baseValue;

    [ShowInInspector]
    public int baseValue
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        readonly get => _baseValue;
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        set
        {
            _baseValue = value;
            var oldValue = this.value;
            this.value = (_baseValue * _boostValue).Floor();
            this.value = this.value.ClampMin(0);
            OnValueChanged?.Invoke(oldValue, this.value);
        }
    }

    private float _boostValue;

    [ShowInInspector]
    public float boostValue
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        readonly get => _boostValue;
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        set
        {
            _boostValue = value;
            var oldValue = this.value;
            this.value = (_baseValue * _boostValue).Floor();
            this.value = this.value.ClampMin(0);
            OnValueChanged?.Invoke(oldValue, this.value);
        }
    }

    public event Action<float, float> OnValueChanged;

    public BaseBoostIntProperty(int baseValue, float boostValue)
    {
        _baseValue = baseValue;
        _boostValue = boostValue;
        value = (_baseValue * _boostValue).Floor();
        value = value.ClampMin(0);
        OnValueChanged = null;
    }

    public BaseBoostIntProperty(int value) : this(value, 1)
    {
    }

    #region To String

    public readonly string ToString(string format, IFormatProvider formatProvider)
    {
        return value.ToString(format, formatProvider);
    }

    public readonly override string ToString()
    {
        return value.ToString();
    }

    #endregion

    public static implicit operator int(BaseBoostIntProperty property) =>
        property.value;
}

#if FISHNET
public static class BaseBoostIntPropertySerializer
{
    public static void WriteBaseBoostIntProperty(this Writer writer, BaseBoostIntProperty property)
    {
        writer.WriteInt32(property.baseValue);
        writer.WriteSingle(property.boostValue);
    }

    public static BaseBoostIntProperty ReadBaseBoostIntProperty(this Reader reader)
    {
        var baseValue = reader.ReadInt32();
        var boostValue = reader.ReadSingle();
        return new BaseBoostIntProperty(baseValue, boostValue);
    }
}
#endif
