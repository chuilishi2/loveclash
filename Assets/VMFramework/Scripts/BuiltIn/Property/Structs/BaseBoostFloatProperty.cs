﻿using System;
using System.Runtime.CompilerServices;
using VMFramework.Core;
using Sirenix.OdinInspector;
using VMFramework.OdinExtensions;

#if FISHNET
using FishNet.Serializing;
#endif

[PreviewComposite]
public struct BaseBoostFloatProperty : IFormattable
{
    [ShowInInspector]
    [ReadOnly]
    public float value { get; private set; }

    private float _baseValue;

    [ShowInInspector]
    public float baseValue
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        readonly get => _baseValue;
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        set
        {
            _baseValue = value;
            var oldValue = this.value;
            this.value = _baseValue * _boostValue;
            this.value = this.value.ClampMin(0);
            OnValueChanged?.Invoke(oldValue, this.value);
        }
    }

    private float _boostValue;

    [ShowInInspector]
    public float boostValue
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        readonly get => _boostValue;
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        set
        {
            _boostValue = value;
            var oldValue = this.value;
            this.value = _baseValue * _boostValue;
            this.value = this.value.ClampMin(0);
            OnValueChanged?.Invoke(oldValue, this.value);
        }
    }

    public event Action<float, float> OnValueChanged;

    public BaseBoostFloatProperty(float baseValue, float boostValue)
    {
        _baseValue = baseValue;
        _boostValue = boostValue;
        value = _baseValue * _boostValue;
        value = value.ClampMin(0);
        OnValueChanged = null;
    }

    public BaseBoostFloatProperty(float value) : this(value, 1)
    {
    }

    #region To String

    public readonly string ToString(string format, IFormatProvider formatProvider)
    {
        return value.ToString(format, formatProvider);
    }

    public readonly override string ToString()
    {
        return value.ToString();
    }

    #endregion

    public static implicit operator float(BaseBoostFloatProperty property) =>
        property.value;
}

#if FISHNET
public static class BaseBoostFloatPropertySerializer
{
    public static void WriteBaseBoostFloatProperty(this Writer writer, BaseBoostFloatProperty property)
    {
        writer.WriteSingle(property.baseValue);
        writer.WriteSingle(property.boostValue);
    }

    public static BaseBoostFloatProperty ReadBaseBoostFloatProperty(this Reader reader)
    {
        var baseValue = reader.ReadSingle();
        var boostValue = reader.ReadSingle();
        return new BaseBoostFloatProperty(baseValue, boostValue);
    }
}
#endif
