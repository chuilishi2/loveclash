using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Script.UI
{
    /// <summary>
    /// 界面路由
    /// </summary>
    public class SceneRoutes : MonoBehaviour
    {
        public int currentSceneIndex;
        [Serializable] 
        public struct TransitionUIStruct
        {
            public SceneAsset scene;
            public GameObject sprite;
        }
        public List<TransitionUIStruct> transitionList;

        /// <summary>
        /// 复制界面按钮和跳转路由给UIManager
        /// </summary>
        /// <param name="list">UIManager的路由记录表</param>
        public void CopySceneInformation(List<TransitionUIStruct> list)
        {
            if (list.Count != 0)
            {
                list.Clear();
            }
            foreach (TransitionUIStruct item in transitionList)
            {
                list.Add(item);
            }
            
        }
    }
}
