using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UIFramework.Base
{
    [RequireComponent(typeof(TimerManager))]
    public class BaseUIManager : MonoBehaviour
    {
        private static BaseUIManager _instance;
        public static BaseUIManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    new GameObject("BaseUIManager").AddComponent<BaseUIManager>(); ;
                }
                return _instance;
            }
        }

        private Transform _uiOpenedRoot;
        public Transform UIOpenedRoot { get { return _uiOpenedRoot; } }

        private Transform _uiClosedRoot;
        public Transform UIClosedRoot { get { return _uiClosedRoot; } }

        private Dictionary<string, BasePanel> panelOpenedDict;
        public Dictionary<string, BasePanel> PanelOpenedDict { get => panelOpenedDict; set => panelOpenedDict = value; }

        private Dictionary<string, BasePanel> panelClosedDict;
        public Dictionary<string, BasePanel> PanelClosedDict { get => panelClosedDict; set => panelClosedDict = value; }

        private Dictionary<string, BaseWidget> widgetClosedDict;
        public Dictionary<string, BaseWidget> WidgetClosedDict { get => widgetClosedDict; set => widgetClosedDict = value; }

        /// <summary>
        /// 缓存预制体，会比较占内存，如果想要内存优化而不在意资源加载这一点点性能开销，可以不用
        /// </summary>
        private Dictionary<string, GameObject> prefabDict;

        private int sortingOrder;

        private void Awake()
        {
            if (_instance != null) Destroy(gameObject);
            else
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
                InitDicts();
            }
        }

        private void Start()
        {
            if (_uiOpenedRoot == null)
            {
                _uiOpenedRoot = new GameObject("UIOpenedRoot").transform;
            }
            DontDestroyOnLoad(_uiOpenedRoot);

            if (_uiClosedRoot == null)
            {
                _uiClosedRoot = new GameObject("UIClosedRoot").transform;
                _uiClosedRoot.transform.position = new Vector3(10000, 10000, 0);
            }
            DontDestroyOnLoad(_uiClosedRoot);

            OpenPanel<GenderPanel>("请选择你的性别", "男性", "女性");
        }

        private void InitDicts()
        {
            sortingOrder = 0;
            prefabDict = new Dictionary<string, GameObject>();
            PanelOpenedDict = new Dictionary<string, BasePanel>();
            PanelClosedDict = new Dictionary<string, BasePanel>();
            WidgetClosedDict = new Dictionary<string, BaseWidget>();
        }

        public BasePanel OpenPanel(string name, params string[] contents)
        {
            //重复打开，则将它的显示优先级拉到最高
            if (PanelOpenedDict.ContainsKey(name))
            {
                PanelOpenedDict[name].SortingOrder = sortingOrder++;
                return null;
            }

            BasePanel panel;

            if (PanelClosedDict.TryGetValue(name, out panel))
            {
                PanelClosedDict.Remove(name);

                panel.ReOpen();

                TimerManager.Instance.StopTimer(panel.timer);
            }
            else
            {
                GameObject panelPrefab;

                if (!prefabDict.TryGetValue(name, out panelPrefab))
                {
                    panelPrefab = Resources.Load<GameObject>("Prefabs/Panels/" + name);

                    if (panelPrefab == null) { Debug.LogError("界面名称错误 / 未配置预制体路径：" + name); return null; }

                    prefabDict.Add(name, panelPrefab);
                }

                panel = GameObject.Instantiate(panelPrefab, UIOpenedRoot).GetComponent<BasePanel>();

                panel.Init();
            }

            PanelOpenedDict.Add(name, panel);

            panel.SortingOrder = sortingOrder++;

            panel.transform.SetParent(UIOpenedRoot.transform, false);

            panel.OpenPanel();

            panel.PanelContents = contents;

            panel.Refresh();

            using var panelOpenEvt = PanelOpenEvent.Get();

            panelOpenEvt.basePanel = panel;

            EventManager.SendEvent(panelOpenEvt);

            return panel;
        }

        public T OpenPanel<T>(params string[] contents) where T : BasePanel
        {
            return OpenPanel(typeof(T).Name, contents) as T;
        }

        public bool ClosePanel(string name)
        {
            BasePanel panel;

            if (!PanelOpenedDict.TryGetValue(name, out panel))
            {
                Debug.LogError("界面未打开: " + name);
                return false;
            }

            PanelOpenedDict.Remove(name);

            PanelClosedDict.Add(name, panel);

            panel.ClosePanel();

            Transform childTransform = panel.transform.GetChild(0).transform;
            panel.OpenedPosition = childTransform.position;
            panel.transform.SetParent(UIClosedRoot);
            childTransform.position = UIClosedRoot.position;

            //在过多开关界面操作的情况下，sortingOrder 可能会到上限值
            //Unity 的 sortingOrder 的上限并不是 int 的上限，它的取值范围大概是 -2^15~2^15，也就是最大 32767
            if (sortingOrder > 30000)
            {
                List<BasePanel> basePanels = PanelOpenedDict.Values.ToList();

                basePanels.Sort((panel1, panel2) =>
                {
                    return panel2.GetComponent<Canvas>().sortingOrder - panel1.GetComponent<Canvas>().sortingOrder;
                });

                for (int i = 0; i < basePanels.Count; i++)
                {
                    panel.GetComponent<Canvas>().sortingOrder = i;
                }

                sortingOrder = basePanels.Count;
            }

            TimerManager.Instance.EnQueue(panel.timer);

            using var panelCloseEvt = PanelCloseEvent.Get();

            panelCloseEvt.basePanel = panel;

            EventManager.SendEvent(panelCloseEvt);

            return true;
        }

        public T ClosePanel<T>() where T : BasePanel
        {
            return ClosePanel(typeof(T).Name) as T;
        }
    }
}