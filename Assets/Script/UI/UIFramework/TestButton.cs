using UIFramework.Base;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TestButton : MonoBehaviour
{
    private Button openBtn;
    private Button scene10Btn;
    private Button scene9Btn;

    private void Awake()
    {
        openBtn = transform.Find("OpenBtn").GetComponent<Button>();
        scene9Btn = transform.Find("Scene9Btn").GetComponent<Button>();
        scene10Btn = transform.Find("Scene10Btn").GetComponent<Button>();
    }

    void Start()
    {
        openBtn.onClick.AddListener(() =>
        {
            BaseUIManager.Instance.OpenPanel<TestPanel>();
        });
        scene9Btn.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(9);
        });
        scene10Btn.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(10);
        });
    }
}
