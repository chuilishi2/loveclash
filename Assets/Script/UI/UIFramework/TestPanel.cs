using UIFramework.Base;
using UnityEngine.UI;

public class TestPanel : BasePanel
{
    private Button button1;

    private void Awake()
    {
        button1 = transform.Find("TestPanel/TopRight/CloseBtn").GetComponent<Button>();
    }

    private void Start()
    {
        button1.onClick.AddListener(() => BaseUIManager.Instance.ClosePanel(name));
    }
}
