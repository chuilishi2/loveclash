using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace UIFramework.Base
{
    //可以考虑将常用的界面另外处理（比如用一个标记接口标记，或者用一个可复用面板基类 ReusablePanel，
    //重写打开、关闭），存在界面池里，同时，游戏开始时就可以直接开始异步加载这种面板了
    public class BasePanel : MonoBehaviour
    {
        //可能还需要一个预制体初始 Position

        /// <summary>
        /// 用于记录界面打开状态的 position
        /// </summary>
        private Vector2 openingPosition;
        public Vector2 OpenedPosition { get => openingPosition; set => openingPosition = value; }

        private string[] panelContent;
        public string[] PanelContents { get => panelContent; set => panelContent = value; }

        private Dictionary<string, BaseWidget> widgetOpenedDict;
        public Dictionary<string, BaseWidget> WidgetOpenedDict { get => widgetOpenedDict; set => widgetOpenedDict = value; }

        private Dictionary<string, GameObject> prefabDict;

        private int sortingOrder;
        public int SortingOrder
        {
            get => sortingOrder;
            set
            {
                sortingOrder = value;
                GetComponent<Canvas>().sortingOrder = sortingOrder;
            }
        }

        public Timer timer;

        /// <summary>
        /// 界面不是关闭状态下被打开时，需要加载资源，且会执行这个方法
        /// </summary>
        public virtual void Init()
        {
            timer = new Timer(30, () => TimeToDestroy());
            WidgetOpenedDict = new Dictionary<string, BaseWidget>();
            prefabDict = new Dictionary<string, GameObject>();
        }

        /// <summary>
        /// 在界面从关闭变为打开时，这时候不用加载资源，且会执行这个方法
        /// </summary>
        public virtual void ReOpen()
        {
            transform.GetChild(0).transform.position = OpenedPosition;
        }

        /// <summary>
        /// 刷新的逻辑都写在这里
        /// </summary>
        public virtual void Refresh()
        {
            WidgetOpenedDict.Clear();
            prefabDict.Clear();
        }

        /// <summary>
        /// 打开这个界面，会完成一些初始化工作
        /// </summary>
        /// <param name="name"></param>
        public virtual void OpenPanel()
        {
            ReadyToOpenPanel();

            // 另外，canvas group有一个blocks raycasts，打上勾面板就可以被点击，去掉后面板就不能被点击，
            // 可以用在新面板打开后对旧面板的处理上
            CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
            if (canvasGroup != null)
            {
                canvasGroup.alpha = 0.0f;

                //1f 是赋值给 x 的，0.5f 是动画时间
                DOTween.To(() => canvasGroup.alpha, x => canvasGroup.alpha = x, 1f, 0.5f);
            }
        }

        /// <summary>
        /// 关闭界面，做善后工作
        /// </summary>
        public virtual void ClosePanel()
        {
            CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
            if (canvasGroup != null)
            {
                DOTween.To(() => canvasGroup.alpha, x => canvasGroup.alpha = x, 0, 0.5f);
            }

            ReadyToClosePanel();
        }

        /// <summary>
        /// 打开子界面
        /// </summary>
        public virtual BaseWidget OpenWidget(string name, params int[] contents)
        {
            if (WidgetOpenedDict.ContainsKey(name))
            {
                return null;
            }

            BaseWidget widget;

            if (BaseUIManager.Instance.WidgetClosedDict.TryGetValue(name, out widget))
            {
                BaseUIManager.Instance.WidgetClosedDict.Remove(name);

                widget.ReOpen();
            }
            else
            {
                GameObject widgetPrefab;

                if (!prefabDict.TryGetValue(name, out widgetPrefab))
                {
                    widgetPrefab = Resources.Load<GameObject>("Prefabs/Widgets/" + name);

                    if (widgetPrefab == null) { Debug.LogError("子界面名称错误 / 未配置预制体路径：" + name); return null; }

                    prefabDict.Add(name, widgetPrefab);
                }

                widget = GameObject.Instantiate(widgetPrefab, transform).GetComponent<BaseWidget>();

                widget.Init();
            }

            WidgetOpenedDict.Add(name, widget);

            widget.ParentPanel = this;

            widget.transform.SetParent(transform);

            widget.OpenWidget();

            widget.WidgetContents = contents;

            widget.Refresh();

            using var widgetOpenEvt = WidgetOpenEvent.Get();

            widgetOpenEvt.baseWidget = widget;

            EventManager.SendEvent(widgetOpenEvt);

            return widget;
        }

        public virtual T OpenWidget<T>(params int[] contents) where T : BaseWidget
        {
            return OpenWidget(typeof(T).Name, contents) as T;
        }

        public bool CloseWidget(string name, params int[] widgetCloseInfo)
        {
            BaseWidget widget;

            if (!WidgetOpenedDict.TryGetValue(name, out widget))
            {
                Debug.LogError("子界面未打开: " + name);
                return false;
            }

            WidgetOpenedDict.Remove(name);

            BaseUIManager.Instance.WidgetClosedDict.Add(name, widget);

            widget.CloseWidget();

            widget.OpenedPosition = widget.transform.position;
            widget.transform.SetParent(BaseUIManager.Instance.UIClosedRoot);
            widget.transform.position = BaseUIManager.Instance.UIClosedRoot.position;

            using var widgetCloseEvt = WidgetCloseEvent.Get();

            widgetCloseEvt.baseWidget = widget;

            if (widgetCloseInfo != null)
            {
                widgetCloseEvt.widgetCloseInfo = widgetCloseInfo;
            }

            EventManager.SendEvent(widgetCloseEvt);

            return true;
        }

        public virtual T CloseWidget<T>(params int[] widgetCloseInfo) where T : BaseWidget
        {
            return CloseWidget(typeof(T).Name, widgetCloseInfo) as T;
        }

        public virtual void ReadyToOpenPanel()
        {
            AddListening();
        }

        public virtual void ReadyToClosePanel()
        {
            RemoveListening();
        }

        public void TimeToDestroy()
        {
            BaseUIManager.Instance.PanelClosedDict.Remove(name);

            RemoveListening();

            Destroy(gameObject);
        }

        private void OnDestroy()
        {
            RemoveListening();
        }

        public virtual void AddListening()
        {

        }

        public virtual void RemoveListening()
        {
            EventManager.UnregisterTarget(this);
        }
    }
}