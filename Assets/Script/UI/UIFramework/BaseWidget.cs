using UnityEngine;
namespace UIFramework.Base
{
    public class BaseWidget : MonoBehaviour
    {
        /// <summary>
        /// 用于记录界面打开状态的 position
        /// </summary>
        private Vector2 openingPosition;
        public Vector2 OpenedPosition { get => openingPosition; set => openingPosition = value; }

        private BasePanel parentPanel;
        public BasePanel ParentPanel { get => parentPanel; set => parentPanel = value; }

        private int[] widgetContent;
        public int[] WidgetContents { get => widgetContent; set => widgetContent = value; }

        /// <summary>
        /// 界面不是关闭状态下被打开时，需要加载资源，且会执行这个方法
        /// </summary>
        public virtual void Init()
        {

        }

        /// <summary>
        /// 在界面从关闭变为打开时，这时候不用加载资源，且会执行这个方法
        /// </summary>
        public virtual void ReOpen()
        {
            transform.position = OpenedPosition;
        }

        /// <summary>
        /// 刷新的逻辑都写在这里
        /// </summary>
        public virtual void Refresh()
        {

        }

        public virtual void OpenWidget()
        {
            ReadyToOpenWidget();

            //淡入淡出效果的动画可以写在这里
        }

        public virtual void CloseWidget()
        {
            ReadyToCloseWidget();

            //淡入淡出效果的动画可以写在这里
        }

        public virtual void ReadyToOpenWidget()
        {
            AddListening();
        }

        public virtual void ReadyToCloseWidget()
        {
            RemoveListening();
        }

        private void OnDestroy()
        {
            RemoveListening();
        }

        public virtual void AddListening()
        {

        }

        public virtual void RemoveListening()
        {
            EventManager.UnregisterTarget(this);
        }
    }
}