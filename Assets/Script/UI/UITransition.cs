using System;
using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Script.UI
{
    public class UITransition : MonoBehaviour
    {
        [HideInInspector]
        public String typeName = "transitionUI";
        public int transitionNo;
        public int targetSceneIndex;
        /// <summary>
        /// UI面板同步切换
        /// </summary>
        public void SyncTransition()
        {
            SceneManager.UnloadSceneAsync(SceneManager.GetSceneByBuildIndex(UIManager.Instance.currentScene));
            SceneManager.LoadSceneAsync(targetSceneIndex, LoadSceneMode.Additive);
            // 面板卸载完成事件
            SceneManager.sceneUnloaded += (Scene sc) =>
            {
                UIManager.Instance.currentScene = targetSceneIndex;
                SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(3));
            };
            
            // 面板加载完成事件
            SceneManager.sceneLoaded += (Scene sc, LoadSceneMode loadSceneMode) =>
            {
                
                UIManager.Instance.ChangeUIPanel();
            };
            
            
            
        }
        
        /// <summary>
        /// 异步切换
        /// </summary>
        /// <param name="sceneAsset"></param>
        /// <returns></returns>
        public IEnumerator ASyncTransition(SceneAsset sceneAsset)
        {
            yield return 0;
        }
    }
}
