using UIFramework.Base;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmWidget : BaseWidget
{
    private Button ConfirmBtn;
    private Button CancelBtn;

    public override void Init()
    {
        base.Init();

        ConfirmBtn = transform.Find("ConfirmBtn").GetComponent<Button>();
        CancelBtn = transform.Find("CancelBtn").GetComponent<Button>();
    }

    public override void AddListening()
    {
        base.AddListening();

        ConfirmBtn.onClick.AddListener(() =>
        {
            ParentPanel.CloseWidget<ConfirmWidget>(0, WidgetContents[0]);
        });

        CancelBtn.onClick.AddListener(() =>
        {
            ParentPanel.CloseWidget<ConfirmWidget>(1, WidgetContents[0]);
        });
    }

    public override void RemoveListening()
    {
        base.RemoveListening();

        ConfirmBtn.onClick.RemoveAllListeners();

        CancelBtn.onClick.RemoveAllListeners();
    }

    public override void Refresh()
    {
        base.Refresh();
    }
}
