using TMPro;
using UIFramework.Base;
using UnityEngine;
using UnityEngine.UI;

public class FemalePanel : BasePanel
{
    private Button StartBtn;
    private Button SettingBtn;
    private Button CreditsBtn;
    private Button ExitBtn;
    private Image Image;

    public override void Init()
    {
        base.Init();
        StartBtn = transform.Find("StartPanel/BottomLeft/StartBtn").GetComponent<Button>();
        SettingBtn = transform.Find("StartPanel/BottomLeft/SettingBtn").GetComponent<Button>();
        CreditsBtn = transform.Find("StartPanel/BottomLeft/CreditsBtn").GetComponent<Button>();
        ExitBtn = transform.Find("StartPanel/BottomLeft/ExitBtn").GetComponent<Button>();
        Image = transform.Find("Background").GetComponent<Image>();
    }

    public override void Refresh()
    {
        base.Refresh();
        StartBtn.GetComponentInChildren<TextMeshProUGUI>().text = "开始";
        SettingBtn.GetComponentInChildren<TextMeshProUGUI>().text = "设置";
        CreditsBtn.GetComponentInChildren<TextMeshProUGUI>().text = "制作人员名单";
        ExitBtn.GetComponentInChildren<TextMeshProUGUI>().text = "退出";

        Image.sprite = Resources.Load<Sprite>("Art/界面/游戏开始界面（女）");
    }

    public override void AddListening()
    {
        base.AddListening();

        StartBtn.onClick.AddListener(() =>
        {
            BaseUIManager.Instance.ClosePanel<FemalePanel>();
            BaseUIManager.Instance.OpenPanel<LoadingPanel>();
        });
        //SettingBtn.onClick.AddListener(() => BaseUIManager.Instance.OpenPanel<SettingPanel>());

    }

    public override void RemoveListening()
    {
        base.RemoveListening();

        StartBtn.onClick.RemoveAllListeners();
    }
}
