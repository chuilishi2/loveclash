using Script.Manager;
using TMPro;
using UIFramework.Base;
using UnityEngine.UI;

public class GenderPanel : BasePanel
{
    private TextMeshProUGUI TextMeshProUGUI;
    private Button MaleBtn;
    private Button FemaleBtn;

    public override void Init()
    {
        base.Init();
        TextMeshProUGUI = transform.Find("GenderPanel/InfoText").GetComponent<TextMeshProUGUI>();
        MaleBtn = transform.Find("GenderPanel/MaleBtn").GetComponent<Button>();
        FemaleBtn = transform.Find("GenderPanel/FemaleBtn").GetComponent<Button>();
    }

    public override void AddListening()
    {
        base.AddListening();
        MaleBtn.onClick.AddListener(() =>
        {
            EventManager.Register<WidgetCloseEvent>(Listening);
            OpenWidget<ConfirmWidget>(0);
        });

        FemaleBtn.onClick.AddListener(() =>
        {
            EventManager.Register<WidgetCloseEvent>(Listening);
            OpenWidget<ConfirmWidget>(1);
        });
    }

    private void Listening(WidgetCloseEvent widgetCloseEvent)
    {
        print(widgetCloseEvent.widgetCloseInfo[0]);

        if (widgetCloseEvent.widgetCloseInfo[0] == 1) return;

        BaseUIManager.Instance.ClosePanel<GenderPanel>();

        if (widgetCloseEvent.widgetCloseInfo[1] == 0)
            BaseUIManager.Instance.OpenPanel<MalePanel>();
        else BaseUIManager.Instance.OpenPanel<FemalePanel>();
    }

    public override void RemoveListening()
    {
        base.RemoveListening();
        MaleBtn.onClick.RemoveAllListeners();
        FemaleBtn.onClick.RemoveAllListeners();
        
    }

    public override void Refresh()
    {
        base.Refresh();

        TextMeshProUGUI.text = PanelContents[0];

        MaleBtn.GetComponentInChildren<TextMeshProUGUI>().text = PanelContents[1];

        FemaleBtn.GetComponentInChildren<TextMeshProUGUI>().text = PanelContents[2];
    }
}
