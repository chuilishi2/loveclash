using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;


namespace Script.UI
{
    
    public class UIManager : MonoBehaviour
    {
        private static UIManager _instance;
        public static UIManager Instance => _instance;
        public Dictionary<Sprite, Scene> Scenes;
        // 面板共用的摄像机
        public Camera mainCamera;
        private RaycastHit _hitInfo;
        /*
         [Serializable]
        public struct TransitionUIStruct
        {
            public SceneAsset scene;
            public GameObject sprite;
        }*/
        public List<SceneRoutes.TransitionUIStruct> transitionList;
        public int currentScene;
        
        
        private void Awake()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
            }

            _instance = this;
        }

        private void Start()
        {
            // TODO：这里需要修改，这个4是起始UI界面
            currentScene = 4;
            ChangeUIPanel();
        }

        // Update is called once per frame
        void Update()
        {
            UIRayCheck();
            ActiveTransitionUI();
        }

        /// <summary>
        /// 修改UI界面
        /// </summary>
        public void ChangeUIPanel()
        {
            //获取场景
            Scene gameScene = SceneManager.GetSceneByBuildIndex(currentScene);
            SceneManager.SetActiveScene(gameScene);
            GameObject obj = gameScene.GetRootGameObjects().FirstOrDefault(x => x.name == "Canvas");
            if (obj != null)
            {
                obj.GetComponent<SceneRoutes>().CopySceneInformation(transitionList);
                // 这里地方很重要没有公用摄像机 摄像机的射线会检测不到对应的UI
                obj.GetComponent<Canvas>().worldCamera = mainCamera;
                
            }
            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(3));
        }

        /// <summary>
        /// UI射线检测方法
        /// </summary>
        void UIRayCheck()
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(ray, out _hitInfo))
            {
                
            }
        }
        
        /// <summary>
        /// 启动跳转 只对TransitionUI标签的UI有效
        /// </summary>
        void ActiveTransitionUI()
        {
            
            if (Input.GetMouseButtonDown(0) && _hitInfo.collider != null)
            {
                // 通过ui射线检测返回的RayCastHit经过Collider获取跳转UI
                GameObject ui = _hitInfo.collider.gameObject;
                UITransition transitionObj =  ui.GetComponent<UITransition>();
                
                if (ui.CompareTag("TransitionUI"))
                {
                    transitionObj.SyncTransition();
                }            
            }
        }
    }
}
