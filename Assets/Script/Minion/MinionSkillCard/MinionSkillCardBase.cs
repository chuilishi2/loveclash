using Script.Cards;
using Script.core;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MinionSkillCardBase : CardBase 
{
    public override void Init()
    {
        base.Init();

        GetMinionTableItemById();

        transform.Find("NameIcon").GetComponentInChildren<TextMeshProUGUI>().text = minionSkillTableItem.minionSkillName;
        transform.Find("Description").GetComponent<TextMeshProUGUI>().text = minionSkillTableItem.description;

        //添加 Effect 脚本
        gameObject.AddComponent(("Effect" + minionSkillTableItem.id.ToString()).GetType());
    }

    /// <summary>
    /// 随从技能卡
    /// </summary>
    private MinionSkillTableItem minionSkillTableItem;
    public MinionSkillTableItem MinionSkillTableItem
    {
        get { return minionSkillTableItem; }
        set { minionSkillTableItem = value; }
    }

    protected void GetMinionTableItemById()
    {
        MinionSkillTableItem = MinionSkillTable.Instance.GetItemById(CardId);
    }

    protected virtual void Awake()
    {
        CardId = 4001;
        //GetMinionTableItemById();
    }

    protected virtual void Start()
    {
        //transform.Find("NameIcon").GetComponentInChildren<TextMeshProUGUI>().text = minionSkillTableItem.minionSkillName;
        //transform.Find("Description").GetComponent<TextMeshProUGUI>().text = minionSkillTableItem.decription;
    }
}
