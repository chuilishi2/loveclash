/// <summary>
/// 节制的技能卡：自我控制
/// </summary>
public class SelfControl : MinionSkillCounterCardEffect
{
    /// <summary>
    /// 卡牌策划说不会出现反制牌被反制的情况了，因此这里大概不会执行，留空都可以
    /// </summary>
    public override void CounteredEffect()
    {

    }

    public override void NormalEffect()
    {
        //栈中这里先于被反制牌执行，所以可以直接操作被反制牌 Effect 的数据，改变它的表现效果
        GameProcessor.CounterInfo.CurCard.GetComponent<EffectBase>().Disable = true;
    }
}
