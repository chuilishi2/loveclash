using Script.Cards;
using System;
using TMPro;
using UnityEngine;

public class MinionCardBase : CardBase
{
    public override void Init()
    {
        base.Init();

        GetMinionTableItemById();

        transform.Find("NameIcon").GetComponentInChildren<TextMeshProUGUI>().text = minionTableItem.minionName;
        transform.Find("Description").GetComponent<TextMeshProUGUI>().text = minionTableItem.description;

        //添加 Effect 脚本
        gameObject.AddComponent(("Effect" + minionTableItem.id.ToString()).GetType());
    }

    private MinionTableItem minionTableItem;
    public MinionTableItem MinionTableItem
    {
        get { return minionTableItem; }
        set { minionTableItem = value; }
    }

    [SerializeField]
    /// <summary>
    /// 随从技能卡
    /// </summary>
    private int minionSkillTableId;
    public int MinionSkillTableId
    {
        get { return minionSkillTableId; }
        set { minionSkillTableId = value; }
    }

    private MinionSkillTableItem minionSkillTableItem;
    public MinionSkillTableItem MinionSkillTableItem
    {
        get
        {
            if (minionTableItem.minionSkillCardId>0)
            {
                if (minionSkillTableItem != null)
                {
                    return minionSkillTableItem;
                }
                else
                {
                    Debug.LogError("当前随从卡 skillCard 为 true 但随从技能卡为空");
                }
            }
            return null;
        }
        set { minionSkillTableItem = value; }
    }

    /// <summary>
    /// 卡牌功能类型枚举，暂时没用上
    /// </summary>
    public enum FunctionType
    {
        /// <summary>
        /// 自身效果
        /// </summary>
        Myself,
        /// <summary>
        /// 对方效果
        /// </summary>
        Opponent,
        /// <summary>
        /// 全局效果
        /// </summary>
        Whole,
        /// <summary>
        /// 随从类型
        /// </summary>
        Minion,
        /// <summary>
        /// 卡牌类型
        /// </summary>
        Card
    }

    protected void GetMinionTableItemById()
    {
        minionTableItem = MinionTable.Instance.GetItemById(CardId);
    }

    protected virtual void Awake()
    {
        CardId = 3001;
        //GetMinionTableItemById();
    }

    protected virtual void Start()
    {
        //transform.Find("NameIcon").GetComponentInChildren<TextMeshProUGUI>().text = minionTableItem.minionName;
        //transform.Find("Description").GetComponent<TextMeshProUGUI>().text = minionTableItem.description;
    }
}