using Script.Cards;
using System.Collections.Generic;
using UnityEngine;

public abstract class MinionCounterCardEffect : MinionCardEffectBase, ICounter
{
    [SerializeField]
    private List<int> canCounterList;
    public List<int> CanCounterList { get => canCounterList; set => canCounterList = value; }

    public virtual bool CanCounter(CardBase cardBase)
    {
        if (CanCounterList.Contains(cardBase.CardId / 1000) || CanCounterList.Contains(cardBase.CardId))
        {
            return true;
        }
        return false;
    }
}
