using Script.Manager;
using Script.view;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MinionCardBase))]
public abstract class MinionCardEffectBase : EffectBase
{
    protected int round;
    protected int remaindRound;

    /// <summary>
    /// 防止重复生效的参数，涉及到比如倍率、监听时可以用
    /// </summary>
    protected bool init;

    /// <summary>
    /// 本回合是否已经用过——似乎不太需要
    /// </summary>
    protected bool used;

    public override void Trigger(List<int> targetIds)
    {
        if (GetComponent<MinionCardBase>().MinionTableItem.round != 0)
        {
            RoundJudge();
            OnTriggerEffect();
        }
        else
        {
            OnTriggerEffect();
        }
    }

    protected override void Awake()
    {
        base.Awake();
        round = GetComponent<MinionCardBase>().MinionTableItem.round;
        remaindRound = GetComponent<MinionCardBase>().MinionTableItem.remaindRound;
        EventManager.Register<TurnOverEvent>(RoundCount);
    }

    /// <summary>
    /// 回合数计算
    /// </summary>
    protected void RoundCount(TurnOverEvent turnOverEvent)
    {
        remaindRound = Mathf.Max(0, --remaindRound);
        used = false;
    }

    /// <summary>
    /// 获取随从技能卡
    /// </summary>
    /*protected void DrawMinionSkillCard()
    {
        int id = GetComponent<MinionCardBase>().MinionTableItem.minionSkillCardId;
        if (id != 0)
        {
            //获取卡牌的行为可能不完整
            UIManager.instance.myselfView.DrawCard<MinionSkillCardBase>(GetComponent<ICardView>(), id);
        }
    }*/

    /// <summary>
    /// 回合数判定，剩余回合数为 0 则 disable 为 false
    /// </summary>
    protected void RoundJudge()
    {
        if (remaindRound < 0)
        {
            remaindRound = round;
            disable = false;
        }
        else
        {
            disable = true;
        }
    }

    protected override void OnTriggerEffect()
    {
        if (!used) used = true;
        else return;
        if (!disable) { NormalEffect(); }//DrawMinionSkillCard(); }
        else { CounteredEffect(); }
    }
}
