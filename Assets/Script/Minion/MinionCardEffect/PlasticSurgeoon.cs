using Script.core;
using Script.Manager;
using UnityEngine;

/// <summary>
/// 整形医生
/// </summary>
public class PlasticSurgeoon : MinionCardEffectBase
{
    public override void CounteredEffect()
    {
        if (!init) init = true;
        else return;
        GameManager.instance.Opponent.上头值倍率 *= ((int)GameManager.instance.Opponent.性别 + 1);
    }

    public override void NormalEffect()
    {
        if (init) init = false;
        else return;
        GameManager.instance.Opponent.上头值倍率 /= ((int)GameManager.instance.Opponent.性别 + 1);
    }
}
