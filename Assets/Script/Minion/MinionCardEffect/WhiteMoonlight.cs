using Script.core;
using Script.Manager;
using Script.Network;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 白月光
/// </summary>
public class WhiteMoonlight : MinionCardEffectBase
{
    public int value = 1;

    private void func(TurnOverEvent turnOverEvent)
    {
        GameManager.instance.Myself.心动值 -= value;
        GameManager.instance.Myself.上头值 -= value;
    }

    public override void NormalEffect()
    {
        EventManager.Register<TurnOverEvent>(func);
    }

    public override void CounteredEffect()
    {
        EventManager.Unregister<TurnOverEvent>(func);
    }
}

