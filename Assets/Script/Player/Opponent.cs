﻿using Script.Manager;
using System;
using UnityEngine;

namespace Script.core
{
    public class Opponent : PlayerBase
    {
        public static Opponent instance;

        public override void Awake()
        {
            base.Awake();
            instance = this;
        }
    }
}