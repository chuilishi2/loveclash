﻿using Cysharp.Threading.Tasks;
using Script.Cards;
using Script.Character;
using Script.Manager;
using System;
using UnityEngine;

namespace Script.core
{
    public class Myself : PlayerBase
    {
        public static Myself instance;

        public override void Awake()
        {
            Debug.Log("myself", gameObject);
            base.Awake();
            instance = this;
            var characterType = Type.GetType(characterName);
            if (characterType == null)
            {
                var type = Type.GetType("Script.Character." + characterName);
                if (type == null) Debug.LogError("Character名称不正确");
                else
                {
                    var component = gameObject.AddComponent(type);
                    character = (CharacterBase)component;
                }
            }
        }
        #region 抽卡
        /// <summary>
        /// 抽n张卡
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public override async UniTask<NetworkObject> DrawCard()
        {
            //这是牌库版本的老代码 
            // var card = Deck.instance.DrawCard();
            // if (card == null)
            // {
            //     Debug.Log("没牌了");
            //     return;
            // }
            // handCards.Add(card);
            // UIManager.instance.playerView.DrawCard(card);
            //Choose a Random Value from ObjectEnum
            return await character.DrawCard();
        }
        #endregion
    }
}
