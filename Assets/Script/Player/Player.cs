using Script.Character;
using Script.core;
using Script.Manager;
using Script.Network;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : PlayerBase
{
    public PlayerEnum playerEnum;

    private void Start()
    {
        if (playerEnum == NetworkManager.instance.playerEnum)
            this.BindView(UIManager.instance.myselfView);
        else
            this.BindView(UIManager.instance.opponentView);
    }
}
