using Script.core;
using Script.view;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerBaseExtension
{
    public static void BindView(this PlayerBase player, PlayerView view)
    {
        player.心动值变化 += view.刷新心动值;
        player.信任值变化 += view.刷新信任值;
        player.上头值变化 += view.刷新上头值;
        player.心动值倍率变化 += view.刷新心动值倍率;
        player.信任值倍率变化 += view.刷新信任值倍率;
        player.上头值倍率变化 += view.刷新上头值倍率;
        player.性别变化 += view.刷新性别;
    }

    public static void UnbindView(this PlayerBase player, PlayerView view)
    {
        player.心动值变化 -= view.刷新心动值;
        player.信任值变化 -= view.刷新信任值;
        player.上头值变化 -= view.刷新上头值;
        player.心动值倍率变化 -= view.刷新心动值倍率;
        player.信任值倍率变化 -= view.刷新信任值倍率;
        player.上头值倍率变化 -= view.刷新上头值倍率;
        player.性别变化 -= view.刷新性别;
    }
}
