﻿using Cysharp.Threading.Tasks;
using Script.Cards;
using Script.Character;
using Script.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Script.core
{
    public abstract class PlayerBase : NetworkObject
    {
        [HideInInspector]
        public CharacterBase character;
        [Header("角色名称,一定要是准确的全名")]
        public string characterName;

        //限制出牌的信息
        public RestrictCardEffect _RestrictCardEffect;
        

        public UnityAction<int> 心动值变化;
        public UnityAction<int> 信任值变化;
        public UnityAction<int> 上头值变化;
        public UnityAction<float> 心动值倍率变化;
        public UnityAction<float> 信任值倍率变化;
        public UnityAction<float> 上头值倍率变化;
        public UnityAction<Gender> 性别变化;

        #region 三个值, 三个倍率
        [SerializeField]
        private int _心动值;
        [SerializeField]
        private int _信任值;
        [SerializeField]
        private int _上头值;
        [SerializeField]
        private float _心动值倍率 = 1;
        [SerializeField]
        private float _信任值倍率 = 1;
        [SerializeField]
        private float _上头值倍率 = 1;
        [SerializeField]
        private Gender _性别 = Gender.Male;

        public int 心动值
        {
            get => _心动值;
            set
            {
                if (value > 99)
                {
                    throw new Exception("心动值太大");
                }
                else
                {
                    _心动值 = value;
                    心动值变化?.Invoke(value);
                }
            }
        }
        public int 信任值
        {
            get => _信任值;
            set
            {
                if (value > 99)
                {
                    throw new Exception("信任值太大");
                }
                else
                {
                    _信任值 = value;
                    信任值倍率变化?.Invoke(value);
                }
            }
        }
        public int 上头值
        {
            get => _上头值;
            set
            {
                if (value > 99)
                {
                    throw new Exception("上头值太大");
                }
                else
                {
                    _上头值 = value;
                    上头值变化?.Invoke(value);
                }
            }
        }

        public float 心动值倍率
        {
            get => _心动值倍率;
            set
            {
                if (value > 99)
                {
                    throw new Exception("心动值倍率太大");
                }
                else
                {
                    _心动值倍率 = value;
                    心动值倍率变化?.Invoke(value);
                }
            }
        }

        public float 信任值倍率
        {
            get => _信任值倍率;
            set
            {
                if (value > 99)
                {
                    throw new Exception("信任值倍率太大");
                }
                else
                {
                    _信任值倍率 = value;
                    信任值倍率变化?.Invoke(value);
                }
            }
        }

        public float 上头值倍率
        {
            get => _上头值倍率;
            set
            {
                if (value > 99)
                {
                    throw new Exception("上头值倍率太大");
                }
                else
                {
                    _上头值倍率 = value;
                    上头值倍率变化?.Invoke(value);
                }
            }
        }

        public Gender 性别
        {
            get => _性别;
            set
            {
                _性别 = value;
                性别变化?.Invoke(value);
            }
        }

        #endregion

        /*
       //键值考虑用 string、卡牌基类
       //期望效果：get<Minion>(id) 可得到卡牌 card 对象

       //泛型解释：
       //第一层：卡牌基类 List
       //第二层：id -> 卡牌基类 List
       //第三层：卡牌具体类型 -> id -> 卡牌基类 List
       public Dictionary<Type, Dictionary<int, List<CardBase>>> allCards = new Dictionary<Type, Dictionary<int, List<CardBase>>>();
       public void AddCards<T>(int id, T card) where T : CardBase
       {
           Type type = typeof(T);
           if (!allCards.ContainsKey(type))
           {
               allCards.Add(type, new Dictionary<int, List<CardBase>>());
           }
           if (!allCards[type].ContainsKey(id))
           {
               allCards[type].Add(id, new List<CardBase>());
           }
           allCards[type][id].Add(card);

           card.Init();
       }*/

        private Dictionary<int, List<CardBase>> handCards = new Dictionary<int, List<CardBase>>()
        {
            {1, new List<CardBase>() {} },
            {2, new List<CardBase>() {} },
            {3, new List<CardBase>() {} },
            {4, new List<CardBase>() {} }
        };
        public Dictionary<int, List<CardBase>> HandCards { get => handCards; set => handCards = value; }

        private List<CardBase> allHandCards = new List<CardBase>();
        public List<CardBase> AllHandCards { get => allHandCards; set => allHandCards = value; }

        List<ICounter> counters = new List<ICounter>();

        public void AddCard(CardBase cardBase)
        {
            if(!handCards.ContainsKey(cardBase.CardId / 1000))
                handCards[cardBase.CardId / 1000] = new List<CardBase>();
            handCards[cardBase.CardId / 1000].Add(cardBase);

            allHandCards.Add(cardBase);

            if (cardBase.GetComponent<EffectBase>() is ICounter counter)
                counters.Add(counter);
        }

        public void RemoveCard(CardBase cardBase)
        {
            handCards[cardBase.CardId / 1000].Remove(cardBase);

            allHandCards.Remove(cardBase);

            if (cardBase.GetComponent<EffectBase>() is ICounter counter)
                counters.Remove(counter);
        }

        #region 获取卡牌
        public List<CardBase> GetCards(int index)
        {
            return handCards[index];
        }

        public List<CardBase> GetBaseCards()
        {
            return handCards[1];
        }

        public List<CardBase> GetFuncionCards()
        {
            return handCards[2];
        }

        public List<CardBase> GetMinionCards()
        {
            return handCards[3];
        }

        public List<CardBase> GetMinionSkillCards()
        {
            return handCards[4];
        }

        public List<CardBase> GetAllCards()
        {
            return allHandCards;
        }
        #endregion

        /// <summary>
        /// 获取对方的反制牌
        /// </summary>
        /// <returns></returns>
        public List<ICounter> GetCounterCard()
        {
            if (GameProcessor.CurPlayer == GameManager.instance.Myself.playerEnum)
                return GameManager.instance.Opponent.counters;
            else
                return GameManager.instance.Myself.counters;
        }
        
        /// <summary>
        /// 性别枚举
        /// </summary>
        public enum Gender
        {
            Male,
            Female
        }

        public virtual void Awake()
        {
            var characterType = Type.GetType(characterName);
            if (characterType == null)
            {
                var type = Type.GetType("Script.Character." + characterName);
                if (type == null) Debug.LogError("Character名称不正确");
                else
                {
                    var component = gameObject.AddComponent(type);
                    character = (CharacterBase)component;
                }
            }
        }

        public virtual void PlayCard(int card, List<int> targets)
        {
            character.PlayCard(card, targets);
        }

        public virtual async UniTask<NetworkObject> DrawCard()
        {
            return await character.DrawCard();
        }
    }
}