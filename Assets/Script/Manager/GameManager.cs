﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using Script.Cards;
using Script.Character;
using Script.core;
using Script.Effects.其他Effect;
using Script.Network;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace Script.Manager
{
    [RequireComponent(typeof(OperationExecutor))]
    public class GameManager : NetworkObject
    {
        [SerializeField]
        private Player myself;
        [SerializeField]
        private Player opponent;

        // 这个是相对的，主要是给玩家接管对方时出牌的效果不会出问题
        public Player Myself
        {
            get
            {
                if (myself.playerEnum == NetworkManager.instance.playerEnum)
                {
                    return myself;
                }
                return opponent;
            }
        }

        // 这个是相对的，主要是给玩家接管对方时出牌的效果不会出问题
        public Player Opponent
        {
            get
            {
                if (opponent.playerEnum != NetworkManager.instance.playerEnum)
                {
                    return opponent;
                }
                return myself;
            }
        }

        public Player LocalPlayer => myself;

        public static GameManager instance;
        public PlayerEnum curPlayer = PlayerEnum.Player1;

        private void Awake()
        {
            instance = this;
        }
        #region 游戏流程
        public void Main(bool isOnline = true)
        {
            if (isOnline == false)
            {
                NetworkManager.isOnline = false;
                NetworkManager.instance.playerEnum = PlayerEnum.Player1; // 单机自己就是一号玩家
            }

            myself.playerEnum = NetworkManager.instance.playerEnum;
            opponent.playerEnum = NetworkManager.instance.playerEnum == PlayerEnum.Player1 ? PlayerEnum.Player2 : PlayerEnum.Player1;
            GameProcessor.Start();
        }

        public void EndGame()
        {
            //TODO
        }
        #endregion

        private void Update()
        {
            curPlayer = NetworkManager.instance.playerEnum;
        }

        private MinionTable minionTable;

        public MinionTable GetMinionTable()
        {
            if (minionTable == null)
            {
                minionTable = MinionTable.Instance;
            }
            return minionTable;
        }

        private MinionSkillTable minionSkillTable;

        public MinionSkillTable GetMinionSkillTable()
        {
            if (minionSkillTable == null)
            {
                minionSkillTable = MinionSkillTable.Instance;
            }
            return minionSkillTable;
        }

        public Player GetPlayer(PlayerEnum playerEnum)
        {
            if (Myself.playerEnum == playerEnum)
                return Myself;
            else
                return Opponent;
        }

        /// <summary>
        /// 给非网络对象用的发包接口
        /// </summary>
        /// <param name="operation"></param>
        public void ExecuteOperation(Operation operation)
        {
            Execute(operation);
        }
    }
}