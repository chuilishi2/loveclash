using Script.core;
using Script.Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableManager : NetworkObject
{
    public static TableManager instance;

    private void Awake()
    {
        instance = this;
        GetBaseCardTable();
        GetMinionTable();
        GetMinionSkillTable();
    }

    private BaseCardTable basecardTable;

    public BaseCardTable GetBaseCardTable()
    {
        if (basecardTable == null)
        {
            basecardTable = BaseCardTable.Instance;
        }
        return basecardTable;
    }

    private MinionTable minionTable;

    public MinionTable GetMinionTable()
    {
        if (minionTable == null)
        {
            minionTable = MinionTable.Instance;
        }
        return minionTable;
    }

    private MinionSkillTable minionSkillTable;

    public MinionSkillTable GetMinionSkillTable()
    {
        if (minionSkillTable == null)
        {
            minionSkillTable = MinionSkillTable.Instance;
        }
        return minionSkillTable;
    }

    //private CounterTable counterTable;

    //public CounterTable GetCounterTable()
    //{
    //    if (counterTable == null)
    //    {
    //        counterTable = Resources.Load<CounterTable>("TableData/CounterTable");
    //    }
    //    return counterTable;
    //}
}
