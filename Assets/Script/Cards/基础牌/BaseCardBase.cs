using TMPro;

namespace Script.Cards
{
    /// <summary>
    /// 基础卡
    /// </summary>
    public class BaseCardBase : CardBase
    {
        public override void Init()
        {
            base.Init();

            GetBaseCardTableItemById();

            transform.Find("NameIcon").GetComponentInChildren<TextMeshProUGUI>().text = basecardTableItem.basecardName;
            transform.Find("Description").GetComponent<TextMeshProUGUI>().text = basecardTableItem.description;

            //添加 Effect 脚本
            gameObject.AddComponent(("Effect" + basecardTableItem.id.ToString()).GetType());

            //TODO 还差从 basecardTableItem 读取加载三个基础值、文本类型
        }

        private BaseCardTableItem basecardTableItem;

        public BaseCardTableItem BasecardTableItem
        {
            get { return basecardTableItem; }
            set { basecardTableItem = value; }
        }

        /// <summary>
        /// 卡牌功能类型枚举，暂时没用上
        /// </summary>
        public enum FunctionType
        {
            /// <summary>
            /// 自身效果
            /// </summary>
            Myself,
            /// <summary>
            /// 对方效果
            /// </summary>
            Opponent,
            /// <summary>
            /// 全局效果
            /// </summary>
            Whole,
            /// <summary>
            /// 卡牌类型
            /// </summary>
            Card
        }

        protected void GetBaseCardTableItemById()
        {
            basecardTableItem = TableManager.instance.GetBaseCardTable().GetItemById(CardId);
        }

        protected void Awake()
        {
            CardId = 1001;
            //GetBaseCardTableItemById();
        }

        protected virtual void Start()
        {
            //transform.Find("NameIcon").GetComponentInChildren<TextMeshProUGUI>().text = basecardTableItem.basecardName;
            //transform.Find("Description").GetComponent<TextMeshProUGUI>().text = basecardTableItem.description;
        }
    }
}