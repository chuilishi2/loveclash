using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;
// 基础牌文本类型
// 文本类型多的话，可能还是读文件，批量创建去映射
public class BaseCardTextType
{
    public const string DatingAtmosphereValueMax = "DatingAtmosphereValueMax"; // 约会氛围值max类  1
    public const string Sports = "Sports"; // 运动类  2
    public const string HeartToHeart = "HeartToHeart"; // 交心类  3
    public const string GoOutToPlay = "GoOutToPlay"; // 出去玩类  4
    public const string Accompany = "Accompany"; // 陪伴类  5
    public const string ItFeelsVeryAmbiguous = "ItFeelsVeryAmbiguous"; // 感觉很暧昧的  6

    public static string getTypeString(int id)
    {
        switch(id)
        {
            case 1:
                return "DatingAtmosphereValueMax";
            case 2:
                return "Sports";
            case 3:
                return "HeartToHeart";
            case 4:
                return "GoOutToPlay";
            case 5:
                return "Accompany";
            case 6:
                return "ItFeelsVeryAmbiguous";
            default:
                return null;
        }
    }
    public static int getTypeId(string type)
    {
        switch (type)
        {
            case "DatingAtmosphereValueMax":
                return 1;
            case "Sports":
                return 2;
            case "HeartToHeart":
                return 3;
            case "GoOutToPlay":
                return 4;
            case "Accompany":
                return 5;
            case "ItFeelsVeryAmbiguous":
                return 6;
            default:
                return -1;
        }
    }

}