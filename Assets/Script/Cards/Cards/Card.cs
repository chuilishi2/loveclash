﻿using VMFramework.GameLogicArchitecture;

namespace LoveClash.Cards
{
    public abstract class Card : VisualGameItem, ICard
    {
        public abstract void Execute(CardExecutionInfo info);

        public abstract void ClientExecute(CardExecutionInfo info);
    }
}