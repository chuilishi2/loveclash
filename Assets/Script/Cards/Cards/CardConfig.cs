﻿using System;
using VMFramework.GameLogicArchitecture;

namespace LoveClash.Cards
{
    public class CardConfig : DescribedGamePrefab
    {
        protected override string idSuffix => "card";

        public override Type gameItemType => typeof(Card);
    }
}