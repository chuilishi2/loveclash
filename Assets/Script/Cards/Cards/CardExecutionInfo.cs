﻿using System.Collections.Generic;

namespace LoveClash.Cards
{
    public struct CardExecutionInfo
    {
        public Player user;
        public IEnumerable<Card> targetCards;
        public Player targetPlayer;
    }
}