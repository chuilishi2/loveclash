using Script.Cards;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// 反制牌基类，弃用
/// </summary>
public class CounterCardBase : CardBase//, ICounter
{
    [SerializeField]
    private int counterTableId;
    public int CounterTableId
    {
        get => counterTableId; set => counterTableId = value;
    }

    [SerializeField]
    private List<int> canCounterList = new List<int>();
    public List<int> CanCounterCard { get => canCounterList; }

    private CounterTableItem counterTableItem;
    public CounterTableItem CounterTableItem
    {
        get => counterTableItem; set => counterTableItem = value;
    }
   
    //弃用
    public enum CounterType
    {
        AnyCard,
        BaseCard,
        MinionCard,
        MinionSkillCard,
        /// <summary>
        /// 反制牌被无效时变成 Null
        /// </summary>
        Null
    }

    //protected CounterTableItem GetCounterTableItemById(int id)
    //{
    //    return TableManager.instance.GetCounterTable().GetItemById(id);
    //}

    //protected void GetCounterTableItemById()
    //{
    //    CounterTableItem = TableManager.instance.GetCounterTable().GetItemById(CounterTableId);
    //}

    protected virtual void Awake()
    {

    }

    protected virtual void Start()
    {
        //GetCounterTableItemById();
        transform.Find("NameIcon").GetComponentInChildren<TextMeshProUGUI>().text = CounterTableItem.counterName;
        transform.Find("Description").GetComponent<TextMeshProUGUI>().text = CounterTableItem.decription;
    }

    public virtual void Listen()
    {
        
    }

    public virtual void Counter(PlayCardEvent playCardEvent)
    {

    }

    public virtual bool CanCounter(CardBase cardBase)
    {
        if (canCounterList.Contains(cardBase.CardId / 1000) || canCounterList.Contains(cardBase.CardId))
        {
            return true;
        }
        return false;
    }

    public void EarlierEffect(CardEffectEvent cardEffectEvent)
    {
        
    }

    public void LaterEffect(CardEffectEvent cardEffectEvent)
    {
        
    }
}

/*public interface ICounter
{
    //List<int> CanCounterList { get; set; }

    bool CanCounter(CardBase cardBase);
}*/