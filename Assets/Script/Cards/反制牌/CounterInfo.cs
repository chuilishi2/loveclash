using Script.Cards;
using Script.Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CounterInfo
{
    public PlayerEnum originPlayerTurn;
    public Stack<(CardBase card, List<int> targets)> counterCards = new();
    public CardBase CurCard
    {
        get
        {
            if (counterCards.Count == 0)
                return null;
            return counterCards.Peek().card;
        }
    }

    public void Push(CardBase card, List<int> targets)
    {
        counterCards.Push((card, targets));
    }

    public (CardBase card, List<int> targets) GetCard()
    {
        if (counterCards.Count == 0)
            return default;
        return counterCards.Pop();
    }

    public void Clear()
    {
        counterCards.Clear();
    }
}
