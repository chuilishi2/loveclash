﻿using System;
using System.Collections.Generic;
using Script.core;
using Script.Manager;
using Script.Network;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Script.Cards
{
    [Serializable]
    public abstract class CardBase : NetworkObject
    {
        [SerializeField]
        private int cardId;
        /// <summary>
        /// CardId：牌型 id * 1000 + 牌型内索引，牌型 id 为个位数
        /// </summary>
        public int CardId { get => cardId; 
            set 
            {
                cardId = value;
                Init();
            } }

        public virtual void Init()
        {

        }

        public virtual void Destroy()
        {
            EventManager.UnregisterTarget(this);
        }

        public virtual void Execute(core.PlayerBase player, List<int> targetIds = null)
        {
            GetComponent<EffectTrigger>().TriggerEffects(targetIds);
        }

        public void PlayCard()
        {
            Execute(new Operation(OperationType.Card, networkId));
        }

        public bool CanPlayCard()
        {
            if (GameProcessor.State == GameState.RunningTurn) // 自己回合可以出牌
            {
                return true;
            }
            else if (GameProcessor.State == GameState.CounterTurn // 反制回合且是可以反制目标牌的情况下也可以出
                && GetComponent<EffectBase>() is ICounter counter
                && counter.CanCounter(GameProcessor.CounterInfo.CurCard))
            {
                return true;
            }
            if(GameManager.instance.Myself._RestrictCardEffect!=null)
            {
                //限制只能出某些牌
                if (GameManager.instance.Myself._RestrictCardEffect.can && (
                    checkType(GameManager.instance.Myself._RestrictCardEffect.RestrictCardType) ||
                    checkID(GameManager.instance.Myself._RestrictCardEffect.RestrictCardIdList) ||
                    checkBaseType(GameManager.instance.Myself._RestrictCardEffect.RestrictBaseTextType)))
                {
                    return true;
                }
                else //限制只能出某些牌
                if(GameManager.instance.Myself._RestrictCardEffect.can|| (
                    checkType(GameManager.instance.Myself._RestrictCardEffect.RestrictCardType) ||
                    checkID(GameManager.instance.Myself._RestrictCardEffect.RestrictCardIdList) ||
                    checkBaseType(GameManager.instance.Myself._RestrictCardEffect.RestrictBaseTextType)))
                {
                    return true;
                }
            }



            // 其他情况回到手牌
            return false;
            //CardView 中：await ResetPosition();
        }
        /// <summary>
        /// 检查该卡牌是不是这个类型，是返回true，不是返回false
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool checkType(string type)
        {
            if (type == null)
                return true;

            if (GetComponent(Type.GetType(type)) == null)
                return true;
            else
                return false;
        }
        /// <summary>
        /// 检查该卡牌是不是这个类型的
        /// </summary>
        /// <param name="TextType"></param>
        /// <returns></returns>
        public bool checkBaseType(string TextType)
        {
            if (TextType == null)
                return true;

            if (GetComponent<BaseCardBase>()==null)
                return false;

            if (!GetComponent<BaseCardBase>().BasecardTableItem.basecardTextType.Equals(TextType))
                return false;

            return true;

        }
        /// <summary>
        /// 检查该卡牌是不是限制的卡牌列表
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public bool checkID(List<int> ids)
        {
            if(ids.Count==0||ids==null)
                return true;

            if (ids.Contains(CardId))
                return false;

            return true ;
        }
    }
}