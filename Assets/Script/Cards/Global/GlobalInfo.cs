using Script.Cards;
using Script.Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalInfo : MonoBehaviour
{
    private CardBase _globalCard;
    public CardBase GlobalCard { get => _globalCard; set => _globalCard = value; }

    private Queue<int> _globalInfoQueue = new Queue<int>();
    public Queue<int> GlobalInfoQueue { get => _globalInfoQueue; set => _globalInfoQueue = value; }

    private Queue<PlayerEnum> _globalPlayers;
    public Queue<PlayerEnum> GlobalPlayers { get => _globalPlayers; set => _globalPlayers = value; }

    /// <summary>
    /// 记录最开始令游戏进入 Global 的玩家
    /// </summary>
    private PlayerEnum _originPlayerTurn;
    public PlayerEnum OriginPlayer { get => _originPlayerTurn; set => _originPlayerTurn = value; }

    public void Clear()
    {
        GlobalInfoQueue.Clear();
    }
}
