using Script.Cards;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FunctionCardBase : CardBase
{
    public override void Init()
    {
        base.Init();

        GetFunctionTableItemById();

        transform.Find("NameIcon").GetComponentInChildren<TextMeshProUGUI>().text = FunctionTableItem.functionName;
        transform.Find("Description").GetComponent<TextMeshProUGUI>().text = FunctionTableItem.description;

        //���� Effect �ű�
        gameObject.AddComponent(("Effect" + FunctionTableItem.id.ToString()).GetType());
    }
     
    private FunctionTableItem functionTableItem;
    public FunctionTableItem FunctionTableItem
    {
        get { return functionTableItem; }
        set { functionTableItem = value; }
    }

    protected void GetFunctionTableItemById()
    {
        functionTableItem = FunctionCardTable.Instance.GetItemById(CardId);
    }

    protected void Awake()
    {
        GetFunctionTableItemById();
    }

    protected virtual void Start()
    {
        transform.Find("NameIcon").GetComponentInChildren<TextMeshProUGUI>().text = functionTableItem.functionName;
        transform.Find("Description").GetComponent<TextMeshProUGUI>().text = functionTableItem.description;
    }
}
