﻿using LoveClash.Entities;
using VMFramework.GameLogicArchitecture;

namespace LoveClash.GameCore
{
    public class GameSettingFile : GameCoreSettingBaseFile
    {
        public EntityGeneralSetting entityGeneralSetting;
        public PlayerGeneralSetting playerGeneralSetting;
        public CharacterGeneralSetting characterGeneralSetting;
    }
}
