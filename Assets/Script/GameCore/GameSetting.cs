﻿using LoveClash.Entities;
using VMFramework.GameLogicArchitecture;

namespace LoveClash.GameCore
{
    public class GameSetting : GameCoreSettingBase
    {
        public static GameSettingFile gameSettingFile => (GameSettingFile)gameCoreSettingsFileBase;
        
        public static EntityGeneralSetting entityGeneralSetting => gameSettingFile.entityGeneralSetting;
        
        public static PlayerGeneralSetting playerGeneralSetting => gameSettingFile.playerGeneralSetting;
        
        public static CharacterGeneralSetting characterGeneralSetting => gameSettingFile.characterGeneralSetting;
    }
}
