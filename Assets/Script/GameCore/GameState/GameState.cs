﻿namespace LoveClash.GameCore
{
    /// <summary>
    /// 游戏对局的状态
    /// </summary>
    public enum GameState
    {
        /// <summary>
        /// 无效状态
        /// </summary>
        Invalid,
        /// <summary>
        /// 游戏开始
        /// </summary>
        GameStart, 
        /// <summary>
        /// 回合开始
        /// </summary>
        TurnStart,
        /// <summary>
        /// 回合中
        /// </summary>
        TurnRunning,
        /// <summary>
        /// 交互状态
        /// </summary>
        TurnInteracting,
        /// <summary>
        /// 反制阶段
        /// </summary>
        TurnCounter,
        /// <summary>
        /// 回合结束
        /// </summary>
        TurnEnd,
        /// <summary>
        /// 游戏结束
        /// </summary>
        GameOver
    }
}