﻿using System;
using System.Collections.Generic;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using LoveClash.Entities;
using Sirenix.OdinInspector;
using VMFramework.Core;
using VMFramework.Procedure;

namespace LoveClash.GameCore
{
    [ManagerCreationProvider(ManagerType.NetworkCore)]
    public class GameStateManager : NetworkManagerBehaviour<GameStateManager>
    {
        #region SyncVar

        [ShowInInspector]
        private readonly SyncVar<GameState> _currentState = new(GameState.Invalid);
        
        [ShowInInspector]
        private readonly SyncVar<int> _currentPlayerId = new(-1);
        
        [ShowInInspector]
        private readonly SyncVar<int> _turnNumber = new(0);

        #endregion

        #region Properties

        public static GameState currentState => instance._currentState.Value;

        public static int currentPlayerId => instance._currentPlayerId.Value;

        public static Entities.Player currentPlayer => PlayerManager.GetPlayer(currentPlayerId);

        public static int turnNumber => instance._turnNumber.Value;

        #endregion

        #region Event

        public delegate void GameStateChangeEvent(GameState previous, GameState current, bool asServer);

        public static event GameStateChangeEvent OnGameStateChangeEvent;

        #endregion

        #region FSM

        [ShowInInspector]
        private static ISingleStateFSM<GameState, GameStateManager> fsm =
            new SingleStateFSM<GameState, GameStateManager>();
        
        [ShowInInspector]
        private static readonly Queue<GameState> stateQueue = new();

        private void InitFSM()
        {
            fsm.AddAllConnections();

            foreach (var gameStateProcessorType in typeof(IGameStateProcessor).GetDerivedClasses(false, false))
            {
                if (gameStateProcessorType.IsAbstract || gameStateProcessorType.IsInterface)
                {
                    continue;
                }

                var gameStateProcessor =
                    Activator.CreateInstance(gameStateProcessorType) as IGameStateProcessor;
                
                fsm.AddState(gameStateProcessor);
            }
            
            fsm.Init(this);

            Run(GameState.Invalid);
        }

        public override void OnStartServer()
        {
            base.OnStartServer();
            
            Run(GameState.Invalid);
        }

        private void Update()
        {
            if (stateQueue.Count > 0)
            {
                fsm.EnterState(stateQueue.Dequeue());
            }
            
            fsm.Update();
        }
        
        [Server]
        [Button]
        public static void Run(GameState state)
        {
            stateQueue.Enqueue(state);
        }

        #endregion

        protected override void OnPreInit()
        {
            base.OnPreInit();
            
            _currentState.OnChange += OnCurrentStateChange;
            
            InitFSM();
        }

        private void OnCurrentStateChange(GameState previous, GameState current, bool asServer)
        {
            fsm.EnterState(current);
            
            OnGameStateChangeEvent?.Invoke(previous, current, asServer);
        }
    }
}