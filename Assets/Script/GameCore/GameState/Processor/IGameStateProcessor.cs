﻿using VMFramework.Core;

namespace LoveClash.GameCore
{
    public interface IGameStateProcessor : IFSMState<GameState, GameStateManager>
    {
        
    }
}