﻿namespace LoveClash.GameCore
{
    public class InvalidStateProcessor : GameStateProcessor
    {
        public override GameState id => GameState.Invalid;
    }
}