﻿using VMFramework.Core;

namespace LoveClash.GameCore
{
    public abstract class GameStateProcessor : IGameStateProcessor
    {
        public bool canEnterFromAnyState => true;

        public IFSM<GameState, GameStateManager> fsm { get;private set; }
        
        IFSM<GameState, GameStateManager> IFSMState<GameState, GameStateManager>.fsm
        {
            get => fsm;
            set => fsm = value;
        }

        public abstract GameState id { get; }
        
        public void OnInit(IFSM<GameState, GameStateManager> fsm)
        {
            
        }

        public void OnEnter()
        {
            
        }

        public void OnExit()
        {
            
        }

        public void OnUpdate(bool isActive)
        {
            
        }

        public void OnFixedUpdate(bool isActive)
        {
            throw new System.NotImplementedException();
        }

        public void OnDestroy()
        {
            
        }
    }
}