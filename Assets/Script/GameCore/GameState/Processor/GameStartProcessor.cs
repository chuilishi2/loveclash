﻿namespace LoveClash.GameCore
{
    public class GameStartProcessor : GameStateProcessor
    {
        public override GameState id => GameState.GameStart;
    }
}