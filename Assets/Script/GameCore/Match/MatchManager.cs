﻿using System.Collections.Generic;
using System.Linq;
using FishNet.Object;
using LoveClash.Entities;
using Sirenix.OdinInspector;
using UnityEngine;
using VMFramework.Procedure;

namespace LoveClash.GameCore
{
    [ManagerCreationProvider(ManagerType.NetworkCore)]
    public class MatchManager : NetworkManagerBehaviour<MatchManager>
    {
        private readonly List<Entities.Player> _players = new();
        
        /// <summary>
        /// 比赛中的所有玩家
        /// </summary>
        public IReadOnlyList<Entities.Player> players => _players;
        
        /// <summary>
        /// 开始比赛
        /// </summary>
        /// <param name="players"></param>
        [Server]
        public void StartMatch(IEnumerable<Entities.Player> players)
        {
            if (GameStateManager.currentState != GameState.Invalid)
            {
                Debug.LogError("比赛已经开始了，不能再开始新的比赛");
                return;
            }

            var playerList = players.ToList();

            if (playerList.Count < 2)
            {
                Debug.LogError("比赛至少需要两个玩家");
                return;
            }

            foreach (var player in _players)
            {
                if (player.character.value == null)
                {
                    Debug.LogError("玩家角色为空，无法开始比赛");
                    return;
                }
            }
            
            _players.Clear();
            _players.AddRange(playerList);
            
            GameStateManager.Run(GameState.GameStart);
        }

        /// <summary>
        /// 快速开始比赛，创建机器人玩家
        /// </summary>
        [Server]
        [Button]
        public void QuickStartMatchWithRobot()
        {
            if (PlayerManager.playerCount == 0)
            {
                Debug.LogError("没有玩家角色，无法开始比赛");
                return;
            }

            if (PlayerManager.playerCount == 1)
            {
                PlayerManager.CreateServerPlayer();
            }
            
            var players = new List<Entities.Player>();
            foreach (var (clientID, player) in PlayerManager.allPlayers)
            {
                if (player.character.value == null)
                {
                    if (clientID < 0)
                    {
                        player.character.value = CharacterManager.CreateRobot();
                    }
                    else
                    {
                        player.character.value = CharacterManager.CreateDefaultCharacter();
                    }
                }
                
                players.Add(player);
            }
            
            StartMatch(players);
        }
    }
}