using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Script.core;
using Script.Effects.其他Effect;
using Script.Manager;
using Script.Network;
using Script.view;
using UnityEngine;

/// <summary>
/// 强迫承诺
/// </summary>
public class Effect2007 : FunctionCardEffectBase, IBaseCounter
{
    public List<string> CanCounterTextTypes { get; set; }
    
    // 我方信任槽值
    private int _myConfidenceValue;
    public int MyConfidenceValue
    {
        get => _myConfidenceValue;
        set => _myConfidenceValue = value;
    }
    
    // 对方信任槽值
    private int _oppConfidenceValue;
    public int OppConfidenceValue
    {
        get => _oppConfidenceValue;
        set => _oppConfidenceValue = value;
    }
    
    // 是否监听
    private bool _listening;
    
    // 是否打出反制
    private bool _beCountered;
    
    // 判断双方信任值
    private bool _needJudgeBoth;
    
    
    public override void NormalEffect()
    {
        if (!_listening)
        {
            //TODO：画出打出【强迫承诺】方15点信任值上限的计数槽
            EventManager.Register<TurnOverEvent>(Listen);
            _listening = true;
        }
        else
        {
            if (!_beCountered)
            {
                // 如果我方上限为15点信任值技术槽满则我方心动值增加15点，反之则对方心动值增加15点
                if (MyConfidenceValue == 15)
                {
                    GameManager.instance.Myself.心动值 += 15;
                }
                else
                {
                    GameManager.instance.Opponent.心动值 += 15;
                }
            }
            else
            {
                if (_needJudgeBoth)
                {
                    if (MyConfidenceValue == 15)
                    {
                        GameManager.instance.Myself.心动值 += 15;
                    }
                    else
                    {
                        GameManager.instance.Opponent.心动值 += 15;
                    }
                    _needJudgeBoth = false;
                }
                else
                {
                    if (OppConfidenceValue == 15)
                    {
                        GameManager.instance.Opponent.心动值 += 15;
                    }
                    else
                    {
                        GameManager.instance.Myself.心动值 += 15;
                    }
                }
            }
        }
    }

    
    //在【强迫承诺】打出后对方使用感觉很暧昧的类卡牌，效果变为：双方同时获得【强迫承诺】效果，并且双方都抽取两张牌（不限定种类，基础和特殊都行）。
    public override async void CounteredEffect()
    {
        // 对方打出反制牌，设为被反制状态
        _beCountered = true;
        //TODO：画出【强迫承诺】目标方15点信任值上限的计数槽
        
        // 双方抽两张牌
        List<BaseCardTableItem> baseCardTableItems = BaseCardTable.Instance.DataList.Values.ToList();
        
        for (int i = 0; i < 2; i++)
        {
            //抽取卡牌
            //双方都发送一次抽牌，抽两次
            Operation operation = new Operation(OperationType.DrawCard, extraMessage: typeof(DrawCardEffect).FullName);
            operation.playerEnum = GameProcessor.CurPlayer;
            operation.targetNetworkIds = new List<int> { (int)GameProcessor.CurPlayer, (int)GameProcessor.OppPlayer };
            GameManager.instance.ExecuteOperation(operation);
        }

    }
    
    
    private void Listen(TurnOverEvent turnOverEvent)
    {
        // 获得这【强迫承诺】的目标对象
        PlayerEnum targetPlayerEnum = (PlayerEnum)targets[0] == PlayerEnum.Player1 ? PlayerEnum.Player2 : PlayerEnum.Player1;
        
        // 回合结束前看看是否被反制
        //未被反制
        if (!_beCountered)
        {
            // 谁打出这张【强迫承诺】，先结束回合的人肯定是打出【强迫承诺】的人，于是结束该方法，如果想要注销监听那么要结束回合的那个人就必须是【强迫承诺】的目标
            if (NetworkManager.instance.playerEnum != targetPlayerEnum)
            {
                return;
            }
            
            // 注销监听
            EventManager.Unregister<TurnOverEvent>(Listen);
        
            //重新执行一次 CardEffect，这时候就会执行 NormalEffect 中 listening == true 的分支
            Operation operation = new Operation(OperationType.CardEffect);
            operation.playerEnum = targetPlayerEnum;
            operation.baseNetworkId = GetComponent<NetworkId>().networkId;
            operation.targetNetworkIds = targets;
            GameManager.instance.ExecuteOperation(operation);
        }
        // 被反制
        else
        {
            // 被反制了，那么结束回合的人肯定是【强迫承诺】目标方
            //【强迫承诺】目标方执行一次CardEffect，这时候就会执行 NormalEffect 中 listening == true 的分支
            Operation operation = new Operation(OperationType.CardEffect);
            operation.playerEnum = targetPlayerEnum;
            operation.baseNetworkId = GetComponent<NetworkId>().networkId;
            operation.targetNetworkIds = targets;
            GameManager.instance.ExecuteOperation(operation);
            
            // 【强迫承诺】被反制方也就是出牌方在执行一次
            PlayerEnum beCounterPlayerEnum = targetPlayerEnum == PlayerEnum.Player1 ? PlayerEnum.Player2 : PlayerEnum.Player1;
            if (NetworkManager.instance.playerEnum != beCounterPlayerEnum)
            {
                return;
            }
            
            // 注销监听
            EventManager.Unregister<TurnOverEvent>(Listen);

            _needJudgeBoth = true;
            
            // 打出【强迫承诺】方再执行一次CardEffect，这时候就会执行 NormalEffect 中 listening == true 的分支
            GameManager.instance.ExecuteOperation(operation);
            
        }
    }
        
    

    
}
