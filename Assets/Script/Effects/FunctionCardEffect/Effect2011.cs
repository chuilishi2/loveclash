using System;
using System.Collections;
using System.Collections.Generic;

using Cysharp.Threading.Tasks;

using Script.Cards;
using Script.core;
using Script.Manager;
using Script.Network;
using Script.view;

using UnityEngine;
/// <summary>
/// 要让对方变成爱我的样子
/// </summary>
public class Effect2011 : FunctionCardEffectBase
{
    Timer timer;
    public bool playCard = false;//记录一下有没有打出该类型的牌
    string selcetType;
    public async override void CounteredEffect()
    {
        //双方上头值大幅度增加，并同时抽取一张基础牌
        GameManager.instance.Myself.心动值 += 5;

        int BaseCardId = 1000 + UnityEngine.Random.Range(1, BaseCardTable.Instance.DataList.Count);
        NetworkObject card = await NetworkManager.InstantiateNetworkObject(BaseCardTable.Instance.DataList[BaseCardId].basecardName, UIManager.instance.CardsParent);
        UIManager.instance.myselfView.DrawCard(card.GetComponent<ICardView>());
        card.GetComponent<CardView>().own = NetworkManager.instance.playerEnum;
    }

    public async override void NormalEffect()
    {
        if ((int)GameManager.instance.Myself.playerEnum == targets[0])
        {
            //选一种基础牌文本类型,并告诉对方
            selcetType=await SelectBaseCardType();
            Operation operation = new Operation(OperationType.Info);
            operation.playerEnum = GameProcessor.CurPlayer;
            operation.targetNetworkIds= new List<int>() { (int)GameProcessor.OppPlayer , BaseCardTextType.getTypeId(selcetType) };
            GameManager.instance.ExecuteOperation(operation);

            timer = new Timer(15, TimeoutExectue);
            TimerManager.Instance.EnQueue(timer);
            EventManager.Register<PlayCardEvent>(SuccessExectue);

        }
        else
        {
            EventManager.Register<OperationInfoEvent>(Listen);
        }
    }
    private async void TimeoutExectue()
    {
        //如果没打，我方抽一张该文本类型的基础牌
        if(!playCard)
        {
            if((int)GameManager.instance.Myself.playerEnum == targets[0])
            {
                while (true)
                {
                    int BaseCardId = 1000 + UnityEngine.Random.Range(1, BaseCardTable.Instance.DataList.Count);
                    if (BaseCardTable.Instance.DataList[BaseCardId] != null && BaseCardTable.Instance.DataList[BaseCardId].basecardTextType.Equals(selcetType))
                    {
                        NetworkObject card = await NetworkManager.InstantiateNetworkObject(BaseCardTable.Instance.DataList[BaseCardId].basecardName, UIManager.instance.CardsParent);
                        UIManager.instance.myselfView.DrawCard(card.GetComponent<ICardView>());
                        card.GetComponent<CardView>().own = NetworkManager.instance.playerEnum;
                        break;
                    }
                }
                //上头值增加
                GameManager.instance.Opponent.上头值 += 1;
            }
            else
            {
                //没写加多少，先这样
                GameManager.instance.Myself.上头值 += 1;
            }
        }
    }
    /// <summary>
    /// 对方成功打出
    /// </summary>
    /// <param name="evt"></param>
    private void SuccessExectue(PlayCardEvent evt)
    {
        
        playCard = true;
        EventManager.Unregister<PlayCardEvent>(SuccessExectue);
        TimerManager.Instance.StopTimer(timer);
        TimeoutExectue();
    }
    /// <summary>
    /// 监听对方发来的文本类型
    /// </summary>
    /// <returns></returns>
    private void Listen(OperationInfoEvent operationInfoEvent)
    {
        if (operationInfoEvent.info<=0)
        {
            throw new Exception("错误文本类型");
        }
        //限制自己的出牌出牌类型
        string TextType = BaseCardTextType.getTypeString(operationInfoEvent.info);
        Operation operation = new Operation(OperationType.OtherEffect);
        operation.playerEnum = GameProcessor.CurPlayer;
        operation.targetNetworkIds= new List<int>() { (int)GameProcessor.OppPlayer };
        operation.extraMessage = "RestrictCardEffect,true,1," + TextType + ",,,";
        GameManager.instance.ExecuteOperation(operation);


        //TODO 等新的ui框架再完善
        Debug.Log("可以开始出类型为" + TextType + "的基础牌了");



        EventManager.Unregister<OperationInfoEvent>(Listen);
        //监听自己有没有打出指定文本类型的基础牌
        EventManager.Register<PlayCardEvent>(SuccessExectue);

        timer = new Timer(15, TimeoutExectue);
        TimerManager.Instance.EnQueue(timer);
    }

    /// <summary>
    ///TODO 弹出一个面板UI选择基础牌文本类型
    /// </summary>
    /// <returns></returns>
    private async UniTask<string> SelectBaseCardType()
    {
        //TODO先这样
        return "Sport";
    }

    // Start is called before the first frame update
}

