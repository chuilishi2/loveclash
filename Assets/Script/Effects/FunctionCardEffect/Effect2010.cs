using Script;
using Script.Cards;
using Script.core;
using Script.Effects.其他Effect;
using Script.Manager;
using Script.Network;
using Script.view;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 情感共鸣
/// </summary>
public class Effect2010 : FunctionCardEffectBase, IBaseCounter
{
    private int turnStartNum = 0;

    private List<string> canCounterTextTypes = new List<string> { "交心类" };
    public List<string> CanCounterTextTypes { get => canCounterTextTypes; set => canCounterTextTypes = value; }

    //在情感共鸣打出后对方使用交心类卡牌，效果变为：双方对对方使用两张随机交心类卡牌。
    public override async void CounteredEffect()
    {
        List<BaseCardTableItem> baseCardTableItems = new List<BaseCardTableItem>();
        foreach (BaseCardTableItem item in BaseCardTable.Instance.DataList.Values)
        {
            if (item.basecardTextType.Equals("交心类"))
                baseCardTableItems.Add(item);
        }

        //设置随机种子
        Random.InitState(53);

        // 生成随机数
        int random1 = Random.Range(0, baseCardTableItems.Count);
        int random2 = Random.Range(0, baseCardTableItems.Count);

        //抽取卡牌
        NetworkObject card1 = await NetworkManager.InstantiateNetworkObject("BaseCard", UIManager.instance.CardsParent);
        UIManager.instance.myselfView.DrawCard(card1.GetComponent<ICardView>());

        //双方都打出卡牌
        PlayCard(card1.GetComponent<CardBase>(), baseCardTableItems[random1].id);

        //然后再根据第二个随机数结果，重复上述内容
        NetworkObject card2 = await NetworkManager.InstantiateNetworkObject("BaseCard", UIManager.instance.CardsParent);
        UIManager.instance.myselfView.DrawCard(card2.GetComponent<ICardView>());
                                                                                
        PlayCard(card2.GetComponent<CardBase>(), baseCardTableItems[random2].id);
    }

    //下回合双方都无法使用特殊牌，同时双方都抽一张牌
    public override void NormalEffect()
    {
        EventManager.Register<TurnStartEvent>(Listen);

        //双方都发送一次抽牌
        Operation operation = new Operation(OperationType.DrawCard, extraMessage: typeof(DrawCardEffect).FullName);
        operation.playerEnum = GameProcessor.CurPlayer;
        operation.targetNetworkIds = new List<int> { (int)GameProcessor.CurPlayer, (int)GameProcessor.OppPlayer };
        GameManager.instance.ExecuteOperation(operation);
    }

    //步骤：
    //1. 监听到对方回合开始，禁用对方功能牌
    //2. 监听到我方回合开始，启用对方功能牌，禁用我方功能牌
    //3. 监听到对方回合开始，启用我方功能牌，注销监听
    private void Listen(TurnStartEvent turnStartEvent)
    {
        switch (turnStartNum)
        {
            //第一次执行，先禁用对方的功能牌
            case 0: 
                ChangeSelf(turnStartEvent.playerEnum, false); 
                break;
            case 1: 
                ChangeOpp(turnStartEvent.playerEnum, true); 
                ChangeSelf(turnStartEvent.playerEnum, false); 
                break;
            case 2:
                ChangeOpp(turnStartEvent.playerEnum, true);
                EventManager.Register<TurnStartEvent>(Listen);
                break;
        }
        turnStartNum++;
    }

    private void PlayCard(CardBase card, int id)
    {
        //TODO 先让我方打出卡牌，考虑在 own 的 set 构造器中提供一个将卡牌移动到手牌区域的方法
        card.GetComponent<CardView>().own = GameProcessor.CurPlayer;

        //接下来是根据第一个随机数的结果，初始化相应的卡牌数据
        card.CardId = id;

        //更改目标
        card.GetComponent<EffectBase>().Targets = new List<int> { (int)GameProcessor.OppPlayer };

        //打出卡牌
        card.PlayCard();

        //换为对方打出卡牌——————————TODO 可能出问题，暂时先这么写
        card.GetComponent<CardView>().own = GameProcessor.OppPlayer;

        //接下来是根据第一个随机数的结果，初始化相应的卡牌数据——不用再初始化一次了
        //card.CardId = id;

        //更改目标
        card.GetComponent<EffectBase>().Targets = new List<int> { (int)GameProcessor.CurPlayer };

        //打出卡牌
        card.PlayCard();
    }

    private void ChangeOpp(PlayerEnum playerEnum, bool isActive)
    {
        if (playerEnum == GameManager.instance.Myself.playerEnum)
        {
            List<CardBase> list = GameManager.instance.Opponent.GetFuncionCards();
            for (int i = 0; i < list.Count; i++)
            {
                list[i].GetComponent<CardView>().active = isActive;
            }
        }
        else
        {
            List<CardBase> list = GameManager.instance.Myself.GetFuncionCards();
            for (int i = 0; i < list.Count; i++)
            {
                list[i].GetComponent<CardView>().active = isActive;
            }
        }
    }

    private void ChangeSelf(PlayerEnum playerEnum, bool isActive)
    {
        if (playerEnum == GameManager.instance.Myself.playerEnum)
        {
            List<CardBase> list = GameManager.instance.Myself.GetFuncionCards();
            for (int i = 0; i < list.Count; i++)
            {
                list[i].GetComponent<CardView>().active = isActive;
            }
        }
        else
        {
            List<CardBase> list = GameManager.instance.Opponent.GetFuncionCards();
            for (int i = 0; i < list.Count; i++)
            {
                list[i].GetComponent<CardView>().active = isActive;
            }
        }
    }
}
