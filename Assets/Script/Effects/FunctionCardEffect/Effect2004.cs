using Script.Cards;
using Script.Manager;
using Script.Network;

/// <summary>
/// 爱意转移术
/// </summary>
public class Effect2004 : FunctionCardEffectBase, IHeartRate
{
    //记录对方对自己心动值的影响，——必须有，因为可能会受到对方功能牌、随从技能影响（策划说的）
    private int oppHeartRateChange;
    public int OppHeartRateChange { get => oppHeartRateChange; set => oppHeartRateChange = value; }

    //这张牌不会有影响自己心动值的情况
    public int MyHeartRateChange { get => 0; set { } }

    //检查是否正在监听
    private bool listening;

    public override void CounteredEffect()
    {
        
    }

    //打出后，如果对方打出的基础牌对我方造成心动值增加，该心动值增加量转移至对方。
    //该牌效果只影响心动值，基础牌内其他数值正常加减，且只影响一次。
    //如果对方打出的下一张基础牌内容中无心动值，则该效果无效。
    //在该基础牌中数值被对方功能牌
    //随从技能影响的情况下，按照计算后的数值转移给对方。
    public override void NormalEffect()
    {
        //如果是打出这张卡牌并生效，这时候 listening == false，就应该去监听 CardEffect 事件
        if (!listening)
        {
            EventManager.Register<CardEffectEvent>(Listen);
            listening = true;
        }
        //如果是监听打牌事件时触发了，则应该去执行真正的内容
        else
        {
            listening = false;

            //策划说的：oppHeartRateChange 为 0，不生效
            if (oppHeartRateChange == 0) return;

            //获取目标
            //PlayerEnum playerEnum = (PlayerEnum)targets[0];

            /*//广播一次 CardEffect 事件，表示这张牌的真正效果生效了，通知给别人，————错误，Operation 就会广播，不能再广播了
            //这时候就可以让真正的效果受到对方功能牌、随从技能影响
            using var cardEffectEvent = CardEffectEvent.Get();

            //记录这张牌的 NetworkId
            cardEffectEvent.cardId = GetComponent<NetworkId>().networkId;

            //cardEffectEvent.playerEnum 表示：谁打出卡牌，而现在，这张牌的 targets[0] 我把他定为对方，
            //而打出这张牌的人是我方，所以用三目表达式对 targets[0] 进行处理即可得到我方 PlayerEnum
            cardEffectEvent.playerEnum = playerEnum == PlayerEnum.Player1 ? PlayerEnum.Player2 : PlayerEnum.Player1;

            //cardEffectEvent.targetids 表示：卡牌对谁生效，直接用 targets 就行
            cardEffectEvent.targetids = targets;

            //定义好事件成员后，广播事件——对方功能牌、随从技能若能产生影响，就是在监听到这个事件的时候来改变这里的 OppHeartRateChange
            EventManager.SendEvent(cardEffectEvent);*/

            //如果我方是目标，改变我方心动值
            if (GameManager.instance.Myself.playerEnum == (PlayerEnum)targets[0])
            {
                GameManager.instance.Myself.心动值 += OppHeartRateChange;
            }
            //如果对方是目标，改变对方心动值
            else
            {
                GameManager.instance.Opponent.心动值 += OppHeartRateChange;
            }
        } 
    }

    private void Listen(CardEffectEvent cardEffectEvent)
    {
        //如果打出卡牌的不是目标（目标应该是打出 LoveTransfer 的对立面），则不生效
        if ((int)cardEffectEvent.playerEnum != targets[0])
        {
            return;
        }

        //如果对方打出的牌不是基础牌，不生效
        if (cardEffectEvent.cardId.ToNetworkObject() is not BaseCardBase card)
        {
            return;
        }

        //能生效，就及时注销监听，防止重复触发（接下来就会有一次 CardEffectEvent，一定要在它之前注销监听）
        EventManager.Unregister<CardEffectEvent>(Listen);

        //获取对方打出卡牌 Effect 引用，强转为 IHeartRate，用于获取 OppHeartRateChange
        if (cardEffectEvent.cardId.ToNetworkObject().GetComponent<EffectBase>() is IHeartRate heartRate)
        {
            //记录对方对我方心动值造成的变化值
            OppHeartRateChange = heartRate.OppHeartRateChange;

            //对方对我方心动值变化值归零
            heartRate.OppHeartRateChange = 0;
        }

        //重新执行一次 CardEffect，这时候就会执行 NormalEffect 中 listening == true 的分支
        Operation operation = new Operation(OperationType.CardEffect);
        operation.playerEnum = (PlayerEnum)targets[0] == PlayerEnum.Player1 ? PlayerEnum.Player2 : PlayerEnum.Player1;
        operation.baseNetworkId = GetComponent<NetworkId>().networkId;
        operation.targetNetworkIds = targets;
        GameManager.instance.ExecuteOperation(operation);
    }
}
