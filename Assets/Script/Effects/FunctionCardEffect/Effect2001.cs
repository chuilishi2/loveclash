using Script.Cards;
using Script.core;
using Script.Effects.其他Effect;
using Script.Manager;
using Script.Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect2001 : FunctionCardEffectBase, ISelectMy
{
    private CardBase card;
    public CardBase Card { get => card; set => card = value; }

    public override void CounteredEffect()
    {
        
    }

    public override void NormalEffect()
    {
        //完成选择后会执行的内容，暂时是让对方抽一张牌

        Operation operation = new Operation(OperationType.DrawCard, extraMessage: typeof(DrawCardEffect).FullName);
        
        //将目标设置为对方
        PlayerEnum playerEnum = GameProcessor.OppPlayer;
        operation.playerEnum = playerEnum;
        operation.targetNetworkIds = new List<int> { (int)playerEnum };

        //让对方执行一次抽牌
        GameManager.instance.ExecuteOperation(operation);
    }
}
