using Script.Cards;
using Script.core;
using Script.Manager;
using Script.Network;
using Script.view;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 功能牌：出去玩，现在的问题：无法确保对方获取两个 info
/// </summary>
public class Effect2009 : FunctionCardEffectBase, IGlobal
{
    // TODO 暂时用按钮模拟一下玩家做出选择
    [SerializeField]
    private Button button1;
    [SerializeField]
    private Button button2;

    public override void CounteredEffect()
    {

    }

    private Timer timer;

    public override void NormalEffect()
    {
        //如果不是当前可操作玩家，不执行
        if (GameProcessor.CurPlayer != NetworkManager.instance.playerEnum)
            return;

        //启用选项按钮
        EnableSelectButtons();

        //计时器的使用——超时处理
        timer = new Timer(10, TimeOut);
        TimerManager.Instance.EnQueue(timer);
    }

    /// <summary>
    /// 启用选项按钮
    /// </summary>
    private void EnableSelectButtons()
    {
        //启用两个按钮
        button1.enabled = true;
        button2.enabled = true;

        //假设这是 “看电影” 选项
        button1.onClick.AddListener(() =>
        {
            //终止计时器
            TimerManager.Instance.StopTimer(timer);

            InfoOperation(1);
        });

        //假设这是 “逛街” 选项
        button2.onClick.AddListener(() =>
        {
            //终止计时器
            TimerManager.Instance.StopTimer(timer);

            InfoOperation(2);
        });
    }

    //超时，则进行默认的处理，向对方发出随机的选项结果
    public void TimeOut()
    {
        InfoOperation(new System.Random().Next(1, 3));
    }

    private void InfoOperation(int value)
    {
        Operation operation = new Operation(OperationType.Info);

        //我方发送
        operation.playerEnum = GameProcessor.CurPlayer;

        operation.baseNetworkId = GameProcessor.GlobalInfo.GlobalCard.networkId;

        //对方接收
        operation.targetNetworkIds = new List<int> { (int)GameProcessor.OppPlayer, value };

        GameManager.instance.ExecuteOperation(operation);

        //禁用选项按钮
        DisableSelectButtons();
    }

    /// <summary>
    /// 禁用选项按钮
    /// </summary>
    private void DisableSelectButtons()
    {
        button1.enabled = false;
        button2.enabled = false;
        button1.onClick.RemoveAllListeners();
        button2.onClick.RemoveAllListeners();
    }

    public void ReallyEffect()
    {
        //如果不是当前可操作玩家，不执行
        if (GameProcessor.CurPlayer != NetworkManager.instance.playerEnum)
            return;

        //用于结算效果：看电影则 + 1，逛街则 + 2，这样两个数字相加结果只会是 2、3、4，分别对应三种最终效果
        int result = GameProcessor.GlobalInfo.GlobalInfoQueue.Dequeue() + GameProcessor.GlobalInfo.GlobalInfoQueue.Dequeue();
        switch (result)
        {
            case 2:
                {
                    // TODO 对双方随机使用一张电影类型的牌，并抽一张牌。
                    break;
                }
            case 3:
                {
                    // TODO 双方随机抽取两张出去玩类型基础牌。
                    break;
                }
            case 4:
                {
                    // TODO 双方都能反制一次功能牌，该功能牌反制效果为出去玩类型的基础牌。
                    break;
                }
            default:
                {
                    Debug.LogError("Global出问题");
                    break;
                }
        }
    } 
}
