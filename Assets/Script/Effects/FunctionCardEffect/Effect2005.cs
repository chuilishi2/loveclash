using System.Collections;
using System.Collections.Generic;
using Script.Cards;
using Script.Manager;
using Script.Network;
using UnityEngine;

/// <summary>
/// 爱意消失术
/// </summary>
public class Effect2005 : FunctionCardEffectBase, IHeartRate
{
    
    public int MyHeartRateChange { get; set; }
    public int OppHeartRateChange { get; set; }
    
    
    
    // 卡牌效果：打出后，如果对方打出的基础牌对我方造成心动值增加，该心动值增加为0（原内容为词条“消失”）。
    // 该牌效果只影响心动值，基础牌内其他数值正常加减，且只影响一次。如果对方打出的下一张基础牌内容中无心动值，则该效果无效。
    public override void NormalEffect()
    {
        EventManager.Register<CardEffectEvent>(Listen);
    }
    

    public override void CounteredEffect()
    {
        
    }
    
    
    private void Listen(CardEffectEvent cardEffectEvent)
    {
        if (cardEffectEvent.cardId.ToNetworkObject() is not BaseCardBase baseCardBase)
        {
            return;
        }
        
        //监听到打出的牌是基础牌后，及时注销监听，防止重复触发
        EventManager.Unregister<CardEffectEvent>(Listen);
        
        // 获取对方打出这张牌是否会改变心动值
        var networkObject = cardEffectEvent.cardId.ToNetworkObject();
        if (networkObject.GetComponent<EffectBase>() is IHeartRate heartRate)
        {
            //对方对我方心动值变化值归零
            heartRate.OppHeartRateChange = 0;
        }
        
    }


}
