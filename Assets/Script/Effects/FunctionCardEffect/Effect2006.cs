using System;
using System.Collections;
using System.Collections.Generic;
using Script.Cards;
using Script.core;
using Script.Manager;
using Script.Network;
using Script.view;
using UnityEngine;

/// <summary>
/// 欲擒故纵
/// </summary>
public class Effect2006 : FunctionCardEffectBase, ITrust
{
    /// <summary>
    /// 卡牌效果：双方将同时使用对方打出的下一张随从技能牌，我方信任值增加。该牌效果在对方打出随从技能牌后触发。
    /// </summary>
    
    // 对我方信任值的修改
    private int _trustChange;
    public int MyTrustChange { get => _trustChange; set => _trustChange = value; }
    public int OppTrustChange { get; set; }

    // 这张欲情故纵牌的NetWorkId
    private int _oppMinionSkillCardNetWorkId;

    // _operationPlayerEnum 是谁打出的欲情故纵这张牌
    private PlayerEnum _operationPlayerEnum;

    // _playerTargetEnum 是欲情故纵这张牌的目标对象，这样比较也就是我方出的随从技能牌
    private PlayerEnum _playerTargetEnum;

    // playerEnumSkill 是打出的随从技能牌的目标，作用对象是对面则是我方打出的，反之则是对面打出
    private PlayerEnum _playerTargetEnumSkill;

    // 获取打出的随从技能牌的目标和进行目标反转
    private List<int> _tempTargetIds;
    
    //检查是否正在监听
    private bool _listening = false;

   


    public override void CounteredEffect()
    {
        
    }

    public override void NormalEffect()
    {
        if (!_listening)
        {
            // 监听出牌事件
            EventManager.Register<CardEffectEvent>(Listen);
            _listening = true;
        }
        else
        {
            _listening = false;
            
            // PlayerEnum playerTargetEnum = (PlayerEnum)targets[0];
            _playerTargetEnumSkill = (PlayerEnum)_tempTargetIds[0];
            
            
            // target[0] 的意思就是这张欲情故纵本身的作用对象是对方， 如果是我方的随从技能牌打出，那么目标一致，模拟对方打出一样的随从技能牌
            // 目标不一致则模拟我方出随从技能牌
            if (_playerTargetEnum == _playerTargetEnumSkill)
            {
                // 反转卡牌作用目标
                for (int i = 0; i < _tempTargetIds.Count; i++)
                {
                    _tempTargetIds[i] = _tempTargetIds[i] % 2 + 1;
                }
                OperationExecutor.instance.Execute(new Operation(OperationType.Card, _oppMinionSkillCardNetWorkId, _tempTargetIds));
                
            }
            else
            {
                OperationExecutor.instance.Execute(new Operation(OperationType.Card, _oppMinionSkillCardNetWorkId, targets));
            }
            
            
            // 看这张欲情故纵是谁打出的谁就增加信任值
            if (_operationPlayerEnum == GameManager.instance.Myself.playerEnum)
            {
                Myself.instance.信任值++;
            }
            else if (_operationPlayerEnum == GameManager.instance.Opponent.playerEnum )
            {
                Opponent.instance.信任值++;
            }
        }
        
    }


    
    private void Listen(CardEffectEvent cardEffectEvent)
    {
        
        if (cardEffectEvent.cardId.ToNetworkObject() is not MinionSkillCardBase minionSkillCardBase)
        {
            return;
        }
        
        //生效后，及时注销监听，防止重复触发
        EventManager.Unregister<CardEffectEvent>(Listen);
        
        //获取打出随从牌技能的目标
        var networkObject = cardEffectEvent.cardId.ToNetworkObject();
        _tempTargetIds = cardEffectEvent.targetids;
        
        // 获取欲情故纵作用目标
        _playerTargetEnum = (PlayerEnum)targets[0];
        
        // 获取这张牌的NetworkId
        _oppMinionSkillCardNetWorkId = networkObject.GetComponent<NetworkId>().networkId;
        
        //重新执行一次 CardEffect，这时候就会执行 NormalEffect 中 listening == true 的分支
        Operation operation = new Operation(OperationType.CardEffect);
        _operationPlayerEnum = _playerTargetEnum == PlayerEnum.Player1 ? PlayerEnum.Player2 : PlayerEnum.Player1;
        operation.baseNetworkId = GetComponent<NetworkId>().networkId;
        operation.targetNetworkIds = targets;
        GameManager.instance.ExecuteOperation(operation);
    }


    
}
