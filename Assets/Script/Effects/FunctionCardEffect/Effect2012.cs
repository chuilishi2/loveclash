using Script.Cards;
using Script.Effects.其他Effect;
using Script.Manager;
using Script.Network;
using Script.view;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect2012 : FunctionCardEffectBase, ISelectOppMinionSkill, ITrust, IBaseCounter
{
    private CardBase card;
    public CardBase Card { get => card; set => card = value; }

    public int MyTrustChange { get => 0 ; set { } }
    
    private int oppTrustChange;
    public int OppTrustChange { get => oppTrustChange; set => oppTrustChange = value; }

    //TODO 这里 canCounterTextTypes 的元素请填入陪伴类
    private List<string> canCounterTextTypes = new List<string> { };
    public List<string> CanCounterTextTypes { get => canCounterTextTypes; set => canCounterTextTypes = value; }

    //在无所依归打出后对方使用陪伴类卡牌，效果变为：下一张牌双方获得的信任值翻倍，双方都抽取一张牌。
    public override void CounteredEffect()
    {
        EventManager.Register<CardEffectEvent>(Listen);   
    }

    //降低对方信任值，获得一张指定对方随从技能牌。
    public override void NormalEffect()
    {
        if ((int)GameManager.instance.Myself.playerEnum == targets[0])
        {
            GameManager.instance.Myself.信任值 += OppTrustChange;

            //获得随从技能牌
            GetOppMinionSkill();
        }
        else
        {
            GameManager.instance.Opponent.信任值 += OppTrustChange;

            GetOppMinionSkill();
        }
    }

    private void GetOppMinionSkill()
    {
        //UIManager 进入选择对方随从技能牌状态
        GetComponent<CardView>().SelectOppMinionSkill();
    }

    private void Listen(CardEffectEvent cardEffectEvent)
    {
        //监听到，开始执行，立刻注销，防止重复触发
        EventManager.Register<CardEffectEvent>(Listen);

        if (cardEffectEvent.cardId.ToNetworkObject().GetComponent<EffectBase>() is ITrust trust)
        {
            //数值 * 2
            trust.MyTrustChange *= 2;
            trust.OppTrustChange *= 2;
        }

        // 自己和对面都触发发送一次抽卡行为
        Operation operation = new Operation(OperationType.DrawCard, extraMessage: typeof(DrawCardEffect).FullName);
        operation.playerEnum = GameProcessor.CurPlayer;
        operation.targetNetworkIds = new List<int> { (int)GameProcessor.CurPlayer, (int)GameProcessor.OppPlayer };
        GameManager.instance.ExecuteOperation(operation);
    }

    //步骤：CardView 的 own 要改、Effect 的 target 要改、UI 要改
    public void ReallyEffect()
    {
        //Card.GetComponent<CardView>().own = Card.GetComponent<CardView>().own == PlayerEnum.Player1 ? PlayerEnum.Player2 : PlayerEnum.Player1;
        List<int> tar = Card.GetComponent<EffectBase>().Targets;
        for (int i = 0; i < tar.Count; i++)
        {
            //如果是 1，则 1 % 2 + 1 = 2，如果是 2，则 2 % 2 + 1 = 1
            tar[i] = tar[i] % 2 + 1;
        }
        //TODO 差一次选择信息发送
    }
}
