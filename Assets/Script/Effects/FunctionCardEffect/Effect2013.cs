using System.Collections;
using System.Collections.Generic;
using Script.Cards;
using Script.core;
using Script.Effects.其他Effect;
using Script.Manager;
using Script.Network;
using Script.view;
using AYellowpaper.SerializedCollections;
using UnityEngine;
/// <summary>
/// 信用卡
/// </summary>
public class Effect2013 : FunctionCardEffectBase
{

    public override void CounteredEffect()
    {
        throw new System.NotImplementedException();
    }

    public override async void NormalEffect()
    {
        if ((int)GameManager.instance.Myself.playerEnum == targets[0])
        {
            Operation operation = new Operation(OperationType.DrawCard, extraMessage: typeof(DrawCardEffect).FullName);

            operation.playerEnum = NetworkManager.instance.playerEnum;
            //if (gameObject.GetComponent<FunctionCardBase>().FunctionTableItem.targetId[0]==0)
            //打出后自己获得牌
            operation.targetNetworkIds = new List<int> { (int)NetworkManager.instance.playerEnum };
            //抽三张牌
            GameManager.instance.ExecuteOperation(operation);
            GameManager.instance.ExecuteOperation(operation);
            GameManager.instance.ExecuteOperation(operation);
        }
        else//对方获得一张信用卡
        {
            NetworkObject card = await NetworkManager.InstantiateNetworkObject("信用卡", UIManager.instance.CardsParent);
            UIManager.instance.myselfView.DrawCard(card.GetComponent<ICardView>());
        }


    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
