using System.Collections;
using System.Collections.Generic;

using Script.core;
using Script.Manager;
using Script.Network;
using Script.view;

using UnityEngine;

public class Effect2014 : FunctionCardEffectBase
{
    //双方抽一张牌，然后交换
    public override void CounteredEffect()
    {
        throw new System.NotImplementedException();
    }

    public override async void NormalEffect()
    {
        //TODO我方拍击一下对面头像，之后再说
        //对方心动值增加

        //获得一张对方手牌,不知道具体怎么样，先定义为盲抽,对方心动值增加
        if (Targets[0]==(int)GameManager.instance.Myself.playerEnum)
        {
            //对方心动值增加
            GameManager.instance.Opponent.心动值 += 1;
            
            int index=Random.Range(0,GameManager.instance.Myself.AllHandCards.Count);
            int Cardid = GameManager.instance.Myself.AllHandCards[index].CardId;
            int CardType = Cardid / 1000;
            string CardName;
            switch(CardType)
            {
                case 1:
                    CardName = BaseCardTable.Instance.GetItemById(Cardid).basecardName;
                    break;
                case 2:
                    CardName = FunctionCardTable.Instance.GetItemById(Cardid).functionName;
                    break;
                case 3:
                    CardName = MinionTable.Instance.GetItemById(Cardid).minionName;
                    break;
                case 4:
                    CardName = MinionSkillTable.Instance.GetItemById(Cardid).minionSkillName;
                    break;
                default:
                    CardName = "";
                    break;
            }
            NetworkObject card = await NetworkManager.InstantiateNetworkObject(CardName, UIManager.instance.CardsParent);
            UIManager.instance.myselfView.DrawCard(card.GetComponent<ICardView>());
            card.GetComponent<CardView>().own = NetworkManager.instance.playerEnum;

        }
        else
        {
            GameManager.instance.Myself.心动值 += 1;
        }
    }

}
