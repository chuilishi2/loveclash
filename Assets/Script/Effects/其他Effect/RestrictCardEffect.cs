using System.Collections;
using System.Collections.Generic;

using Script.Manager;

using UnityEngine;
/// <summary>
/// 限制只能出或禁止出xx牌的effect
/// </summary>
public class RestrictCardEffect : EffectBase
{
    public bool can;//禁止还是只允许1
    public int Round;//几回合结束2
    public string RestrictBaseTextType;//哪种文本基础牌4
    public string RestrictCardType;//哪种类型牌5
    public List<int> RestrictCardIdList=new List<int>();//哪张卡牌3


    public override void CounteredEffect()
    {
        throw new System.NotImplementedException();
    }
    /// <summary>
    /// 
    /// </summary>
    public override void NormalEffect()
    {
        can = bool.Parse(ParameterList[1]);
        Round = int.Parse(ParameterList[2]);
        RestrictBaseTextType = ParameterList[3];
        RestrictCardType = ParameterList[4];
        for (int i = 5; i < ParameterList.Length; i++)
        {
            RestrictCardIdList.Add(int.Parse(ParameterList[i]));
        }
        if (Targets[0] ==(int)GameManager.instance.Myself.playerEnum)
        {
            //先把之前的覆盖
            GameManager.instance.Myself._RestrictCardEffect = null;
            GameManager.instance.Myself._RestrictCardEffect = this;
        }
        if(Targets[0] == (int)GameManager.instance.Opponent.playerEnum)
        {
            //先把之前的覆盖
            GameManager.instance.Opponent._RestrictCardEffect = null;
            GameManager.instance.Myself._RestrictCardEffect = this;
        }
        EventManager.Register<TurnOverEvent>(Listen);
    }
    public void Listen(TurnOverEvent evt)
    {
        Round--;
        if(Round<=0)
        {
            //没有对象引用后自动垃圾回收
            GameManager.instance.Myself._RestrictCardEffect = null;
            GameManager.instance.Opponent._RestrictCardEffect = null;
            EventManager.Unregister<TurnOverEvent>(Listen);
        }

    }
}
