﻿using System.Collections.Generic;
using Script.Cards;
using Script.core;
using Script.Manager;
using Script.Network;
using UnityEngine;

namespace Script.Effects.其他Effect
{
    public class DrawCardEffect : EffectBase
    {
        public override void CounteredEffect()
        {

        }

        public override async void NormalEffect()
        {
            PlayerEnum playerEnum = (PlayerEnum)targets[0];
            var card = await GameManager.instance.GetPlayer(playerEnum).DrawCard();

            Debug.Log("抽卡", card);

            var drawCardEvent = DrawCardEvent.Get();
            drawCardEvent.playerEnum = playerEnum;
            drawCardEvent.card = card.GetComponent<CardBase>();
            EventManager.SendEvent(drawCardEvent);


        }
    }
}