using Script.Cards;
using Script.Manager;
using System.Collections.Generic;
using UnityEngine;
//基础牌的effect实现 待重写逻辑
[SerializeField]
public class BaseCardEffect : EffectBase, IHeartRate, ITrust, IExcitement, ICounter
{
    public string TextType
    {
        get => GetComponent<BaseCardBase>().BasecardTableItem.basecardTextType;
    }

    protected CardValueChange myselfChange = new CardValueChange();
    protected CardValueChange opponentChange = new CardValueChange();

    public int MyHeartRateChange { get => myselfChange.heartRateChange; set => myselfChange.heartRateChange = value; }
    public int OppHeartRateChange { get => opponentChange.heartRateChange; set => opponentChange.heartRateChange = value; }
    public int MyTrustChange { get => myselfChange.excitementChange; set => myselfChange.excitementChange = value; }
    public int OppTrustChange { get => opponentChange.trustChange; set => opponentChange.trustChange = value; }
    public int MyExcitementChange { get => myselfChange.excitementChange; set => myselfChange.excitementChange = value; }
    public int OppExcitementChange { get => opponentChange.excitementChange; set => opponentChange.excitementChange = value; }

    public override void NormalEffect()
    {
        // 生效时，更新我方和对方的心动值和信任值
        GameManager.instance.Myself.心动值 += myselfChange.heartRateChange;
        GameManager.instance.Myself.信任值 += myselfChange.trustChange;
        GameManager.instance.Myself.上头值 += myselfChange.excitementChange;
        GameManager.instance.Opponent.心动值 += opponentChange.heartRateChange;
        GameManager.instance.Opponent.信任值 += opponentChange.trustChange;
        GameManager.instance.Opponent.上头值 += opponentChange.excitementChange;

    }

    public override void CounteredEffect()
    {

    }

    public override void Trigger(List<int> targetIds = null)
    {
        OnTriggerEffect();
    }

    public bool CanCounter(CardBase cardBase)
    {
        if ((cardBase.GetComponent<Effect>() is IBaseCounter baseCounter))
        {
            baseCounter.CanCounterTextTypes.Contains(TextType);
            return true;
        }
        return false;
    }
}
