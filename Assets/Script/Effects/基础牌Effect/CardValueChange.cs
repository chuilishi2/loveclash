using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 用于表示心动值、信任值、上头值的变化
public class CardValueChange
{
    public int heartRateChange = 1;
    public int trustChange = 1;
    public int excitementChange = 1;

    public CardValueChange(int heartRateChange, int trustChange, int excitementChange)
    {
        this.heartRateChange = heartRateChange;
        this.trustChange = trustChange;
        this.excitementChange = excitementChange;
    }

    public CardValueChange() { }
}
