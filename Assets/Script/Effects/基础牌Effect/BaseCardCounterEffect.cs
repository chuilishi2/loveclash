using Script.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCardCounterEffect : BaseCardEffect
{
    [SerializeField]
    private List<string> canCounterTextTypes; // 可以反制的文本类型列表  还没想好是通过文本类型的字符串还是说将文本类型映射到卡牌id

    public List<string> CanCounterTextTypes
    {
        get => canCounterTextTypes;
        set => canCounterTextTypes = value;
    }

    private List<int> canCounterList;
    public List<int> CanCounterList { get => canCounterList; set => canCounterList = value; }

    /*public virtual bool CanCounter(CardBase cardBase)
    {
        // 检查卡牌ID是否在反制列表中
        return CanCounterList.Contains(cardBase.CardId);
    }*/

    public virtual bool CanCounter(BaseCardBase baseCard)
    {
        // 检查基础牌的文本类型是否在反制条件列表中
        return CanCounterTextTypes.Contains(baseCard.BasecardTableItem.basecardTextType);
    }

    // ? 如果要具体实现反制要怎么弄？
    public void CounterEffect(BaseCardBase cardToCounter)
    {
        // 根据不同的文本类型获取对应的反制脚本，并调用其 CounterEffect 方法?
        switch (cardToCounter.BasecardTableItem.basecardTextType)
        {
            default:
                break;
        }
    }
}