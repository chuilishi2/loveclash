using Script.Cards;
using System.Collections.Generic;
using UnityEngine;
//基础牌效果基类
public abstract class EffectBase : MonoBehaviour
{
    protected List<int> targets;
    public List<int> Targets { get => targets; set => targets = value; }

    public string[] ParameterList;//参数列表，index 0是effect名字
    public virtual void Trigger(List<int> targetIds)
    {
        targets = targetIds;
        OnTriggerEffect();
    }

    /// <summary>
    /// 是否处于禁用状态
    /// </summary>
    protected bool disable;
    public bool Disable { get => disable; set => disable = value; }

    protected virtual void Awake()
    {
        
    }

    protected virtual void OnDisable()
    {
        EventManager.UnregisterTarget(this);
    }

    protected virtual void OnTriggerEffect()
    {
        if (!Disable) { NormalEffect(); }
        else { CounteredEffect(); }
    }

    /// <summary>
    /// 卡牌正常效果
    /// </summary>
    public abstract void NormalEffect();

    /// <summary>
    /// 被反制后的效果
    /// </summary>
    public abstract void CounteredEffect();
}

/// <summary>
/// 只需要自己进行选择操作
/// </summary>
public interface ISelectMy
{
    CardBase Card { get; set; }
}

/// <summary>
/// 选择对方随从
/// </summary>
public interface ISelectOppMinionSkill
{
    CardBase Card { get; set; }

    void ReallyEffect();
}

public interface ISelectBaseTextType
{
    BaseCardTextType Type { get; set; }

    void ReallyEffect();
}

/// <summary>
/// 对双方都产生影响的
/// </summary>
public interface IGlobal
{
    void ReallyEffect();
}

//如果卡牌效果会对基础值产生影响，请实现以下对应接口，这样外部可通过强转为接口类型获取变化值
//比如要获取心动值变化值，则：(card as IHeartRate).HeartRateChange 即可获取
public interface IHeartRate
{
    /// <summary>
    /// 我方心跳值变化值
    /// </summary>
    int MyHeartRateChange { get; set; }

    /// <summary>
    /// 对方心跳值变化值
    /// </summary>
    int OppHeartRateChange { get; set; }
}

public interface ITrust
{
    int MyTrustChange { get; set; }
    int OppTrustChange { get; set; }
}

public interface IExcitement
{
    int MyExcitementChange { get; set; }
    int OppExcitementChange { get; set; }
}

/// <summary>
/// 如果卡牌可以被某种 / 某些文本类型的基础牌反制，请实现此接口
/// </summary>
public interface IBaseCounter
{
    List<string> CanCounterTextTypes { get; set; }
}

/// <summary>
/// 如果卡牌可以去反制卡牌，请实现此接口
/// </summary>
public interface ICounter
{
    bool CanCounter(CardBase cardBase);
}