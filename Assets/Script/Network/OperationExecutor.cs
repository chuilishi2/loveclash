﻿using System;
using Script.core;
using Script.Manager;
using Script.view;

using UnityEngine;

namespace Script.Network
{
    /// <summary>
    /// 只负责执行,不关心网络
    /// </summary>
    public class OperationExecutor : MonoBehaviour
    {
        public static OperationExecutor instance;

        private void Awake()
        {
            instance = this;
        }

        /// <summary>
        /// 最常用的,外部用来执行命令的方法
        /// </summary>
        /// <param name="operation"></param>
        public void Execute(Operation operation)
        {
            if (!NetworkManager.isOnline)
            {
                m_Execute(operation);
                return;
            }
            var task = NetworkUtility.RequestAsync(NetworkManager.instance.senderClient, JsonUtility.ToJson(operation));
            task.GetAwaiter().OnCompleted(() =>
            {
                m_Execute(JsonUtility.FromJson<Operation>(task.GetAwaiter().GetResult()));
            });
        }
        
        public void m_Execute(Operation operation)
        {
            switch (operation.operationType)
            {
                case OperationType.Card:
                    Card(operation);
                    break;
                case OperationType.CardEffect:
                    CardEffect(operation);
                    break;
                case OperationType.EndTurn:
                    EndTurn(operation);
                    break;
                case OperationType.CreateObject:
                    CreateObject(operation);
                    break;
                case OperationType.Debug:
                    Debug(operation);
                    break;
                case OperationType.DrawCard:
                    DrawCard(operation);
                    break;
                case OperationType.Message:
                    Message(operation);
                    break;
                case OperationType.Info:
                    Info(operation);
                    break;
                case OperationType.OtherEffect:
                    OtherEffect(operation);
                    break;
                    
            }
        }

        /// <summary>
        /// 注意：这里的方案是—— targetNetworkIds[0] 存发送目标，targetNetworkIds[1] 存发送的内容
        /// 所以发送时一定要规范内容
        /// </summary>
        /// <param name="operation"></param>
        private static void Info(Operation operation)
        {
            using var infoEvt = OperationInfoEvent.Get();
            //谁发出信息
            infoEvt.playerEnum = operation.playerEnum;
            //发给谁——PlayerEnum 对应的 int
            infoEvt.targetNetworkId = operation.targetNetworkIds[0];
            // TODO 暂时用 targetNetworkIds 的第二个元素记录发出的信息
            infoEvt.info = operation.targetNetworkIds[1];
            //广播 OperationInfoEvent
            EventManager.SendEvent(infoEvt);
        }

        private static void Message(Operation operation)
        {
            var messageEvt = OperationMessageEvent.Get();
            messageEvt.targetNetworkId = operation.targetNetworkIds[0];
            messageEvt.playerEnum = operation.playerEnum;
            messageEvt.operationMessage = JsonUtility.FromJson<OperationMessage>(operation.extraMessage);
            EventManager.SendEvent(messageEvt);
        }

        private static void EndTurn(Operation operation)
        {
            GameProcessor.TurnOver(operation.playerEnum);
        }

        private static void CardEffect(Operation operation)
        {
            var card = operation.baseNetworkId;

            using var cardEffectEvent = CardEffectEvent.Get();;
            cardEffectEvent.cardId = card;
            cardEffectEvent.playerEnum = operation.playerEnum;
            EventManager.SendEvent(cardEffectEvent);

            if (operation.playerEnum == NetworkManager.instance.playerEnum)
            {
                GameManager.instance.Myself.PlayCard(card,
                    operation.targetNetworkIds);
            }
            else
            {
                GameManager.instance.Opponent.PlayCard(card,
                    operation.targetNetworkIds);
            }
        }

        private static void Card(Operation operation)
        {
            var card = operation.baseNetworkId;

            using var playCardEvent = PlayCardEvent.Get();
            playCardEvent.playerEnum = operation.playerEnum;
            playCardEvent.cardId = card;
            playCardEvent.targetids = operation.targetNetworkIds;
            EventManager.SendEvent(playCardEvent);
        }

        private static void CreateObject(Operation operation)
        {
            NetworkObject card=NetworkManager.InstantiateNetworkObjectLocal(operation.extraMessage,
                operation.baseNetworkId, UIManager.instance.物品池.transform);
            UIManager.instance.opponentView.DrawCard(card.GetComponent<ICardView>());
        }

        private static void Debug(Operation operation)
        {
            UnityEngine.Debug.Log("DEBUG message from Player" + operation.playerEnum + ": " + operation.extraMessage);
        }

        private static void Error(Operation operation)
        {
            if (operation.playerEnum == NetworkManager.instance.playerEnum)
            {
                UnityEngine.Debug.Log("服务器错误");
            }
        }
        private void DrawCard(Operation operation)
        {
            var type = Type.GetType(operation.extraMessage);
            if (type == null)
                return;

            var effect = (EffectBase)gameObject.GetComponent(type);
            if (effect == null)
            {
                effect = (EffectBase)gameObject.AddComponent(type);
            }
            effect.Trigger(operation.targetNetworkIds);
        }
        /// <summary>
        /// 禁止出xx牌
        /// </summary>
        /// 参数1 effect名字 后面的参数以字符串数组存储,在用到的effect上自行解析，参数列表名为ParameterList
        /// <param name="operation"></param>
        private void OtherEffect(Operation operation)
        {
            string _Message=operation.extraMessage;
            string[] Message = _Message.Split(',');
            var type = Type.GetType(Message[0]);
            if (type == null)
                return;

            var effect = (EffectBase)gameObject.GetComponent(type);
            if (effect == null)
            {
                effect = (EffectBase)gameObject.AddComponent(type);
            }
            effect.ParameterList = Message;
            effect.Trigger(operation.targetNetworkIds);
        }
    }
}