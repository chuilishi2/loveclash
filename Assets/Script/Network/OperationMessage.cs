using Script.Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class OperationMessage
{
    public enum Result
    {
        Success,
        Failure,
    }

    public OperationType operationType;
    public Result operationResult;

    public OperationMessage(OperationType operationType, Result operationResult)
    {
        this.operationType = operationType;
        this.operationResult = operationResult;
    }
}
