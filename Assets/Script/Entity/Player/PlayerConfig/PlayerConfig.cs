﻿using System;
using Sirenix.OdinInspector;
using VMFramework.Configuration;
using VMFramework.GameLogicArchitecture;
using VMFramework.Containers;
using VMFramework.OdinExtensions;

namespace LoveClash.Entities
{
    [GamePrefabAutoRegister(ID)]
    public partial class PlayerConfig : EntityConfig
    {
        public const string ID = "player_entity";
        
        protected const string PLAYER_CATEGORY = "玩家设置";

        public override Type gameItemType => typeof(Player);

        protected override Type controllerType => typeof(PlayerController);

        [LabelText("遗物容器"), TabGroup(TAB_GROUP_NAME, PLAYER_CATEGORY)]
        [GamePrefabIDValueDropdown(typeof(ContainerPreset))]
        [IsNotNullOrEmpty]
        public string cardInventoryID;

        //
        // [LabelText("初始遗物"), TabGroup(TAB_GROUP_NAME, PLAYER_CATEGORY)]
        // [ListDrawerSettings(ShowFoldout = false)]
        // public List<RelicGenerationConfig> initialRelics = new();
    }
}