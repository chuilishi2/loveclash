﻿using System;
using Cysharp.Threading.Tasks;
using LoveClash.GameCore;
using UnityEngine.Scripting;
using VMFramework.Procedure;

namespace LoveClash.Entities
{
    [GameInitializerRegister(typeof(ClientLoadingProcedure))]
    [Preserve]
    public sealed class PlayerClientInitializer : IGameInitializer
    {
        async void IInitializer.OnInit(Action onDone)
        {
            PlayerManager.instance.RequestCreatePlayer();

            await UniTask.WaitUntil(() => PlayerManager.isThisPlayerInitialized);

            onDone();
        }
    }
}