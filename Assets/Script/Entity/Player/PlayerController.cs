﻿using FishNet.Connection;

namespace LoveClash.Entities
{
    public class PlayerController : EntityController
    { 
        public Player player { get; private set; }

        #region Init

        protected override void OnPreInit()
        {
            base.OnPreInit();

            player = entity as Player;
        }

        protected override void OnPostInit()
        {
            base.OnPostInit();
            
            if (IsServerStarted == false)
            {
                PlayerManager.RegisterPlayer(Owner.ClientId, player);
            }
        }

        #endregion

        #region Despawn

        public override void OnDespawnServer(NetworkConnection connection)
        {
            base.OnDespawnServer(connection);
        }

        #endregion
    }
}
