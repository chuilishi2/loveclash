﻿using System.Collections.Generic;
using VMFramework.Core;
using FishNet.Serializing;
using VMFramework.Containers;
using VMFramework.Property.Structs;

namespace LoveClash.Entities
{
    public class Player : Entity, IContainerOwner
    {
        public new PlayerController controller { get; private set; }

        protected PlayerConfig playerConfig => (PlayerConfig)origin;

        // [ShowInInspector]
        // public RelicInventory relicInventory { get; private set;}

        public ReferenceProperty<Character> character = new(null);

        #region Init

        protected override void OnCreate()
        {
            base.OnCreate();

            // if (isServer)
            // {
            //     foreach (var initialRelic in playerConfig.initialRelics)
            //     {
            //         relicInventory.AddItem(initialRelic.GenerateItem());
            //     }
            // }
        }

        protected override void OnInit()
        {
            base.OnInit();

            controller = base.controller as PlayerController;

            controller.AssertIsNotNull(nameof(controller));
        }

        #endregion

        #region Net Serialization

        

        #endregion

        #region Container Owner

        public IEnumerable<Container> GetContainers()
        {
            // yield return relicInventory;
            yield break;
        }

        #endregion
    }
}
