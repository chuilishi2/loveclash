﻿using VMFramework.Core;
using Sirenix.OdinInspector;
using UnityEngine;
using VMFramework.GameLogicArchitecture;
using VMFramework.OdinExtensions;

namespace LoveClash.Entities
{
    public sealed partial class PlayerGeneralSetting : GeneralSettingBase
    {
        #region Fields

        [LabelText("默认玩家ID")]
        [GamePrefabIDValueDropdown(typeof(PlayerConfig))]
        [SerializeField]
        private string _defaultPlayerID;

        #endregion

        #region Property

        public string defaultPlayerID
        {
            get
            {
                if (GamePrefabManager.ContainsGamePrefab(_defaultPlayerID))
                {
                    return _defaultPlayerID;
                }

                return GamePrefabManager.GetRandomGamePrefab<PlayerConfig>()?.id;
            }
        }

        #endregion
        
        public override void CheckSettings()
        {
            base.CheckSettings();

        }
    }
}
