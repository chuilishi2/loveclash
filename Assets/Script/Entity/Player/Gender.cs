﻿using Sirenix.OdinInspector;

namespace LoveClash.Entities
{
    /// <summary>
    /// 性别枚举
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// 男性
        /// </summary>
        [LabelText("男性")]
        Male,
        /// <summary>
        /// 女性
        /// </summary>
        [LabelText("女性")]
        Female
    }
}