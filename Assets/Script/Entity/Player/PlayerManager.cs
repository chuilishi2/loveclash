﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using FishNet.Connection;
using FishNet.Object;
using Sirenix.OdinInspector;
using UnityEngine;
using VMFramework.GameLogicArchitecture;
using VMFramework.Procedure;

namespace LoveClash.Entities
{
    [ManagerCreationProvider(ManagerType.NetworkCore)]
    public class PlayerManager : NetworkManagerBehaviour<PlayerManager>
    {
        [ShowInInspector]
        private static Dictionary<int, Player> _allPlayers = new();

        [ShowInInspector]
        private static Dictionary<int, Player> serverPlayers = new();
 
        [ShowInInspector]
        private static Player thisPlayer;

        public static bool isThisPlayerInitialized { get; private set; }

        #region Properties

        public static int playerCount => _allPlayers.Count;
        
        /// <summary>
        /// 所有玩家，不管是否在比赛中
        /// </summary>
        public static IReadOnlyDictionary<int, Player> allPlayers => _allPlayers;

        #endregion

        #region Create Player
        
        private static int serverPlayerMinIndex = 0;

        [Server]
        [Button]
        public static void CreateServerPlayer()
        {
            serverPlayerMinIndex--;
            
            if (_allPlayers.TryGetValue(serverPlayerMinIndex, out var existedPlayer))
            {
                Debug.LogWarning("Server player already exists");
                return;
            }
            
            var player = IGameItem.Create<Player>(PlayerConfig.ID);

            EntityManager.CreateEntity(player, Vector2.zero);
            
            RegisterPlayer(serverPlayerMinIndex, player);
        }

        [ServerRpc(RequireOwnership = false)]
        [Button]
        public void RequestCreatePlayer(NetworkConnection connection = null)
        {
            if (_allPlayers.TryGetValue(connection.ClientId, out var existedPlayer))
            {
                EntityManager.DestroyEntity(existedPlayer);
                
                UnregisterPlayer(connection.ClientId);
            }
            
            var player = IGameItem.Create<Player>(PlayerConfig.ID);

            EntityManager.CreateEntity(player, Vector2.zero, connection);

            RegisterPlayer(connection.ClientId, player);
            
            CreatePlayerResponse(connection, player.uuid);
        }

        [TargetRpc]
        private void CreatePlayerResponse(NetworkConnection connection, string entityUUID)
        {
            CreatePlayerResponseAsync(connection, entityUUID);
        }

        private async void CreatePlayerResponseAsync(NetworkConnection connection, string entityUUID)
        {
            var entity = await EntityManager.GetOwnerAsync(entityUUID);

            if (entity is not Player player)
            {
                Debug.LogError("Could not register player from UUID the server sent");
                return;
            }
            
            RegisterPlayer(connection.ClientId, player);
        }

        #endregion

        #region Register & Unregister

        public static void RegisterPlayer(int ownerID, Player player)
        {
            _allPlayers[ownerID] = player;

            if (ownerID < 0)
            {
                serverPlayers[ownerID] = player;
            }
            else if (_instance.IsClientStarted)
            {
                if (ownerID == _instance.ClientManager.Connection.ClientId)
                {
                    thisPlayer = player;

                    isThisPlayerInitialized = true;
                }
            }
        }

        public static void UnregisterPlayer(int ownerID)
        {
            _allPlayers.Remove(ownerID);
            
            if (ownerID < 0)
            {
                serverPlayers.Remove(ownerID);
            }
            else if (_instance.IsClientStarted)
            {
                if (ownerID == _instance.ClientManager.Connection.ClientId)
                {
                    thisPlayer = null;

                    isThisPlayerInitialized = false;
                }
            }
        }

        #endregion

        #region Get Player

        public static bool TryGetPlayer(int ownerID, out Player player)
        {
            return _allPlayers.TryGetValue(ownerID, out player);
        }

        public static Player GetPlayer(int ownerID)
        {
            return TryGetPlayer(ownerID, out var player) ? player : null;
        }

        public static IEnumerable<Player> GetAllPlayers()
        {
            return _allPlayers.Values;
        }

        #endregion

        #region Get This Player

        public static bool TryGetThisPlayer(out Player player)
        {
            player = thisPlayer;

            return player != null;
        }

        public static Player GetThisPlayer()
        {
            return thisPlayer;
        }

        public static PlayerController GetThisPlayerController()
        {
            return thisPlayer?.controller;
        }

        public static bool TryGetThisPlayerController(out PlayerController playerController)
        {
            playerController = thisPlayer?.controller;

            return playerController != null;
        }

        #endregion

        #region Get This Player Inventory

        // public static RelicInventory GetThisPlayerRelicInventory()
        // {
        //     return GetThisPlayer()?.relicInventory;
        // }

        #endregion
    }
}
