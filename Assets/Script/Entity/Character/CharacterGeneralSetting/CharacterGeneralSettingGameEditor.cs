﻿using LoveClash.GameCore;
using VMFramework.Editor;
using VMFramework.GameLogicArchitecture;
using VMFramework.Localization;

namespace LoveClash.Entities
{
    public partial class CharacterGeneralSetting : IGameEditorMenuTreeNode
    {
        string INameOwner.name => new LocalizedTempString()
        {
            { "zh-CN", "角色" },
            { "en-US", "Character" }
        };

        string IGameEditorMenuTreeNode.folderPath =>
            (GameSetting.entityGeneralSetting as IGameEditorMenuTreeNode)?.nodePath;
    }
}