﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using VMFramework.GameLogicArchitecture;
using VMFramework.OdinExtensions;

namespace LoveClash.Entities
{
    public sealed partial class CharacterGeneralSetting : GeneralSettingBase
    {
        #region Categories

        private const string CHARACTER_CATEGORY = "角色";

        #endregion

        [field: LabelText("默认角色ID"), TabGroup(TAB_GROUP_NAME, CHARACTER_CATEGORY)]
        [field: GamePrefabIDValueDropdown(typeof(CharacterConfig))]
        [field: SerializeField]
        public string defaultCharacterID { get; private set; }
        
        [field: LabelText("机器人角色ID"), TabGroup(TAB_GROUP_NAME, CHARACTER_CATEGORY)]
        [field: GamePrefabIDValueDropdown(typeof(CharacterConfig))]
        [field: SerializeField]
        public string robotCharacterID { get; private set; }
    }
}