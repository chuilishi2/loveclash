﻿using FishNet.Object;
using LoveClash.GameCore;
using VMFramework.GameLogicArchitecture;
using VMFramework.Procedure;

namespace LoveClash.Entities
{
    [ManagerCreationProvider(ManagerType.NetworkCore)]
    public class CharacterManager : NetworkManagerBehaviour<CharacterManager>
    {
        [Server]
        public static Character CreateCharacter(string characterID)
        {
            var character = IGameItem.Create<Character>(characterID);

            EntityManager.CreateEntity(character);
            return character;
        }

        [Server]
        public static Character CreateDefaultCharacter()
        {
            var character =
                IGameItem.Create<Character>(GameSetting.characterGeneralSetting.defaultCharacterID);
            
            EntityManager.CreateEntity(character);
            return character;
        }
        
        [Server]
        public static Character CreateRobot()
        {
            var character = IGameItem.Create<Character>(GameSetting.characterGeneralSetting.robotCharacterID);
            
            EntityManager.CreateEntity(character);
            return character;
        }
    }
}