﻿using FishNet.Serializing;

namespace LoveClash.Entities
{
    public abstract class Character : Entity
    {
        protected CharacterConfig characterConfig => (CharacterConfig)origin;
        
        /// <summary>
        /// 心动值
        /// </summary>
        public BaseBoostIntProperty affection;

        /// <summary>
        /// 信任值
        /// </summary>
        public BaseBoostIntProperty trust;

        /// <summary>
        /// 上头值
        /// </summary>
        public BaseBoostIntProperty excitement;

        protected override void OnCreate()
        {
            base.OnCreate();
            
            affection = new(characterConfig.defaultAffection);
            trust = new(characterConfig.defaultTrust);
            excitement = new(characterConfig.defaultExcitement);
        }

        #region Net Serialization

        protected override void OnWrite(Writer writer)
        {
            base.OnWrite(writer);

            writer.WriteBaseBoostIntProperty(affection);
            writer.WriteBaseBoostIntProperty(trust);
            writer.WriteBaseBoostIntProperty(excitement);
        }

        protected override void OnRead(Reader reader)
        {
            base.OnRead(reader);
            
            affection = reader.ReadBaseBoostIntProperty();
            trust = reader.ReadBaseBoostIntProperty();
            excitement = reader.ReadBaseBoostIntProperty();
        }

        #endregion
    }
}