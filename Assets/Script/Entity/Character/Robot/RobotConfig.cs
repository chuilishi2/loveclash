﻿using System;
using VMFramework.GameLogicArchitecture;

namespace LoveClash.Entities
{
    [GamePrefabAutoRegister(ID)]
    public class RobotConfig : CharacterConfig
    {
        public const string ID = "robot_character";

        public override Type gameItemType => typeof(Robot);
    }
}