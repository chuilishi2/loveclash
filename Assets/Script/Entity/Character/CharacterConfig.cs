﻿using System;
using Sirenix.OdinInspector;

namespace LoveClash.Entities
{
    public abstract class CharacterConfig : EntityConfig
    {
        protected const string CHARACTER_CATEGORY = "角色";
        
        protected override string idSuffix => "character";

        public override Type gameItemType => typeof(Character);

        protected override Type controllerType => typeof(CharacterController);
        
        [LabelText("默认心动值"), TabGroup(TAB_GROUP_NAME, CHARACTER_CATEGORY)]
        [MinValue(0)]
        public int defaultAffection = 0;

        [LabelText("默认信任值"), TabGroup(TAB_GROUP_NAME, CHARACTER_CATEGORY)]
        [MinValue(0)]
        public int defaultTrust = 0;

        [LabelText("初始上头值"), TabGroup(TAB_GROUP_NAME, CHARACTER_CATEGORY)]
        [MinValue(0)]
        public int defaultExcitement = 0;
    }
}