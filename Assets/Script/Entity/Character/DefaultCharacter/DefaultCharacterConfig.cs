﻿using System;
using VMFramework.GameLogicArchitecture;

namespace LoveClash.Entities
{
    [GamePrefabAutoRegister(ID)]
    public class DefaultCharacterConfig : CharacterConfig
    {
        public const string ID = "default_character";

        public override Type gameItemType => typeof(DefaultCharacter);
    }
}