﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using VMFramework.GameLogicArchitecture;
using VMFramework.OdinExtensions;

namespace LoveClash.Entities
{
    public sealed partial class EntityGeneralSetting : GamePrefabGeneralSetting
    {
        #region Meta Data

        public override string prefabName => "Entity Config";

        public override Type baseGamePrefabType => typeof(EntityConfig);

        #endregion
    }
}
