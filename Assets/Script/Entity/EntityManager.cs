﻿using FishNet;
using FishNet.Connection;
using FishNet.Object;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Scripting;
using VMFramework.Core;
using VMFramework.GameLogicArchitecture;
using VMFramework.Network;
using VMFramework.OdinExtensions;
using VMFramework.Procedure;

namespace LoveClash.Entities
{
    [ManagerCreationProvider(ManagerType.NetworkCore)]
    public class EntityManager : UUIDManager<EntityManager, Entity, EntityManager.EntityInfo>
    {
        [Preserve]
        public class EntityInfo : OwnerInfo
        {

        }

        #region Create & Destroy

        [Server]
        public static EntityController CreateEntity(Entity entity, Vector2 position = default,
            NetworkConnection ownerConnection = null)
        {
            entity.AssertIsNotNull(nameof(entity));
            entity.prefab.AssertIsNotNull(nameof(entity.prefab));

            var gameObject = InstanceFinder.NetworkManager.GetPooledInstantiated(entity.prefab, true);
            //Instantiate(entity.prefab);

            gameObject.transform.position = position;

            var entityController = gameObject.GetComponent<EntityController>();

            entityController.Init(entity);

            InstanceFinder.ServerManager.Spawn(gameObject, ownerConnection);

            return entityController;
        }

        [Server]
        public static void DestroyEntity(Entity entity)
        {
            if (entity == null)
            {
                Debug.LogWarning("entity为Null，无法破坏");
                return;
            }

            if (TryGetInfo(entity.uuid, out var info))
            {
                Unregister(entity);

                entity.Destroy();

                InstanceFinder.ServerManager.Despawn(entity.controller.gameObject, DespawnType.Pool);
            }
        }

        #endregion

        #region Debug

        [Button("创建实体")]
        [Server]
        public static EntityController CreateEntity(
            [LabelText("实体")] [GamePrefabIDValueDropdown(typeof(EntityConfig))] string entityID,
            [LabelText("位置")] Vector2 position)
        {
            var entity = IGameItem.Create<Entity>(entityID);

            return CreateEntity(entity, position);
        }

        [Button("在玩家位置创建实体")]
        [Server]
        public static EntityController CreateEntityOnThisPlayer(
            [LabelText("实体")] [GamePrefabIDValueDropdown(typeof(EntityConfig))] string entityID)
        {
            if (PlayerManager.isThisPlayerInitialized == false)
            {
                Debug.LogError("此方法只能在玩家初始化之后调用");
                return null;
            }

            var entity = IGameItem.Create<Entity>(entityID);

            return CreateEntity(entity, PlayerManager.GetThisPlayerController().transform.position);
        }

        #endregion
    }
}
