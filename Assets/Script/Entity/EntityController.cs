﻿using Cysharp.Threading.Tasks;
using VMFramework.Core;
using FishNet.Connection;
using FishNet.Object;
using Sirenix.OdinInspector;
using UnityEngine;
using VMFramework.MouseEvent;

namespace LoveClash.Entities
{
    public class EntityController : NetworkBehaviour
    {
        [ShowInInspector]
        public Entity entity { get; private set; }

        #region Spawn & Despawn

        public override void OnSpawnServer(NetworkConnection connection)
        {
            base.OnSpawnServer(connection);

            InitOnClient(connection, entity);
        }

        public override void OnDespawnServer(NetworkConnection connection)
        {
            base.OnDespawnServer(connection);

            DestroyOnClient(connection);
        }

        #endregion

        #region RPC

        [TargetRpc(ExcludeServer = true)]
        private void InitOnClient(NetworkConnection connection, Entity entity)
        {
            Init(entity);
        }

        [TargetRpc(ExcludeServer = true)]
        private void DestroyOnClient(NetworkConnection connection)
        {
            entity?.Destroy();
        }

        #endregion

        #region Init

        public bool initDone { get; private set; }

        public async void Init(Entity entity)
        {
            this.entity = entity;

            OnPreInit();
            
            OnInit();

            entity.Init(this);

            await UniTask.WaitUntil(() => IsNetworked);
            
            OnPostInit();

            initDone = true;
        }

        protected virtual void OnPreInit()
        {

        }

        protected virtual void OnInit()
        {

        }

        protected virtual void OnPostInit()
        {
            
        }

        #endregion

        #region Debug

        protected const string DEBUGGING_GROUP = "调试";

        [Button("破坏此实体"), TitleGroup(DEBUGGING_GROUP)]
        [Server]
        [HideInEditorMode]
        private void DestroyThisEntity()
        {
            EntityManager.DestroyEntity(entity);
        }

        #endregion
    }
}
