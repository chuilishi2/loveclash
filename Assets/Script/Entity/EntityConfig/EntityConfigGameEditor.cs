﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using VMFramework.Core;
using VMFramework.Editor;

namespace LoveClash.Entities
{
    public partial class EntityConfig : IGameEditorMenuTreeNode
    {
        public Sprite spriteIcon
        {
            get
            {
                if (prefab == null)
                {
                    return null;
                }
                
                return AssetPreview.GetAssetPreview(prefab).ToSprite();
            }
        }
    }
}
#endif