using Cysharp.Threading.Tasks;
using DG.Tweening;
using Script.Cards;
using Script.Manager;
using Script.Network;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Script.view
{
    [RequireComponent(typeof(InputHandler))]
    public class CardView : ICardView
    {
        public PlayerEnum own;
        public CardBase card;

        //原始的RectTransform.AnchoredPosition
        [HideInInspector]
        public Vector3 originPos;
        /// <summary>
        /// 是否是可触发状态
        /// </summary>
        private bool _active = true;
        public bool active
        {
            get => _active;
            set //=> _shine.gameObject.SetActive(value);
            {
                _shine.gameObject.SetActive(value);
                _active = value;
            }
        }
        /// <summary>
        /// 开始拖动时的偏移量
        /// </summary>
        protected Vector2 dragOffset;
        // 发亮组件
        protected Transform _shine;
        //是否是放大状态
        protected bool _bigMode = false;
        protected Camera mainCamera;
        
        #region 一些动画属性
        //放大时向上移动的距离
        // private readonly float enterUpDistance = 200;
        
        #endregion
        protected virtual void Awake()
        {
            _shine = transform.Find("Shine");
            mainCamera = Camera.main;
            originPos = transform.position;
            card = GetComponent<CardBase>();
        }

        protected virtual void OnEnable()
        {
            EventManager.Register<CardEffectEvent>(OnCardEffect);
            EventManager.Register<DrawCardEvent>(OnDrawCard);
        }

        protected virtual void OnDisable()
        {
            EventManager.UnregisterTarget(this);
        }

        protected virtual async void OnCardEffect(CardEffectEvent @event)
        {
            if (@event.cardId != networkId)
                return;

            PlayerView playerView = GetComponentInParent<PlayerView>();
            Debug.Log(playerView, playerView);

            /*if (card.CardId / 1000 == 3)
            {
                transform.SetParent(UIManager.instance.entourageArea.transform);
                active = false;
                playerView.handCards.Remove(this);
                await ResetPosition(UIManager.instance.entourageArea.transform.position);
                return;
            }*/
            transform.SetParent(UIManager.instance.弃牌堆.transform);
            active = false;
            playerView.handCards.Remove(this);

            if (own == GameManager.instance.Myself.playerEnum)
                GameManager.instance.Myself.RemoveCard(GetComponent<CardBase>());
            else GameManager.instance.Opponent.RemoveCard(GetComponent<CardBase>());

            await ResetPosition(UIManager.instance.弃牌堆.transform.position);
        }

        protected virtual void OnDrawCard(DrawCardEvent @event)
        {
            if (@event.card == card)
            {
                own = @event.playerEnum;
            }
        }

        public virtual void PlayCard()
        {
            // TODO 这里还差第三个参数：List<int> targetNetworkIds，用来指定目标，最后会传到 EffectBase
            // 个人想法：卡牌 SO 再加一个目标字段，这里再根据卡牌 id 从 SO 里获取目标，作为这里的第三个参数
            // 然后 EffectBase 中就可以通过 targetNetworkIds，判断卡牌效果要对谁生效
            Execute(new Operation(OperationType.Card, networkId));
        }

        #region 一些事件函数
        public override async void OnPointerEnter(PointerEventData eventData)
        {
            if (!active) return;
            if (_bigMode) return;
            active = false;
            _bigMode = true;
            await DOTween.Sequence()
                .Join(transform.DOScale(Vector3.one * 1.5f, 0.3f))
                .Join(DOTween.To(() => transform.position,
                    value => transform.position = value,
                    originPos + new Vector3(0, Screen.height * 0.125f, 0), 0.2f)).Play().ToUniTask();
            active = true;
        }

        //设置并回到原位置(不传入就是回到原位置)
        public override async UniTask ResetPosition(Vector3? position = null)
        {
            if (position != null)originPos = position.GetValueOrDefault();
            active = false;
            await DOTween.Sequence()
                .Join(transform.DOMove(originPos,0.2f))
                .Join(transform.DOScale(Vector3.one, 0.3f))
                .Play().ToUniTask();
            active = true;
        }
        
        public override void OnBeginDrag(PointerEventData eventData)
        {
            if (!active) return;
            if (NetworkManager.instance.playerEnum != own) return;
            dragOffset = (Vector2)transform.position - eventData.position;
        }

        public override void OnDrag(PointerEventData eventData)
        {
            if (!active) return;
            if (NetworkManager.instance.playerEnum != own) return;
            transform.position = eventData.position + dragOffset;
        }

        public override async void OnEndDrag(PointerEventData eventData)
        {
            if (!active) return;

            if (NetworkManager.instance.playerEnum != own) return;

            //牌是对方的 && 选择对方随从牌状态 && 这张牌是随从技能牌
            else if (UIManager.instance.selectingOppMinionSkill && GetComponent<CardBase>().CardId / 1000 == 4)
            {
                SelectingOppMinionSkill();
                return;
            }

            if (UIManager.instance.卡牌回手区域.rect.Contains(eventData.position))
            {
                await ResetPosition();
                return;
            }

            if (GameProcessor.CurPlayer != NetworkManager.instance.playerEnum)
            {
                await ResetPosition();
                return;
            }

            //判断是否可出牌
            if (!card.CanPlayCard())
            {
                //不可出牌，回到手牌区
                await ResetPosition();

                return;
            }
            //可出牌的情况
            else
            {
                //如果卡牌是要进行选择操作的，在选择时将它放到弃牌堆，并跳过后面 PlayCard 的执行
                if (GetComponent<EffectBase>() is ISelectMy)
                {
                    SelectMy();
                    return;
                }

                //如果处于选择状态，跳过 PlayCard，且要作为 使 UIManager 进入选择状态的卡牌 selectingCard 的一段信息
                if (UIManager.instance.selectingMy)
                {
                    SelectingMy();
                    return;
                }

                //正常打出卡牌
                card.PlayCard();
            }
        }

        public override async void OnPointerExit(PointerEventData eventData)
        {
            if(!active)return;
            if (!_bigMode)return;
            _bigMode = false;
            active = false;
            await ResetPosition();
            active = true;
        }
        #endregion

        public async void SelectMy()
        {
            //启用取消选择按钮
            UIManager.instance.取消选择.enabled = true;

            //记录卡牌信息
            UIManager.instance.selectingMyCard = card;

            //暂时移到弃牌堆
            transform.SetParent(UIManager.instance.弃牌堆.transform);
            await ResetPosition(UIManager.instance.弃牌堆.transform.position);

            //进入选择状态：用于下次判断
            UIManager.instance.selectingMy = true;

            //玩家不选择卡牌（需要一个取消选择按钮）会执行
            UIManager.instance.取消选择.onClick.AddListener(async () =>
            {
                //回到手牌区
                transform.SetParent(UIManager.instance.卡牌回手区域.transform);
                await ResetPosition(UIManager.instance.卡牌回手区域.transform.position);

                //确保 UI 不处于选择状态
                UIManager.instance.selectingMy = false;

                //禁用取消选择按钮
                UIManager.instance.取消选择.enabled = false;
            });
        }

        public async void SelectingMy()
        {
            //防止重复选择
            UIManager.instance.selectingMy = false;

            //记录被选择卡牌
            (UIManager.instance.selectingMyCard.GetComponent<EffectBase>() as ISelectMy).Card = card;

            //打出 selectingCard
            UIManager.instance.selectingMyCard.PlayCard();

            //移除取消选择按钮监听
            UIManager.instance.取消选择.onClick.RemoveAllListeners();

            UIManager.instance.取消选择.enabled = false;

            transform.SetParent(UIManager.instance.弃牌堆.transform);

            //禁用
            active = false;

            //从手牌移除
            GetComponentInParent<PlayerView>().handCards.Remove(this);

            if (own == GameManager.instance.Myself.playerEnum)
                GameManager.instance.Myself.RemoveCard(GetComponent<CardBase>());
            else GameManager.instance.Opponent.RemoveCard(GetComponent<CardBase>());

            await ResetPosition(UIManager.instance.弃牌堆.transform.position);
        }

        /// <summary>
        /// 可能会被反制导致无法选择，所以这个内容在 EffectBase 执行
        /// 在具体卡牌才调用这个方法，进入选择对方随从技能牌状态
        /// </summary>
        public async void SelectOppMinionSkill()
        {
            //启用取消选择按钮
            UIManager.instance.取消选择.enabled = true;

            //记录卡牌信息
            UIManager.instance.selectingOppMinionSkillCard = card;

            //TODO 暂时移到弃牌堆，上面选择自己卡牌也是这样
            transform.SetParent(UIManager.instance.弃牌堆.transform);
            await ResetPosition(UIManager.instance.弃牌堆.transform.position);

            //进入选择状态：用于下次判断
            UIManager.instance.selectingOppMinionSkill = true;

            //玩家不选择卡牌（需要一个取消选择按钮）会执行
            UIManager.instance.取消选择.onClick.AddListener(async () =>
            {
                //回到手牌区
                transform.SetParent(UIManager.instance.卡牌回手区域.transform);
                await ResetPosition(UIManager.instance.卡牌回手区域.transform.position);

                //确保 UI 不处于选择状态
                UIManager.instance.selectingOppMinionSkill = false;

                //禁用取消选择按钮
                UIManager.instance.取消选择.enabled = false;
            });
        }

        public async void SelectingOppMinionSkill()
        {
            //防止重复选择
            UIManager.instance.selectingOppMinionSkill = false;

            //更改所有者
            own = own == PlayerEnum.Player1 ? PlayerEnum.Player2 : PlayerEnum.Player1;

            //记录被选择卡牌
            ISelectOppMinionSkill selectOppMinionSkill = UIManager.instance.selectingOppMinionSkillCard.GetComponent<EffectBase>() as ISelectOppMinionSkill;
            selectOppMinionSkill.Card = card;

            //获取被选择卡牌引用后，执行 selectOppMinionSkill.ReallyEffect();
            selectOppMinionSkill.ReallyEffect();

            //移除取消选择按钮监听
            UIManager.instance.取消选择.onClick.RemoveAllListeners();

            UIManager.instance.取消选择.enabled = false;

            //进入我方手牌区——有问题，应该是：从对方手牌变到我方手牌，我方手牌变到对方手牌
            transform.SetParent(UIManager.instance.卡牌回手区域.transform);

            //加入手牌
            GetComponentInParent<PlayerView>().handCards.Add(this);

            if (own == GameManager.instance.Myself.playerEnum)
                GameManager.instance.Myself.AddCard(GetComponent<CardBase>());
            else GameManager.instance.Opponent.AddCard(GetComponent<CardBase>());

            //TODO 请和上面 transform.SetParent(UIManager.instance.卡牌回手区域.transform); 一起修改
            await ResetPosition(UIManager.instance.卡牌回手区域.transform.position);
        }
    }
}
