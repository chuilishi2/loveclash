using Cysharp.Threading.Tasks;
using Script.Cards;
using Script.Manager;
using System.Collections.Generic;
using UnityEngine;

namespace Script.view
{
    [RequireComponent(typeof(InputHandler))]
    public class NewMinionView : CardView
    {
        /// <summary>
        /// 5个随从的rect,中间的是 minionRects[2]
        /// </summary>
        private List<Rect> minionRects = new List<Rect>();

        /// <summary>
        /// 所有随从的初始位置
        /// </summary>
        private List<Vector3> originPoses = new List<Vector3>();

        /// <summary>
        /// 用来放一个空Minion的——用于模拟炉石的效果，随从牌拖到随从区上方自动调整位置，而在释放鼠标后销毁该对象，才真正放上去
        /// 但现在，我不想要这种效果
        /// </summary>
        //private static MinionView emptyMinionView = null;

        protected override void Awake()
        {
            base.Awake();
            //if (own == 0) own = NetworkManager.instance.playerEnum;
        }

        protected override async void OnCardEffect(CardEffectEvent @event)
        {
            if (@event.cardId != networkId)
                return;

            PlayerView playerView = GetComponentInParent<PlayerView>();

            /*if (card.CardId / 1000 == 3)
            {
                transform.SetParent(UIManager.instance.entourageArea.transform);
                active = false;
                playerView.handCards.Remove(this);
                await ResetPosition(UIManager.instance.entourageArea.transform.position);
                return;
            }*/
            transform.SetParent(UIManager.instance.myselfMinion.transform);

            if (playerView != null && playerView.handCards.Contains(this))
            {
                playerView.handCards.Remove(this);

                if (own == GameManager.instance.Myself.playerEnum)
                    GameManager.instance.Myself.AddCard(GetComponent<CardBase>());
                else GameManager.instance.Opponent.AddCard(GetComponent<CardBase>());
            }
            await ResetPosition(UIManager.instance.myselfMinion.transform.position);

            //强制不让再用
            active = false;
        }
    }
}