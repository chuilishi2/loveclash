﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Script.core;
using Script.Manager;
using TMPro;
using UnityEngine;

namespace Script.view
{
    public class PlayerView : MonoBehaviour
    {
        public List<ICardView> handCards = new List<ICardView>();
        public List<MinionView> minions = new List<MinionView>(7);

        // TODO 上面 MinionView 考虑废弃
        public List<NewMinionView> newMinionViews = new List<NewMinionView>(5);

        public int _心动值_ = 10;
        public int _信任值_ = 10;
        public int _上头值_ = 10;

        [HideInInspector]
        public TMP_Text 心动值;
        [HideInInspector]
        public TMP_Text 信任值;
        [HideInInspector]
        public TMP_Text 上头值;
        [HideInInspector]
        public Transform 手牌区;
        [HideInInspector]
        public Transform 随从区;

        public virtual void Awake()
        {
            心动值 = transform.Find("心动值/心动值Text").gameObject.GetComponent<TextMeshProUGUI>();
            上头值 = transform.Find("上头值/上头值Text").gameObject.GetComponent<TextMeshProUGUI>();
            信任值 = transform.Find("信任值/信任值Text").gameObject.GetComponent<TextMeshProUGUI>();
            手牌区 = transform.Find("手牌区");
            随从区 = UIManager.instance.myselfMinion;
        }

        public virtual void 刷新心动值(int value)
        {
            心动值.text = value.ToString();
        }

        public virtual void 刷新信任值(int value)
        {
            信任值.text = value.ToString() ;
        }

        public virtual void 刷新上头值(int value)
        {
            上头值.text = value.ToString();
        }

        public virtual void 刷新心动值倍率(float value)
        {

        }

        public virtual void 刷新信任值倍率(float value)
        {

        }

        public virtual void 刷新上头值倍率(float value)
        {

        }

        public virtual void 刷新性别(PlayerBase.Gender value)
        {

        }

        //anchoredPosition为单位的间隔
        private float interval;
        protected UniTask AdjustHandCardsPos()
        {
            if (handCards.Count == 0) return UniTask.CompletedTask;
            UniTask[] tasks = new UniTask[handCards.Count];
            for (int i = 0; i < handCards.Count; i++)
            {
                var minus = i - (handCards.Count - 1) / 2f;
                var position = 手牌区.position;
                handCards[i].transform.SetParent(手牌区);
                tasks[i] = handCards[i].ResetPosition(new Vector3(minus * UIManager.instance.cardInterval + position.x, position.y));
            }
            return UniTask.WhenAll(tasks);
        }

        public UniTask AdjustMinionsPos()
        {
            if (minions.Count == 0) return UniTask.CompletedTask;
            UniTask[] tasks = new UniTask[minions.Count];
            for (int i = 0; i < minions.Count; i++)
            {
                var minus = i - (minions.Count - 1) / 2f;
                var position = 随从区.position;
                minions[i].transform.SetParent(随从区);
                if (minions[i] == null) continue;
                //TODO 随从相关
                //tasks[i] = minions[i].ResetPosition(new Vector3(minus * UIManager.instance.minionInterval + position.x, position.y));
            }
            return UniTask.WhenAll(tasks);
        }
    }
}