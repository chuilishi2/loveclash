﻿using Cysharp.Threading.Tasks;
using Script.Cards;
using Script.Manager;
using System;
using TMPro;
using UnityEngine;

namespace Script.view
{
    public class OpponentView : PlayerView
    {
        public override void Awake()
        {
            base.Awake();
            UIManager.instance.opponentView = this;
            
        }

        public void DrawCard(ICardView cardView)
        {
            try
            {
                handCards.Add(cardView);

                GameManager.instance.Opponent.AddCard(cardView.GetComponent<CardBase>());

                AdjustHandCardsPos();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}