using Cysharp.Threading.Tasks;
using Script.Cards;
using Script.Manager;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Script.view
{
    public class MyselfView : PlayerView
    {
        public override void Awake()
        {
            base.Awake();
            UIManager.instance.myselfView = this;
            随从区 = UIManager.instance.myselfMinion;
        }

        /// <summary>
        /// 抽Cards.Card入手牌
        /// </summary>
        /// <param name="cards"></param>
        public UniTask DrawCard(List<Cards.CardBase>cards)
        {
            if (cards.Count == 0)
            {
                Debug.Log("没牌了");
                return UniTask.CompletedTask;
            }
            return AdjustHandCardsPos();
        }

        public void DrawCard(ICardView cardView)
        {
            try
            {
                handCards.Add(cardView);

                GameManager.instance.Myself.AddCard(cardView.GetComponent<CardBase>());

                AdjustHandCardsPos();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /*
        /// <summary>
        /// 获取指定卡牌，需要卡牌 TableId
        /// 或者：改进 TableManager 的查询方法，提供泛型查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cardView"></param>
        /// <param name="id"></param>
        public void DrawCard<T>(ICardView cardView, int id) where T : CardBase
        {
            try
            {
                handCards.Add(cardView);
                //这里是通过 CardView 再去获取卡牌基类脚本，如果不想要 CardView 参数，参数里就换成 T
                T cardBase = cardView.GetComponent<T>();
                if (cardBase != null)
                {
                    GameManager.instance.Myself.AddCards(id, cardBase);
                }

                AdjustHandCardsPos();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        */
    }
}
