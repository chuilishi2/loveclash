﻿using System;
using VMFramework.Containers;
using VMFramework.GameLogicArchitecture;

namespace LoveClash.Containers
{
    [GamePrefabAutoRegister(ID)]
    public class DiscardPileContainerPreset : ListContainerPreset
    {
        public const string ID = "discard_pile_container";

        public override Type gameItemType => typeof(DiscardPileContainer);
    }
}