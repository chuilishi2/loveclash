﻿using System;
using VMFramework.Containers;
using VMFramework.GameLogicArchitecture;

namespace LoveClash.Containers
{
    [GamePrefabAutoRegister(ID)]
    public class CurrentDeckContainerPreset : ListContainerPreset
    {
        public const string ID = "current_deck_container";

        public override Type gameItemType => typeof(CurrentDeckContainer);
    }
}