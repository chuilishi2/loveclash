using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SOBase<T> : ScriptableObject where T : SOBase<T>
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                T[] asset = Resources.LoadAll<T>("");
                if (asset == null || asset.Length < 1)
                {
                    throw new System.Exception("找不到名为" + typeof(T).FullName + "的ScriptObject");
                }
                else if(asset.Length>1)
                {
                    Debug.LogWarning("有多个名为" + typeof(T).FullName + "的ScriptObject");
                }
                instance= asset[0];
            }
            return instance;
        }
    }
}
