using System.Collections;
using System.Collections.Generic;

using AYellowpaper.SerializedCollections;

using Script.core;

using UnityEngine;
/// <summary>
/// ������so
/// </summary>
[CreateAssetMenu(menuName = "Card/BaseCardTable", fileName = "BaseCardTable")]
public class BaseCardTable : SOBase<BaseCardTable>
{
    [SerializeField]
    public SerializedDictionary<int, BaseCardTableItem> DataList = new SerializedDictionary<int, BaseCardTableItem>();

    public BaseCardTableItem GetItemById(int id)
    {
        return DataList[id];
    }
    public void addData(BaseCardTableItem item)
    {
        if(!DataList.ContainsKey(item.id))
        {
            DataList.Add(item.id, item);
        }
    }
}