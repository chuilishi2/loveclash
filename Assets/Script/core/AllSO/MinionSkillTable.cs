using System.Collections;
using System.Collections.Generic;

using AYellowpaper.SerializedCollections;

using Script.core;

using UnityEngine;
[CreateAssetMenu(menuName = "Card/MinionSkillTable", fileName = "MinionSkillTable")]
public class MinionSkillTable : SOBase<MinionSkillTable>
{
    [SerializeField]
    public SerializedDictionary<int, MinionSkillTableItem> DataList = new SerializedDictionary<int, MinionSkillTableItem>();

    public MinionSkillTableItem GetItemById(int id)
    {
        return DataList[id];
    }
    public void AddMinionSkillCard(MinionSkillTableItem item)
    {
        if(!DataList.ContainsKey(item.id))
            DataList[item.id] = item;
    }
}