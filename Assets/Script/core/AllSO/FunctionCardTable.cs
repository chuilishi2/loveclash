using System.Collections;
using System.Collections.Generic;

using AYellowpaper.SerializedCollections;

using Script.core;

using UnityEngine;
/// <summary>
/// ������so
/// </summary>
[CreateAssetMenu(menuName = "Card/FunctionCardTable", fileName = "FuctionCardTable")]
public class FunctionCardTable : SOBase<FunctionCardTable>
{
    [SerializeField]
    public SerializedDictionary<int, FunctionTableItem> DataList = new SerializedDictionary<int, FunctionTableItem>();

    public FunctionTableItem GetItemById(int id)
    {
        return DataList[id];
    }
    public void addFunctionCard(FunctionTableItem functionTableItem)
    {
        if (!DataList.ContainsKey(functionTableItem.id))
        {
            DataList.Add(functionTableItem.id, functionTableItem);
        }
    }
}