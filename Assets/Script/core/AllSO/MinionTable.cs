using System.Collections;

using System.Collections.Generic;

using AYellowpaper.SerializedCollections;

using Script.core;

using UnityEngine;


[CreateAssetMenu(menuName = "Card/MinionTable", fileName = "MinionTable")]
public class MinionTable : SOBase<MinionTable>
{
    [SerializeField]
    public SerializedDictionary<int, MinionTableItem> DataList = new SerializedDictionary<int, MinionTableItem>();

    public MinionTableItem GetItemById(int id)
    {
        return DataList[id];
    }
    public void AddMinion(MinionTableItem item)
    {
        DataList[item.id] = item;
    }
}