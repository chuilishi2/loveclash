using System.Collections;
using System.Collections.Generic;
using SQLite4Unity3d;

using UnityEngine;

/// <summary>
/// 具体某个功能牌
/// </summary>
[System.Serializable]
public class FunctionTableItem
{
#region
    [SerializeField]private int _id;
    [SerializeField]private string _functionName;
    [SerializeField]private string _functionEnglishName;
    [SerializeField]private string _description;
    [SerializeField]private string _specialEffect;
    [SerializeField] public List<string> counterText = new List<string>();
    [SerializeField] private string _counterDescription;
    [SerializeField] private int _candrawRound;
    [SerializeField] private int _effectiveRound;
    [SerializeField] private float _drwaProbability;
    [SerializeField] private float _buffProbability;
    [SerializeField] private string _buff;
    [SerializeField] private string _SpeciousEffect;
    [SerializeField] public List<int> targetId = new List<int>();
    #endregion
    [PrimaryKey]
    public int id { get => _id; set => _id = value; }
    //功能牌名字
    public  string functionName { get => _functionName; set => _functionName = value; }
    //功能英文名
    public string functionEnglishName { get => _functionEnglishName; set => _functionEnglishName = value; }
    //描述
    public string description { get => _description; set => _description = value; }
    //请使用counterText查看可反制的牌的类型
    public string Text { get; set; }
    //反制效果UI描述
    public string counterDescription { get => _counterDescription; set => _counterDescription = value; }
    //几回合后出效果
    public int effectiveRound { get => _effectiveRound; set => _effectiveRound = value; }
    //几回合后可抽取
    public  int candrawRound { get => _candrawRound; set => _candrawRound = value; }
    //抽取概率
    public float drwaProbability { get => _drwaProbability; set => _drwaProbability = value; }
    //随机事件概率
    public float buffProbability { get => _buffProbability; set => _buffProbability = value; } 
    //buff
    public string buff { get => _buff; set => _buff = value; }
    //特殊效果
    public string SpeciousEffect { get => _SpeciousEffect; set => _SpeciousEffect = value; }
    //目标id：0是对方，1是自己
    public string TargetIdText { get ; set ; }
}


/// <summary>
/// 具体某个基础卡
/// </summary>
[System.Serializable]
public class BaseCardTableItem
{
    #region
    [SerializeField] private int _id;
    [SerializeField] private string _basecardname;
    [SerializeField] private string _basecardTextType;
    [SerializeField] private int _;
    //策划表暂时没有补充
    [SerializeField] private string _description;
    [SerializeField] private string _relatedRandomEvents;
    [SerializeField] private string _series;

    [SerializeField] private int _heartbeat;
    [SerializeField] private int _trust;
    [SerializeField] private int _exciting;

    [SerializeField] private float _probabilityOfTriggeringRandomEvents = 0.01F;

    [SerializeField] public List<int> targetId = new List<int>();
    #endregion

    [PrimaryKey] public int id { get { return _id; } set { _id = value; } }
    //基础牌名字
    public string basecardName { get { return _basecardname; } set { _basecardname = value; } }
    //基础牌类
    public string basecardTextType { get { return _basecardTextType; } set { _basecardTextType = value; } }
    //基础牌描述
    public string description { get { return _description; } set { _description = value; } }
    //心动值
    public int heartBeat { get { return _heartbeat; } set { _heartbeat = value; } }
    //信任值
    public int trust { get { return _trust; } set { _trust = value; } }
    //上头值
    public int exciting { get { return _exciting; } set { _exciting = value; } }
    //随机事件
    public string relatedRandomEvents { get { return _relatedRandomEvents; } set { _relatedRandomEvents = value; } }
    //所属系列
    public string series { get { return _series; } set { _series = value; } }
    //随机事件概率
    public float probabilityOfTriggeringRandomEvents { get { return _probabilityOfTriggeringRandomEvents; } set { _probabilityOfTriggeringRandomEvents = value; } }
    //目标id：0是对方，1是自己
    public string targetIdText { get ; set ; }
}

/// <summary>
/// 具体某个随从技能
/// </summary>
[System.Serializable]
public class MinionSkillTableItem
{
    #region
    [SerializeField]
    private int _id;
    [SerializeField]
    private string _minionSkillName;
    [SerializeField]

    private string _decription;
    [SerializeField] public List<int> targetId = new List<int>();

    private string _description;

    #endregion

    [PrimaryKey]
    public int id { get => _id; set => _id = value; }
    //随从技能牌名字
    public string minionSkillName { get => _minionSkillName; set => _minionSkillName = value; }
    //随从技能牌描述

    public string decription { get => _decription; set => _decription = value; }
    //目标id：0是对方，1是自己
    public string targetIdText { get; set; }

    public string description { get => _description; set => _description = value; }

}

[System.Serializable]
public class MinionTableItem
{
    #region
    private int _id;
    [SerializeField]
    private string _minionName;
    [SerializeField]
    private string _minionEnglishName;
    [SerializeField]
    private string _minionSkill;
    [SerializeField]
    private string _description;
    [SerializeField]
    private int _minionSkillCardId;
    [SerializeField]
    private int _round;
    [SerializeField]
    private int _remaindRound;
    [SerializeField]
    public List<int> targetId=new List<int>();
    #endregion
    //key
    [PrimaryKey] public int id { get => _id; set => _id = value; }

    //随从牌名字
    public string minionName { get => _minionName; set => _minionName = value; }

    //随从牌英文名字
    public string minionEnglishName { get => _minionEnglishName; set => _minionEnglishName = value; }

    //随从技能名
    public string minionSkill { get => _minionSkill; set => _minionSkill = value; }

    //随从技能描述
    public string description { get => _description; set => _description = value; }

    //随从牌技能ID,随从技能牌Id是随从牌id+1000，0就是没有随从技能牌
    public int minionSkillCardId { get => _minionSkillCardId; set => _minionSkillCardId = value; }

    //CD回合
    public int round { get => _round; set => _round = value; }

    //剩余CD回合
    public int remaindRound { get => _remaindRound; set => _remaindRound = value; }
    //目标id：0是对方，1是自己
    public string targetIdText { get; set; }
}
