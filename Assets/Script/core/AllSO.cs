using System.Collections;
using System.Collections.Generic;

using AYellowpaper.SerializedCollections;

using Script.core;

using UnityEngine;


[CreateAssetMenu(menuName = "Card/���ƿ�", fileName = "AllCards")]
public class AllCardSO : SOBase<AllCardSO>
{
    public SerializedDictionary<string, NetworkObject> allCards;
}


