using System.Collections;
using System.Collections.Generic;

using Script;
using Script.core;

using UnityEngine;

public class testSO : MonoBehaviour
{
    public Dictionary<string, NetworkObject> dic1;
    public Dictionary<string, NetworkObject> dic2;
    public Dictionary<int, MinionTableItem> dic3;
    public Dictionary<int, MinionSkillTableItem> dic4;
    private void Start()
    {
        dic1 = AllCardSO.Instance.allCards;
        foreach(string  a in dic1.Keys)
        {
            Debug.Log(a.ToString());
        }
        dic2 = AllMinionSO.Instance.allMinions;
        foreach (string a in dic2.Keys)
        {
            Debug.Log(a.ToString());
        }
        dic3 = MinionTable.Instance.DataList;
        foreach (int a in dic3.Keys)
        {
            Debug.Log(a);
        }
        dic4=MinionSkillTable.Instance.DataList;
        foreach (MinionSkillTableItem a in dic4.Values)
        {
            Debug.Log(a.minionSkillName.ToString());
        }
    }
}
