using Script.Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOverState : IProcessState
{
    public void OnEnter()
    {
        
    }

    public void OnExit()
    {
        
    }

    public void OnUpdate()
    {
        using var evt = TurnOverEvent.Get();
        evt.playerEnum = GameProcessor.CurPlayer;
        EventManager.SendEvent(evt);
        Debug.Log($"�غϽ�����{GameProcessor.CurPlayer}");

        if (!GameProcessor.CheckGameOver(out PlayerEnum winner))
        {
            GameProcessor.Winner = winner;
            GameProcessor.CurPlayer = GameProcessor.OppPlayer;
            GameProcessor.Run(GameState.StartTurn);
        }
        else
        {
            GameProcessor.Run(GameState.GameOver);
        }
    }
}
