using Script.Cards;
using Script.core;
using Script.Manager;
using Script.Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnRunningState : IProcessState
{
    private Timer timer;

    public void OnEnter()
    {
        EventManager.Register<PlayCardEvent>(OnPlayCard, GameEventPriority.High);

        timer ??= new Timer(GameProcessor.MAX_OUT_TIME, () =>
        {
            GameProcessor.TurnOver(GameProcessor.CurPlayer);
        });
    }

    public void OnExit()
    {
        TimerManager.Instance.StopTimer(timer);
        EventManager.UnregisterTarget(this);
    }

    public void OnUpdate()
    {
        using var evt = TurnRunningEvent.Get();
        evt.playerEnum = GameProcessor.CurPlayer;
        EventManager.SendEvent(evt);
        Debug.Log($"回合中：{GameProcessor.CurPlayer}");

        TimerManager.Instance.StopTimer(timer);
        TimerManager.Instance.EnQueue(timer);
    }

    private void OnPlayCard(PlayCardEvent @event)
    {
        CardBase card = @event.cardId.ToNetworkObject().GetComponent<CardBase>();
        PlayerEnum opponent = @event.playerEnum == PlayerEnum.Player1 ? PlayerEnum.Player2 : PlayerEnum.Player1;

        var counterCards = GameManager.instance.Opponent.GetCounterCard();
        foreach (var counterCard in counterCards)
        {
            if (counterCard.CanCounter(card))
            {
                GameProcessor.CounterInfo.originPlayerTurn = GameProcessor.CurPlayer;
                GameProcessor.CounterInfo.Push(card, @event.targetids);
                GameProcessor.CurPlayer = opponent;
                GameProcessor.Run(GameState.CounterTurn);
                return;
            }
        }

        if (card.GetComponent<EffectBase>() is IGlobal)
        {
            //要发消息的双方入队，在这里可以调整执行顺序、执行人数——意味着只让一方发消息也能在这里实现
            GlobalPlayersEnqueue(GameProcessor.CurPlayer, GameProcessor.OppPlayer);

            //检查一下 GlobalPlayers 队列是否为空
            if (GameProcessor.GlobalInfo.GlobalPlayers.Count >= 0)
            {
                //记录一下使游戏进入 Global 状态的卡牌
                GameProcessor.GlobalInfo.GlobalCard = card;

                //记录一下使游戏进入 Global 状态的玩家
                GameProcessor.GlobalInfo.OriginPlayer = GameProcessor.CurPlayer;

                GameProcessor.Run(GameState.GlobalTurn);
                return;
            }     
        }

        Operation operation = new Operation(OperationType.CardEffect);
        operation.baseNetworkId = @event.cardId;
        operation.targetNetworkIds = @event.targetids;
        GameManager.instance.ExecuteOperation(operation);
    }

    private void GlobalPlayersEnqueue(params PlayerEnum[] playerEnums)
    {
        foreach (PlayerEnum playerEnum in playerEnums)
        {
            GameProcessor.GlobalInfo.GlobalPlayers.Enqueue(playerEnum);
        }
    }
}
