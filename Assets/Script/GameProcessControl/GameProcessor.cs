using Script;
using Script.Cards;
using Script.core;
using Script.Effects.其他Effect;
using Script.Manager;
using Script.Network;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    None,
    GameStart, // 游戏开始
    StartTurn, // 回合开始
    RunningTurn, // 回合中
    GlobalTurn, //全局状态
    CounterTurn, // 反制阶段
    OverTurn, // 回合结束
    GameOver // 游戏结束
}

public class GameProcessor
{
    public static float MAX_OUT_TIME = 120;

    private static GameProcessor _instance;
    public static GameProcessor Instance
    {
        get
        {
            _instance ??= new GameProcessor();
            return _instance;
        }
    }

    private int _turnNum;
    public static int TurnNum { get => Instance._turnNum; set => Instance._turnNum = value; }

    private GameState _state;
    public static GameState State { get => Instance._state; set => Instance._state = value; }

    private PlayerEnum _curPlayer;
    public static PlayerEnum CurPlayer { 
        get => Instance._curPlayer;
        set
        {
            Instance._curPlayer = value;
        }
    }

    public static PlayerEnum OppPlayer
    {
        get => Instance._curPlayer == PlayerEnum.Player1 ? PlayerEnum.Player2 : PlayerEnum.Player1;
    }

    private PlayerEnum _winner;
    public static PlayerEnum Winner { get => Instance._winner; set => Instance._winner = value; }

    private CounterInfo _counerInfo;
    public static CounterInfo CounterInfo { get => Instance._counerInfo; set => Instance._counerInfo = value; }

    private GlobalInfo _globalInfo;
    public static GlobalInfo GlobalInfo { get => Instance._globalInfo; set => Instance._globalInfo = value; }

    private Dictionary<GameState, IProcessState> _states = new();
    private IProcessState _curProcessState;

    public GameProcessor()
    {
        _counerInfo = new();
        _globalInfo = new();

        _state = GameState.None;

        _states.Add(GameState.GameStart, new GameStartState());
        _states.Add(GameState.StartTurn, new TurnStartState());
        _states.Add(GameState.RunningTurn, new TurnRunningState());
        _states.Add(GameState.GlobalTurn, new GlobalState());
        _states.Add(GameState.CounterTurn, new CounterState());
        _states.Add(GameState.OverTurn, new TurnOverState());
        _states.Add(GameState.GameOver, new GameOverState());
    }

    public static void Start()
    {
        CounterInfo.Clear();
        GlobalInfo.Clear();
        Run(GameState.GameStart);
    }

    public static void Run(GameState gameState)
    {
        if (State == gameState)
        {
            Instance._curProcessState?.OnUpdate();
            return;
        }

        Instance._curProcessState?.OnExit();

        Instance._state = gameState;
        Instance._curProcessState = Instance._states[gameState];
        Instance._curProcessState.OnEnter();
        Instance._curProcessState.OnUpdate();
    }

    public static void TurnOver(PlayerEnum player)
    {
        if (CurPlayer != player)
            return;

        if (State == GameState.GlobalTurn)
        {
            //TODO CurPlayer 应该是不用改的，因为 SelectState 中没有改过 CurPlayer，回到回合中
            Run(GameState.RunningTurn);
            return;
        }

        if (State == GameState.CounterTurn)
        {
            CurPlayer = CounterInfo.originPlayerTurn;
            Run(GameState.RunningTurn);
        }
        else
        {
            Run(GameState.OverTurn);
        }
    }

    /// <summary>
    /// 返回开始的玩家
    /// </summary>
    /// <returns>玩家</returns>
    public static PlayerEnum GetStartPlayer()
    {
        return PlayerEnum.Player1;
    }

    public static PlayerEnum GetOtherPlayer(PlayerEnum playerEnum)
    {
        return playerEnum == PlayerEnum.Player1 ? PlayerEnum.Player2 : PlayerEnum.Player1;
    }

    #region 检测方法
    /// <summary>
    /// 检查玩家能否行动
    /// </summary>
    /// <param name="player">玩家</param>
    /// <returns>true：可以行动，false：不可以行动</returns>
    public static bool CheckPlayerCanAction(PlayerEnum playerEnum)
    {
        PlayerBase player = playerEnum == NetworkManager.instance.playerEnum ? GameManager.instance.Myself : GameManager.instance.Opponent;
        if (player == null)
            return false;

        /*// 如果是离线，那敌人也不要行动了。如果后面有敌人AI逻辑在改吧
        if (!NetworkManager.isOnline && player == GameManager.instance.Opponent)
            return false;*/

        return true;
    }

    /// <summary>
    /// 检查游戏是否结束了
    /// </summary>
    /// <returns>true：游戏结束，false：游戏没结束</returns>
    public static bool CheckGameOver(out PlayerEnum winner)
    {
        if (GameManager.instance.Myself.上头值 >= 50 || GameManager.instance.Myself.心动值 >= 50 || GameManager.instance.Myself.信任值 >= 50 ||
            GameManager.instance.Opponent.上头值 >= 50 || GameManager.instance.Opponent.心动值 >= 50 || GameManager.instance.Opponent.信任值 >= 50)
        {
            if ((GameManager.instance.Myself.上头值 + GameManager.instance.Myself.心动值 + GameManager.instance.Myself.信任值) > 
                (GameManager.instance.Opponent.上头值 + GameManager.instance.Opponent.心动值 + GameManager.instance.Opponent.信任值))
            {
                winner = NetworkManager.instance.playerEnum == PlayerEnum.Player1 ? PlayerEnum.Player1 : PlayerEnum.Player2;
                return true;
            }
            else if ((GameManager.instance.Myself.上头值 + GameManager.instance.Myself.心动值 + GameManager.instance.Myself.信任值) <
                (GameManager.instance.Opponent.上头值 + GameManager.instance.Opponent.心动值 + GameManager.instance.Opponent.信任值))
            {
                winner = NetworkManager.instance.playerEnum == PlayerEnum.Player1 ? PlayerEnum.Player2 : PlayerEnum.Player1;
                return true;
            }
        }
        winner = PlayerEnum.NotReady;
        return false;
    }
    #endregion
}
