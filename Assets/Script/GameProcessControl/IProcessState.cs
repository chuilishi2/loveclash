using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IProcessState
{
    /// <summary>
    /// 状态初始化使用，重复进入状态不会触发，主要用来初始化数据和监听事件的
    /// </summary>
    public void OnEnter();

    /// <summary>
    /// 实际逻辑写这里
    /// </summary>
    public void OnUpdate();

    /// <summary>
    /// 切换到其他状态前调用，主要用来处理善后工作，例如移除事件
    /// </summary>
    public void OnExit();
}
