using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStartState : IProcessState
{
    public void OnEnter()
    {
        
    }

    public void OnUpdate()
    {
        GameProcessor.TurnNum = 0;

        using var evt = GameStartEvent.Get();
        EventManager.SendEvent(evt);
        Debug.Log($"��Ϸ��ʼ");

        GameProcessor.CurPlayer = GameProcessor.GetStartPlayer();
        GameProcessor.Run(GameState.StartTurn);
    }

    public void OnExit()
    {
        
    }
}

