using Script.Effects.其他Effect;
using Script.Manager;
using Script.Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnStartState : IProcessState
{
    public void OnEnter()
    {
        
    }

    public void OnExit()
    {
        
    }

    public void OnUpdate()
    {
        if (GameProcessor.CurPlayer == PlayerEnum.Player1)
        {
            GameProcessor.TurnNum++;
            Debug.Log($"第{GameProcessor.TurnNum}回合");
        }

        using var evt = TurnStartEvent.Get();
        evt.playerEnum = GameProcessor.CurPlayer;
        EventManager.SendEvent(evt);
        Debug.Log($"回合开始：{GameProcessor.CurPlayer}");

        // 只管自己，不然就会自己和对面都触发发送一次抽卡行为
        if (GameProcessor.CurPlayer == NetworkManager.instance.playerEnum || !NetworkManager.isOnline)
        {
            Operation operation = new Operation(OperationType.DrawCard, extraMessage: typeof(DrawCardEffect).FullName);
            operation.playerEnum = GameProcessor.CurPlayer;
            operation.targetNetworkIds = new List<int> { (int)GameProcessor.CurPlayer };
            GameManager.instance.ExecuteOperation(operation);
        }

        if (GameProcessor.CheckPlayerCanAction(GameProcessor.CurPlayer))
        {
            GameProcessor.Run(GameState.RunningTurn);
        }
        else
        {
            GameProcessor.Run(GameState.OverTurn);
        }
    }
}
