using System.Collections;
using System.Collections.Generic;

using Script.Manager;

using UnityEngine;

public class GameOverState : IProcessState
{
    public void OnEnter()
    {
        
    }

    public void OnExit()
    {
        
    }

    public void OnUpdate()
    {
        var evt = GameOverEvent.Get();
        evt.winner = GameProcessor.Winner;
        evt.我方心动值 = GameManager.instance.Myself.心动值;
        evt.我方信任值 = GameManager.instance.Myself.信任值;
        evt.我方上头值 = GameManager.instance.Myself.上头值;
        evt.对方心动值 = GameManager.instance.Opponent.心动值;
        evt.对方信任值 = GameManager.instance.Opponent.信任值;
        evt.对方上头值 = GameManager.instance.Opponent.上头值;
        EventManager.SendEvent(evt);
        Debug.Log($"游戏结束");
    }
}
