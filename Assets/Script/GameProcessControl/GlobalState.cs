using Script.Cards;
using Script.Effects.其他Effect;
using Script.Manager;
using Script.Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 让双方都进行选择操作重点：让双方都进行选择操作的卡牌，要在 Effect 中管好 “自己” 的选择操作
/// 然后选择状态下，让自己触发一次 Effect，再给对面发一次 Effect
/// 
/// “出去玩” Effect 下，两种情况：1. 点击按钮做出选择，发出信息，等待对面信息；2. 超时，发出默认信息，等待对面信息
/// 双方在自己选择完毕并获取对方信息后，各自处理自己和对方的选择信息，决定接下来执行的 Effect 内部分支
/// </summary>
public class GlobalState : IProcessState
{
    private Timer timer;

    public void OnEnter()
    {
        EventManager.Register<OperationInfoEvent>(OnOperationInfo, GameEventPriority.High);

        timer ??= new Timer(GameProcessor.MAX_OUT_TIME, () =>
        {
            GameProcessor.TurnOver(GameProcessor.CurPlayer);
        });

        //流程：进入 GlobalState 后，队列出队，先执行一次 CardEffect，要求发送 OperationInfo，广播 OperationInfoEvent
        //然后这里监听到 OperationInfo，队列出队，出队成功再执行一次 CardEffect，直到队列为空
    }

    public void OnExit()
    {
        TimerManager.Instance.StopTimer(timer);
        EventManager.UnregisterTarget(this);

        //在成功获取双方信息后，最后执行一下真正的效果——注意：会有两次状态退出，要稍作处理
        (GameProcessor.GlobalInfo.GlobalCard.GetComponent<EffectBase>() as IGlobal).ReallyEffect();
    }

    public void OnUpdate()
    {
        using var turnGlobalEvt = TurnGlobalEvent.Get();
        turnGlobalEvt.playerEnum = GameProcessor.CurPlayer;
        EventManager.SendEvent(turnGlobalEvt);

        TimerManager.Instance.StopTimer(timer);
        TimerManager.Instance.EnQueue(timer);

        //队列不为空，则出队，轮到那个人执行 CardEffect
        if (GameProcessor.GlobalInfo.GlobalPlayers.Count > 0)
        {
            //更新枚举
            GameProcessor.CurPlayer = GameProcessor.GlobalInfo.GlobalPlayers.Dequeue();

            if (GameProcessor.CurPlayer == NetworkManager.instance.playerEnum)
            {
                //出队的人执行 CardEffect
                CardEffectOperation();
            }
        }
    }

    private void OnOperationInfo(OperationInfoEvent @event)
    {
        //记录消息
        GameProcessor.GlobalInfo.GlobalInfoQueue.Enqueue(@event.info);

        //队列为不空，则 Peek（这里不能出队，要在 OnUpdate 再出队），轮到那个人执行 Global
        if (GameProcessor.GlobalInfo.GlobalPlayers.Count > 0)
        {
            //更新枚举
            GameProcessor.CurPlayer = GameProcessor.GlobalInfo.GlobalPlayers.Peek();

            //出队的人执行 Global
            GameProcessor.Run(GameState.GlobalTurn);
        }
        else
        //如果队列为空，说明队列里的人都执行完毕，退出 Global
        {
            //先恢复 Cur
            GameProcessor.CurPlayer = GameProcessor.GlobalInfo.OriginPlayer;

            GameProcessor.TurnOver(@event.playerEnum);
        }
    }

    /// <summary>
    /// OriginPlayer 使用 GlobalCard 对 CurPlayer 执行 CardEffect 类型的 Operation
    /// </summary>
    private void CardEffectOperation()
    {
        Operation operation = new Operation(OperationType.CardEffect);

        //这里注意：CardEffect 的 operation.playerEnum 记录的是谁打出卡牌，所以要用 OriginPlayer
        operation.playerEnum = GameProcessor.GlobalInfo.OriginPlayer;

        operation.baseNetworkId = GameProcessor.GlobalInfo.GlobalCard.networkId;
        operation.targetNetworkIds = new List<int> { (int)GameProcessor.CurPlayer };
        GameManager.instance.ExecuteOperation(operation);
    }
}
