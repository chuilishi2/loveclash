using Script.Cards;
using Script.Manager;
using Script.Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CounterState : IProcessState
{
    private Timer timer;

    public void OnEnter()
    {
        EventManager.Register<PlayCardEvent>(OnPlayCard, GameEventPriority.High);

        timer ??= new Timer(GameProcessor.MAX_OUT_TIME, () =>
        {
            GameProcessor.TurnOver(GameProcessor.CurPlayer);
        });
    }

    public void OnExit()
    {
        TimerManager.Instance.StopTimer(timer);
        EventManager.UnregisterTarget(this);

        // 结束后把被反制的卡的效果触发一下
        while (GameProcessor.CounterInfo.counterCards.Count > 0)
        {
            (CardBase cardBase, List<int> targets) cardEffect = GameProcessor.CounterInfo.GetCard();
            Operation operation = new Operation(OperationType.CardEffect);
            operation.baseNetworkId = cardEffect.cardBase.networkId;
            operation.targetNetworkIds = cardEffect.targets;
            GameManager.instance.ExecuteOperation(operation);
        }
    }

    public void OnUpdate()
    {
        using var turnCounterEvt = TurnCounterEvent.Get();
        turnCounterEvt.playerEnum = GameProcessor.CurPlayer;
        EventManager.SendEvent(turnCounterEvt);

        TimerManager.Instance.StopTimer(timer);
        TimerManager.Instance.EnQueue(timer);
    }

    private void OnPlayCard(PlayCardEvent @event)
    {
        CardBase card = @event.cardId.ToNetworkObject().GetComponent<CardBase>();
        PlayerEnum opponent = @event.playerEnum == PlayerEnum.Player1 ? PlayerEnum.Player2 : PlayerEnum.Player1;
        // 不管有没有人能反制他，都把他加进反制列表，最后统一调用
        GameProcessor.CounterInfo.Push(card, @event.targetids);

        //以下是反制 反制牌的情况，考虑弃用
        /*var counterCards = GameManager.instance.Opponent.GetCounterCard();
        foreach (var counterCard in counterCards)
        {
            if (counterCard.CanCounter(card))
            {
                GameProcessor.CurPlayer = opponent;
                GameProcessor.Run(GameState.CounterTurn);
                return;
            }
        }*/

        // 反制阶段能打出的牌肯定是能反制当前牌的，所以不用检查条件了，直接回到原来的玩家回合吧
        GameProcessor.TurnOver(@event.playerEnum);
    }
}
