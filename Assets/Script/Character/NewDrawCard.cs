using Cysharp.Threading.Tasks;
using Script.core;
using Script.Manager;
using Script.Network;
using Script.view;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

namespace Script.Character
{
    public class NewDrawCard : CharacterBase
    {
        private Button BaseCardBtn;
        private Button FunctionCardBtn;
        private Button MinionCardBtn;
        private Button MinionSkillCardBtn;

        public override void PlayCard(int card, List<int> targets)
        {
            card.ToNetworkObject().GetComponent<EffectTrigger>().TriggerEffects(targets);
        }

        public override async UniTask<NetworkObject> DrawCard()
        {
            string randomCardName = null;
            NetworkObject card = null;

            // TODO 这里考虑放到游戏开始
            /*if (GameProcessor.TurnNum <= 1)
            {
                randomCardName = MinionTable.Instance.DataList[3000 + new Random().Next(1, 3)].minionName;
                randomCardName = ObjectFactory.Instance.allMinionsName[new Random().Next(0, ObjectFactory.Instance.allMinionsName.Count)];
                card = await NetworkManager.InstantiateNetworkObject(randomCardName, UIManager.instance.CardsParent);
                Debug.Log("回合随从牌启动！" + randomCardName);
                UIManager.instance.myselfView.DrawCard(card.GetComponent<ICardView>());
                return card;
            }*/

            randomCardName = ObjectFactory.Instance.allCardsName[new Random().Next(0, ObjectFactory.Instance.allCardsName.Count)];
            card = await NetworkManager.InstantiateNetworkObject(randomCardName, UIManager.instance.CardsParent);
            UIManager.instance.myselfView.DrawCard(card.GetComponent<ICardView>());
            return card;
        }

        private void Awake()
        {
            BaseCardBtn = UIManager.instance.myselfView.transform.Find("DrawCardBtn/BaseCardBtn").GetComponent<Button>();
            FunctionCardBtn = UIManager.instance.myselfView.transform.Find("DrawCardBtn/FunctionCardBtn").GetComponent<Button>();
            MinionCardBtn = UIManager.instance.myselfView.transform.Find("DrawCardBtn/MinionCardBtn").GetComponent<Button>();
            MinionSkillCardBtn = UIManager.instance.myselfView.transform.Find("DrawCardBtn/MinionSkillCardBtn").GetComponent<Button>();
        }

        private void Start()
        {
            BaseCardBtn.onClick.AddListener(async() =>
            {
                //从 SO 随机读取一张卡牌的数据
                //BaseCardTableItem CardTableItem = BaseCardTable.Instance.DataList[1000 + new Random().Next(1, BaseCardTable.Instance.DataList.Count)];

                //string name = CardTableItem.basecardName;

                NetworkObject card = await NetworkManager.InstantiateNetworkObject("BaseCard", UIManager.instance.CardsParent);
                UIManager.instance.myselfView.DrawCard(card.GetComponent<ICardView>());

                card.GetComponent<CardView>().own = NetworkManager.instance.playerEnum;
            });

            FunctionCardBtn.onClick.AddListener(async() =>
            {
                //从 SO 随机读取一张卡牌的数据
                //FunctionTableItem CardTableItem = FunctionCardTable.Instance.DataList[2000 + new Random().Next(1, FunctionCardTable.Instance.DataList.Count)];

                //string name = CardTableItem.functionName;

                NetworkObject card = await NetworkManager.InstantiateNetworkObject("FunctionCard", UIManager.instance.CardsParent);
                UIManager.instance.myselfView.DrawCard(card.GetComponent<ICardView>());

                card.GetComponent<CardView>().own = NetworkManager.instance.playerEnum;
            });

            MinionCardBtn.onClick.AddListener(async() =>
            {
                //从 SO 随机读取一张卡牌的数据
                //MinionTableItem CardTableItem = MinionTable.Instance.DataList[3000 + new Random().Next(1, MinionTable.Instance.DataList.Count)];

                //string name = CardTableItem.minionName;

                NetworkObject card = await NetworkManager.InstantiateNetworkObject("MinionCard", UIManager.instance.CardsParent);
                UIManager.instance.myselfView.DrawCard(card.GetComponent<ICardView>());

                card.GetComponent<CardView>().own = NetworkManager.instance.playerEnum;
            });

            MinionSkillCardBtn.onClick.AddListener(async() =>
            {
                //从 SO 随机读取一张卡牌的数据
                //MinionSkillTableItem CardTableItem = MinionSkillTable.Instance.DataList[4000 + new Random().Next(1, MinionSkillTable.Instance.DataList.Count)];

                //string name = CardTableItem.minionSkillName;

                NetworkObject card = await NetworkManager.InstantiateNetworkObject("MinionSkillCard", UIManager.instance.CardsParent);
                UIManager.instance.myselfView.DrawCard(card.GetComponent<ICardView>());

                card.GetComponent<CardView>().own = NetworkManager.instance.playerEnum;
            });
        }
    }
}