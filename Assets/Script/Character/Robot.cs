using Cysharp.Threading.Tasks;
using Script;
using Script.core;
using Script.Manager;
using Script.Network;
using Script.view;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Script.Character
{
    public class Robot : CharacterBase
    {
        private Player player;
        public bool autoOper = true;

        private void Start()
        {
            player = GetComponent<Player>();

            var autoControlTrans = UIManager.instance.opponentView.transform.Find("AutoControl");
            if (autoControlTrans != null)
            {
                var toggle = autoControlTrans.GetComponent<Toggle>();
                toggle.enabled = autoOper;
                toggle.onValueChanged.AddListener((vaue) =>
                {
                    autoOper = vaue;
                });
            }
        }

        private void OnEnable()
        {
            EventManager.Register<TurnRunningEvent>(OnTurnRunning);
            EventManager.Register<TurnCounterEvent>(OnTurnCounter);
            EventManager.Register<TurnOverEvent>(OnTurnOver);
        }

        private void OnDisable()
        {
            EventManager.UnregisterTarget(this);
        }

        private void OnTurnRunning(TurnRunningEvent @event)
        {
            // 如果当前是自己玩家的回合，但是本地的当前玩家不是自己，那就切换到自己去。
            // 因为如果操控了机器人进入反制阶段去回合结束的话，
            // 此时会回到自己的玩家回合，但是本地的玩家还是在机器人上的，所以要归位回自己玩家
            if (@event.playerEnum == GameManager.instance.LocalPlayer.playerEnum && NetworkManager.instance.playerEnum != @event.playerEnum)
            {
                NetworkManager.instance.playerEnum = GameManager.instance.LocalPlayer.playerEnum;
                return;
            }

            if (player == null)
                return;

            if (@event.playerEnum != player.playerEnum)
                return;

            if (autoOper) // 如果是ai自己操作，就先直接结束回合吧，后面有ai逻辑在补
            {
                GameProcessor.TurnOver(player.playerEnum);
            }
            else // 如果是玩家介入操作，那就把本地玩家改成机器人的玩家类型，这样就能切换身份了
            {
                NetworkManager.instance.playerEnum = player.playerEnum;
            }
        }

        private void OnTurnOver(TurnOverEvent @event)
        {
            if (player == null)
                return;

            if (@event.playerEnum != player.playerEnum)
                return;

            if (!autoOper) // 机器人回合结束，并且是玩家操作的，那就把玩家身份还回去
            {
                NetworkManager.instance.playerEnum = GameManager.instance.LocalPlayer.playerEnum;
            }
        }

        private void OnTurnCounter(TurnCounterEvent @event)
        {
            if (player == null)
                return;

            if (@event.playerEnum != player.playerEnum)
                return;

            if (autoOper) // 如果是ai自己操作，就先直接结束回合吧，后面有ai逻辑在补
            {
                GameProcessor.TurnOver(player.playerEnum);
            }
            else // 如果是玩家介入操作，那就把本地玩家改成机器人的玩家类型，这样就能切换身份了
            {
                NetworkManager.instance.playerEnum = player.playerEnum;
            }
        }

        public override async UniTask<NetworkObject> DrawCard()
        {
            var randomCardName = ObjectFactory.Instance.allCardsName[Random.Range(0, ObjectFactory.Instance.allCardsName.Count)];
            var card = await NetworkManager.InstantiateNetworkObject(randomCardName, UIManager.instance.CardsParent);
            UIManager.instance.opponentView.DrawCard(card.GetComponent<ICardView>());
            return card;
        }

        public override void PlayCard(int card, List<int> targets)
        {
            if (card.ToNetworkObject().GetComponent<BaseCardEffect>() != null)
            {
                GameManager.instance.Opponent.心动值++;
            }
            card.ToNetworkObject().GetComponent<EffectTrigger>().TriggerEffects(targets);
        }
    }
}