using Script.Cards;
using Script.core;
using Script.Network;
using System.Collections;
using System.Collections.Generic;
using UIFramework.Base;
using UnityEngine;
using UnityEngine.UIElements.Experimental;

public class TestEvent : GameEvent<TestEvent>
{
    public string message;
}

// 游戏开始
public class GameStartEvent : GameEvent<GameStartEvent>
{
    
}

// 游戏结束
public class GameOverEvent : GameEvent<GameOverEvent>
{
    public PlayerEnum winner;
    public int 我方心动值;
    public int 我方信任值;
    public int 我方上头值;
    public int 对方心动值;
    public int 对方信任值;
    public int 对方上头值;
}

// 回合开始
public class TurnStartEvent : GameEvent<TurnStartEvent>
{
    public PlayerEnum playerEnum;
}

// 回合中
public class TurnRunningEvent : GameEvent<TurnRunningEvent>
{
    public PlayerEnum playerEnum;
}

// 进入全局状态时广播
public class TurnGlobalEvent : GameEvent<TurnGlobalEvent>
{
    // TODO 待完善
    public PlayerEnum playerEnum;
}

// 反制阶段
public class TurnCounterEvent : GameEvent<TurnCounterEvent>
{
    public PlayerEnum playerEnum;
}

// 回合结束
public class TurnOverEvent : GameEvent<TurnOverEvent>
{
    public PlayerEnum playerEnum;
    public int 我方心动值;
    public int 我方信任值;
    public int 我方上头值;

    public int 对方心动值;
    public int 对方信任值;
    public int 对方上头值;
}

// 使用卡牌
public class PlayCardEvent : GameEvent<PlayCardEvent>
{
    public int cardId;
    public PlayerEnum playerEnum;
    public List<int> targetids;
}

// 卡牌效果
public class CardEffectEvent : GameEvent<CardEffectEvent>
{
    public int cardId;
    public PlayerEnum playerEnum;
    public List<int> targetids;
}

// 反制
public class CounterEvent : GameEvent<CounterEvent>
{
    public bool disable;
}

// 用技能
public class PlaySkillEvent : GameEvent<PlaySkillEvent>
{
    public PlayerEnum playerEnum;
}

// 抽卡
public class DrawCardEvent : GameEvent<DrawCardEvent>
{
    public PlayerEnum playerEnum;
    public CardBase card;
}

// 操作通知
public class OperationMessageEvent : GameEvent<OperationMessageEvent>
{
    public int targetNetworkId;
    public PlayerEnum playerEnum;
    public OperationMessage operationMessage;
}

// 发送信息
public class OperationInfoEvent : GameEvent<OperationInfoEvent>
{
    public int targetNetworkId;
    public PlayerEnum playerEnum;
    //改进为 int，避免解析字符串
    public int info;
}

public class PanelOpenEvent : GameEvent<PanelOpenEvent>
{
    public BasePanel basePanel;
}

public class PanelCloseEvent : GameEvent<PanelCloseEvent>
{
    public BasePanel basePanel;
}

public class WidgetOpenEvent : GameEvent<WidgetOpenEvent>
{
    public BaseWidget baseWidget;
}

public class WidgetCloseEvent : GameEvent<WidgetCloseEvent>
{
    public BaseWidget baseWidget;
    public int[] widgetCloseInfo;
}