using System;
using UnityEngine;

public class TimerManager : MonoBehaviour
{
    public static TimerManager Instance;

    private MyPriortyQueue<Timer> queue = new MyPriortyQueue<Timer>((x, y) => x.endTime.CompareTo(y.endTime));

    public float elapsedTime = 0;

    /// <summary>
    /// 单个计时器入队，多传入第二个参数可以反复使用一个计时器
    /// </summary>
    /// <param name="timer">复用次数</param>
    public void EnQueue(Timer timer, int count = 1)
    {
        timer.endTime = elapsedTime + timer.time;

        timer.leftTime = timer.time;

        timer.count = count;

        queue.Enqueue(timer);
    }

    /// <summary>
    /// 批量计时器入队
    /// </summary>
    /// <param name="timers"></param>
    public void EnQueue(params Timer[] timers)
    {
        foreach (Timer timer in timers)
        {
            if (timer != null)
            {
                timer.time = elapsedTime;

                timer.leftTime = timer.time;

                queue.Enqueue(timer);
            }
        }
    }

    /// <summary>
    /// 终止最近的计时器，一般用不上
    /// </summary>
    public void StopCurTimer()
    {
        //记录一下结束时间，可能有用
        queue.Dequeue().endTime = elapsedTime;
    }

    public void StopTimer(Timer timer)
    {
        timer.stopCount = timer.count;

        timer.count = 0;

        timer.leftTime = timer.endTime - elapsedTime;

        //结束事件设为 0，这样肯定会到队首并在 Update 出队
        timer.endTime = 0;
    }

    public void ReStartTimer(Timer timer)
    {
        timer.endTime = timer.leftTime + elapsedTime;

        timer.count = timer.stopCount;

        queue.Enqueue(timer);
    }

    private void Awake()
    {
        if (Instance != null) Destroy(gameObject);
        else Instance = this;
    }

    private void Update()
    {
        elapsedTime += Time.deltaTime;

        while (queue.Count > 0)
        {
            Timer temp = queue.Peek();

            //经过的时间 >= 结束时间，则出队，并且循环继续（因为同一帧里可能有多个计时器符合条件）
            if (elapsedTime >= temp.endTime)
            {
                queue.Dequeue();

                if (temp.count > 0)
                {
                    temp.action();

                    temp.count--;
                }

                if (temp.count > 0)
                {
                    temp.endTime += temp.time;
                    queue.Enqueue(temp);
                }
            }
            else
            {
                //如果队首就不符合上述条件了，说明后面的内容也都不会符合了，直接结束循环
                break;
            }
        }
    }

    /// <summary>
    /// @ 写的优先队列
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MyPriortyQueue<T>
    {
        private T[] items;
        private Comparison<T> comparison;
        private int count;
        public int Count { get => count; }
        public int Capacity { get => items == null ? 0 : items.Length; }

        public MyPriortyQueue()
        {
            items = new T[10];
            comparison = (x, y) => x.GetHashCode().CompareTo(y.GetHashCode());
        }

        public MyPriortyQueue(Comparison<T> comparison) : this()
        {
            this.comparison = comparison;
        }

        public void Enqueue(T item)
        {
            if (count >= Capacity)
                Expansion();
            items[count] = item;

            int cur = count++;
            if (cur == 0)
                return;
            int parent = cur;
            T oldValue;
            T newValue;
            do
            {
                cur = parent;
                parent = (cur - 1) / 2;
                oldValue = items[parent];
                Heapify(parent);
                newValue = items[parent];
            } while (!oldValue.Equals(newValue));
        }

        public T Dequeue()
        {
            if (count == 0)
                throw new Exception("the queue is empty");

            T result = items[0];

            Swap(items, 0, count - 1);
            count--;
            if (count > 0)
                Heapify(0);

            return result;
        }

        public T Peek()
        {
            if (count == 0)
                throw new Exception("the queue is empty");
            return items[0];
        }

        private void Heapify(int node)
        {
            int lc = 2 * node + 1;
            int rc = 2 * node + 2;
            int min = node;
            if (lc < count && comparison(items[min], items[lc]) > 0)
                min = lc;
            if (rc < count && comparison(items[min], items[rc]) > 0)
                min = rc;
            if (min != node)
            {
                Swap(items, node, min);
                Heapify(min);
            }
        }

        private void Swap(T[] array, int i, int j)
        {
            T t = array[i];
            array[i] = array[j];
            array[j] = t;
        }

        private void Expansion()
        {
            T[] newItems = new T[Capacity * 2];
            for (int i = 0; i < count; i++)
                newItems[i] = items[i];
            items = newItems;
        }
    }
}
