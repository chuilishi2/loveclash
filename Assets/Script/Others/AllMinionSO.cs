using System.Collections;
using System.Collections.Generic;

using AYellowpaper.SerializedCollections;

using Script.core;

using UnityEngine;

[CreateAssetMenu(menuName = "Card/��ӿ�", fileName = "��ӿ�")]
public class AllMinionSO : SOBase<AllMinionSO>
{
    [SerializeField]
    public SerializedDictionary<string, NetworkObject> allMinions=new SerializedDictionary<string, NetworkObject>();
}
