using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeCountTest : MonoBehaviour
{
    Timer timer;

    private void Awake()
    {
        timer = new Timer(1, () => { print(Time.time); });

        TimerManager.Instance.EnQueue(timer, 5);

        // 2s ����ֹ
        StartCoroutine("Test1");

        // 5s ������
        StartCoroutine("Test2");
    }

    IEnumerator Test1()
    {
        yield return new WaitForSeconds(2);

        TimerManager.Instance.StopTimer(timer);

        print("Stop");
    }

    IEnumerator Test2()
    {
        yield return new WaitForSeconds(5);

        TimerManager.Instance.ReStartTimer(timer);

        print("ReStart");
    }
}
