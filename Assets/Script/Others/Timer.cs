using System;

/// <summary>
/// 计时器，支持浮点数秒计时
/// </summary>
public class Timer
{
    //复用次数
    public int count = 1;

    //延时时长
    public float time;

    //计时结束执行的委托
    public Action action;

    //结束即时的时间（出队时间，时间早的优先级高）
    public float endTime;

    //终止时剩余 count
    public int stopCount;

    //剩余时间
    public float leftTime;

    /// <summary>
    /// 计时器带参构造方法
    /// </summary>
    /// <param name="time">延时时长</param>
    /// <param name="action">延时结束后执行的委托</param>
    public Timer(float time, Action action)
    {
        this.time = time;
        this.action = action;
    }

    //测试方法，倒计时结束时执行
    public void TimeOut()
    {
        action();
    }
}
