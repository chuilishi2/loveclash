using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SQLite4Unity3d;
using UnityEditor.MemoryProfiler;
using NugetForUnity;
using System;
using System.IO;
using OfficeOpenXml;
using Cysharp.Threading.Tasks;

public class DataTool : MonoBehaviour
{
    public SQLiteConnection Connection;

    public bool pushMinionToSqlite = true;
    public bool pushMinionToSO = true;
    public bool pushMinionSkillCardToSqlite = true;
    public bool pushMinionSkillCardToSO = true;
    public bool pushFunctionToSqlite = true;
    public bool pushFunctionToSO = true;
    public bool pushBaseToSqlite = true;
    public bool pushBaseToSO = true;

    // Start is called before the first frame update
    void Start()
    {
        Connection = new SQLiteConnection(Application.streamingAssetsPath + "/LoveClashDB.db", SQLiteOpenFlags.ReadWrite);
        if (pushMinionToSqlite)
            PushMinionToSqlite();

        if (pushMinionToSO)
            PushMinionToSO();

        if (pushMinionSkillCardToSqlite)
            PushMinionSkillCardToSqlite();

        if (pushMinionSkillCardToSO)
            PushMinionSkillCardToSO();

        if (pushFunctionToSqlite)
            PushFunctionToSqlite();

        if (pushFunctionToSO)
            PushFunctionToSO();

        if (pushBaseToSqlite)
            PushBaseToSqlite();

        if (pushBaseToSO)
            PushBaseToSO();
    }
    /// <summary>
    /// 将基础牌从excel到入数据库
    /// </summary>
    private void PushBaseToSqlite()
    {
        Connection.DropTable<BaseCardTableItem>();
        Connection.CreateTable<BaseCardTableItem>();
         using (ExcelPackage excel = new ExcelPackage(new FileInfo(Application.streamingAssetsPath + "/BaseCard.xlsx")))
         {
            ExcelWorksheet wb = excel.Workbook.Worksheets[0];
            for (int i = 2; i <= wb.Dimension.End.Row; i++)
            {
                if (wb.Cells[i, 2].Value == null)
                    continue;
                 var p = new BaseCardTableItem
                 {
                    id = wb.Cells[i, 2].Value != null ? int.Parse(wb.Cells[i, 2].Value.ToString()):0,
                    basecardName = wb.Cells[i, 1].Value != null ? wb.Cells[i, 1].Value.ToString() : null,
                    heartBeat= wb.Cells[i, 3].Value != null ? int.Parse(wb.Cells[i, 3].Value.ToString()) : 0,
                    trust= wb.Cells[i, 4].Value != null ? int.Parse(wb.Cells[i, 4].Value.ToString()) : 0,
                    exciting= wb.Cells[i, 5].Value != null ? int.Parse(wb.Cells[i, 5].Value.ToString()) : 0,
                    basecardTextType= wb.Cells[i, 6].Value != null ? wb.Cells[i, 6].Value.ToString() : null,
                    relatedRandomEvents= wb.Cells[i, 7].Value != null ? wb.Cells[i, 7].Value.ToString() : null,
                    series= wb.Cells[i, 9].Value != null ? wb.Cells[i, 9].Value.ToString() : null,
                    probabilityOfTriggeringRandomEvents= wb.Cells[i, 8].Value != null ? float.Parse(wb.Cells[i, 8].Value.ToString()) : 0.1f,
                    targetIdText= wb.Cells[i, 10].Value != null ? wb.Cells[i, 10].Value.ToString() : null,
                 };
                 Connection.Insert(p);
            }
        }
    }
    /// <summary>
    /// 基础牌从sqltite导入SO
    /// </summary>
    private void PushBaseToSO()
    {
        var datas = Connection.Table<BaseCardTableItem>();
        foreach (BaseCardTableItem v in datas)//遍历
        {
            if(v.targetIdText != null)
            {
                foreach(string id in v.targetIdText.Split(','))
                {
                    v.targetId.Add(int.Parse(id));
                }
            }
            BaseCardTable.Instance.addData(v);
        }
    }
    /// <summary>
    /// 将技能牌从excel到入数据库
    /// </summary>
    private void PushFunctionToSqlite()
    {
        Connection.DropTable<FunctionTableItem>();
        Connection.CreateTable<FunctionTableItem>();
        using (ExcelPackage excel = new ExcelPackage(new FileInfo(Application.streamingAssetsPath + "/FunctionCard.xlsx")))
        {
            ExcelWorksheet wb = excel.Workbook.Worksheets[0];
            for (int i = 2; i <= wb.Dimension.End.Row; i++)
            {
                if (wb.Cells[i, 2].Value == null)
                    continue;
                var p = new FunctionTableItem
                {
                    id = wb.Cells[i, 2].Value!=null?int.Parse(wb.Cells[i, 2].Value.ToString()):0,
                    functionName = wb.Cells[i, 1].Value != null ? wb.Cells[i, 1].Value.ToString() : null,
                    functionEnglishName = wb.Cells[i, 3].Value != null ? wb.Cells[i, 3].Value.ToString() : null,
                    description = wb.Cells[i, 4].Value != null ? wb.Cells[i, 4].Value.ToString() : null,
                    Text = wb.Cells[i, 5].Value != null ? wb.Cells[i, 5].Value.ToString() : null,
                    counterDescription = wb.Cells[i, 6].Value != null ? wb.Cells[i, 6].Value.ToString() : null,
                    candrawRound = wb.Cells[i, 10].Value != null ? int.Parse(wb.Cells[i, 10].Value.ToString()):0,
                    effectiveRound = wb.Cells[i, 14].Value != null ? int.Parse(wb.Cells[i, 14].Value.ToString()):0,
                    drwaProbability = wb.Cells[i, 7].Value != null ? float.Parse(wb.Cells[i, 7].Value.ToString()):0.1f,
                    buffProbability = wb.Cells[i, 9].Value != null ? float.Parse(wb.Cells[i, 9].Value.ToString()):0.1f,
                    buff = wb.Cells[i, 8].Value != null ? wb.Cells[i, 8].Value.ToString() : null,
                    SpeciousEffect = wb.Cells[i, 13].Value != null ? wb.Cells[i, 13].Value.ToString() : null,
                    TargetIdText = wb.Cells[i, 15].Value != null ? wb.Cells[i, 15].Value.ToString() : null,
                };
                Connection.Insert(p);
            }
        }
    }
    /// <summary>
    /// 将功能牌从数据库导入so
    /// </summary>
    private void PushFunctionToSO()
    {
        var datas = Connection.Table<FunctionTableItem>();
        foreach (FunctionTableItem v in datas)//遍历
        {
            if (v.TargetIdText != null)
            {
                foreach (string id in v.TargetIdText.Split(','))
                {
                    v.targetId.Add(int.Parse(id));
                }
            }
            if (v.Text!=null)
            {
                foreach (string part in v.Text.Split(','))
                {
                    v.counterText.Add(part);
                }
            }
            FunctionCardTable.Instance.addFunctionCard(v);
        }
    }
    /// <summary>
    /// 随从技能牌从Excel导入Sqlite
    /// </summary>
    private void PushMinionSkillCardToSqlite()
    {
        Connection.DropTable<MinionSkillTableItem>();
        Connection.CreateTable<MinionSkillTableItem>();
        using (ExcelPackage excel = new ExcelPackage(new FileInfo(Application.streamingAssetsPath + "/MinionSkillCard.xlsx")))
        {
            ExcelWorksheet wb = excel.Workbook.Worksheets[0];
            for (int i = 2; i <= wb.Dimension.End.Row; i++)
            {
                if (wb.Cells[i, 2].Value == null)
                    continue;
                var p = new MinionSkillTableItem
                {
                    id = wb.Cells[i, 2].Value!=null?int.Parse(wb.Cells[i, 2].Value.ToString()):0,
                    minionSkillName = wb.Cells[i, 1].Value != null ? wb.Cells[i, 1].Value.ToString():null,
                    decription = wb.Cells[i, 3].Value != null ? wb.Cells[i, 3].Value.ToString():null,
                    targetIdText = wb.Cells[i, 5].Value != null ? wb.Cells[i, 5].Value.ToString() : null,
                };
                Connection.Insert(p);
            }
        }
    }
    
/// <summary>
/// 随从技能牌从sqltite导入SO
/// </summary>
    private void PushMinionSkillCardToSO()
    {
        var datas = Connection.Table<MinionSkillTableItem>();
        foreach (MinionSkillTableItem v in datas)//遍历
        {
            if (v.targetIdText != null)
            {
                foreach (string id in v.targetIdText.Split(','))
                {
                    v.targetId.Add(int.Parse(id));
                }
            }
            MinionSkillTable.Instance.AddMinionSkillCard(v);
        }
    }

    /// <summary>
    /// 随从牌从Excel导入Sqlite
    /// </summary>
    private void PushMinionToSqlite()
    {
        Connection.DropTable<MinionTableItem>();
        Connection.CreateTable<MinionTableItem>();//创建表
        using (ExcelPackage excel = new ExcelPackage(new FileInfo(Application.streamingAssetsPath + "/MinionCard.xlsx")))
        {
            ExcelWorksheet wb = excel.Workbook.Worksheets[0];
            for(int row=2;row<=wb.Dimension.End.Row; row++)
            {
                if (wb.Cells[row, 2].Value == null)
                    continue;
                var p = new MinionTableItem
                {
                    id = wb.Cells[row, 2].Value != null?int.Parse(wb.Cells[row, 2].Value.ToString()):0,
                    minionName = wb.Cells[row, 1].Value != null?wb.Cells[row, 1].Value.ToString():null,
                    minionEnglishName = wb.Cells[row, 3].Value != null ? wb.Cells[row, 3].Value.ToString():null,
                    minionSkill = wb.Cells[row, 4].Value != null ? wb.Cells[row, 4].Value.ToString():null,
                    description = wb.Cells[row, 7].Value != null ? wb.Cells[row, 7].Value.ToString():null,
                    minionSkillCardId = wb.Cells[row, 5].Value != null ? int.Parse(wb.Cells[row, 5].Value.ToString()):0,
                    round = wb.Cells[row, 6].Value != null ? int.Parse(wb.Cells[row, 6].Value.ToString()):0,
                    remaindRound = wb.Cells[row, 6].Value != null ? int.Parse(wb.Cells[row, 6].Value.ToString()):0,
                    targetIdText = wb.Cells[row, 8].Value != null ? wb.Cells[row, 8].Value.ToString() : null,
                };
                Connection.Insert(p);
            }
        }
    }

    /// <summary>
    /// 随从牌从sqltite导入SO
    /// </summary>
    private void PushMinionToSO()
    {
        var datas = Connection.Table<MinionTableItem>();
        foreach (MinionTableItem v in datas)//遍历
        {
            if (v.targetIdText != null)
            {
                Debug.Log(v.minionName);
                foreach (string id in v.targetIdText.Split(','))
                {
                    Debug.Log(int.Parse(id));
                    v.targetId.Add(int.Parse(id));
                }
            }
            MinionTable.Instance.AddMinion(v);
        }
    }
}


