using System.Collections;
using System.Collections.Generic;
using Script.Network;
using Script.core;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
/// <summary>
/// 战斗数据结算
/// </summary>
public class EndMessage
{
    public PlayerEnum winner;

    public int 我方心动值;
    public int 我方信任值;
    public int 我方上头值;

    public int 对方心动值;
    public int 对方信任值;
    public int 对方上头值;

    public EndMessage(PlayerEnum winner, int 我方心动值, int 我方信任值, int 我方上头值, int 对方心动值, int 对方信任值, int 对方上头值)
    {
        this.winner = winner;
        this.我方心动值 = 我方心动值;
        this.我方信任值 = 我方信任值;
        this.我方上头值 = 我方上头值;
        this.对方心动值 = 对方心动值;
        this.对方信任值 = 对方信任值;
        this.对方上头值 = 对方上头值;
    }
}
public class EndManager : MonoBehaviour
{
    private static EndManager instance;
    public static EndManager Instance => instance;
    public EndMessage message;
    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this);
    }
    // Start is called before the first frame update
    void Start()
    {
        //EventManager.Register<GameOverEvent>(NewEndMessage, GameEventPriority.High);
        EventManager.Register<GameOverEvent>(LoadScene, GameEventPriority.High);
    }
    /// <summary>
    /// 创造战斗结算信息
    /// </summary>
    /// <param name="evt"></param>
    public void SetBattleEndMessage(ref GameOverEvent evt)
    {
        evt.我方心动值 = Myself.instance.心动值;
        evt.我方信任值 = Myself.instance.信任值;
        evt.我方上头值 = Myself.instance.上头值;
        evt.对方心动值 = Opponent.instance.心动值;
        evt.对方信任值 = Opponent.instance.信任值;
        evt.对方上头值 = Opponent.instance.上头值;
    }
    /// <summary>
    /// 加载结束界面
    /// </summary>
    /// <param name="evt"></param>
    private void LoadScene(GameOverEvent evt)
    {
        StartCoroutine(LoadEndScene(evt));
    }
    IEnumerator LoadEndScene(GameOverEvent evt)
    {
        message = new EndMessage(evt.winner,evt.我方心动值, evt.我方上头值, evt.我方信任值, evt.对方心动值, evt.对方上头值, evt.对方信任值);
        SceneManager.LoadSceneAsync(2);
        yield return null;
    }
}
