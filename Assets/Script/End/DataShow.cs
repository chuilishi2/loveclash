using System.Collections;
using System.Collections.Generic;

using Script.Network;

using TMPro;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DataShow : MonoBehaviour
{
    public TextMeshProUGUI Winner;
    public TextMeshProUGUI MyHeartbeatText;
    public TextMeshProUGUI MyTrustText;
    public TextMeshProUGUI MyExcitingText;
    public TextMeshProUGUI OpponentHeartbeatText;
    public TextMeshProUGUI OpponentTrustText;
    public TextMeshProUGUI OpponentExcitingText;

    public Button backHomePage;
    // Start is called before the first frame update
    private void OnEnable()
    {
        show();
        backHomePage.onClick.AddListener(() => { SceneManager.LoadSceneAsync("开始游戏"); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void show()
    {
        Debug.Log(EndManager.Instance.message.winner);
        if (EndManager.Instance.message.winner == NetworkManager.instance.playerEnum)
            Winner.text = "你胜利了";
        else
            Winner.text = "对方胜利了";
        Winner.color= Color.red;
        //战斗痕迹
        MyHeartbeatText.text = "我方心动值:" + EndManager.Instance.message.我方心动值;
        MyTrustText.text = "我方信任值:" + EndManager.Instance.message.我方信任值;
        MyExcitingText.text = "我方上头值:" + EndManager.Instance.message.我方上头值;
        OpponentHeartbeatText.text = "对方心动值:" + EndManager.Instance.message.对方心动值;
        OpponentTrustText.text = "对方信任值:" + EndManager.Instance.message.对方信任值;
        OpponentExcitingText.text = "对方上头值:" + EndManager.Instance.message.对方上头值;
    }

}
