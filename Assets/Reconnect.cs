using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Script.Network;
using UnityEngine;
using UnityEngine.EventSystems;

public class Reconnect : MonoBehaviour,IPointerClickHandler
{
    //用于在用户点击游戏对象时关闭所有网络连接并退出应用程序。
    public void OnPointerClick(PointerEventData eventData)
    {
         NetworkManager.CloseAll();
         UniTask.Delay(100).GetAwaiter().OnCompleted((Application.Quit));
    }
}